﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DTO.Objects
{
	public enum SMSSendMoneyStatus : short
	{
		New = 1,
		Sending = 2,
		Success = 3,
		Fail = 4,
		WrongTypePhone=5
	}

	public enum SMSSendMoneyPushStatus : short
	{
		New = 1,
		Success = 2,
		Fail = 3,
	}

	public class SMSSendMoney:ObjectBase
	{
		private string phonenumber;
		public string TelCo { get; set; }
		public string PhoneNumber 
		{ 
			get
			{
				string Phone = phonenumber.Replace("+","");


				if (Phone.StartsWith("84"))
				{
					Phone = "0" + Phone.Substring(2);
				}

				if (!Phone.StartsWith("0"))
				{
					Phone = "0" + Phone;
				}

				return Phone;
			}
			set
			{
				string Phone = value.Replace("+", "");


				if (Phone.StartsWith("84"))
				{
					Phone = "0" + Phone.Substring(2);
				}

				if (!Phone.StartsWith("0"))
				{
					Phone = "0" + Phone;
				}

				this.phonenumber = Phone;
			}

		}
		public string Money { get; set; }
		public SMSSendMoneyStatus SMSSendMoneyStatus { get; set; }
		public SMSSendMoneyPushStatus SMSSendMoneyPushStatus { get; set; }
		public string Remark { get; set; }
		public string ModemSent { get; set; }
		public string GUID { get; set; }

		public SMSSendMoney()
		{
			SMSSendMoneyStatus = SMSSendMoneyStatus.New;
			SMSSendMoneyPushStatus = SMSSendMoneyPushStatus.New;
		}
	}
}
