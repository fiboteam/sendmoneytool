using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Code.Utility;

namespace DTO.Objects
{
    [DataContract]
    public class KeywordPlanner : ObjectBase
    {

		public KeywordPlanner():base(){
		}

		[DataMember]
		public string KeywordName { get; set; } 

		[DataMember]
		public long AvgMonthlySearches { get; set; } 

		[DataMember]
		public string Competition { get; set; }
		public float CompetitionIndex { get; set; }

		[DataMember]
		public float SuggestedBid { get; set; } 

		[DataMember]
		public string FormatCurrency { get; set; } 

		[DataMember]
		public  KeywordPlanerStatus KeywordPlanerStatus { get; set; }
		public float ClickRate { get; set; }
		public long TotalCostPerMonth { get; set; }
		public long SEOFibo { get; set; }
		public long ChargesForMaintenance { get; set; }
		public long Setup { get; set; }
		public float HardWorkIndex { get; set; }
		public short CompetitionInt { get; set; }
		public long SetupCost { get; set; }
		[DataMember]
		public string Note { get; set; } 

		[DataMember]
		public string Remark { get; set; } 

		[DataMember]
		public int ShortCreatedDate { get; set; }
		public long UserId { get; set; }
    }
}