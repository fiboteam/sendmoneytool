﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using log4net;
using System.Threading;
using BusinessObjects;
using ModemInterface.ModemAdaptorService;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using Utility;

namespace ModemInterface
{

    public enum SMSProcessStatus
    {
        Connecting,
        Disconnect
    }
    public abstract class BaseSMSProcessFactory: BaseSupportSMS
    {
        public string ModemAdaptorName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsGatewayAdaptorPro { get; set; }
        public int mSendMTErrors = 0;
        public int mGetMOErros = 0;

        public TokenResponse Token { get; set; }
        public LoginResponse Logins { get; set; }

        public ModemAdaptor ModemAdaptor { get; set; }

        public delegate void ViewLog(string Message);
        public event ViewLog ViewLogEvent;

        public SMSProcessStatus SMSProcessStatus { get; set; }
        public void ViewLogHandle(string Message)
        {
            if (ViewLogEvent != null)
                ViewLogEvent(Message);
        }

        public ModemAdaptorServiceClient managerAdaptor { get; set; }

        /// <summary>
        /// List Tin nhắn cần gửi
        /// </summary>
        public List<SMS> SMSList { get; set; }

        /// <summary>
        /// List tin nhắn update status bị lỗi cần update lại
        /// </summary>
        public List<SMS> SMSListUpdateFail { get; set; }

        /// <summary>
        /// Dữ liệu SMS MO
        /// </summary>
        public List<ClientCommingSMSHostingData> SMSListMO { get; set; }

        public List<IncommingSMS> dataIncomming { get; set; }

        /// <summary>
        /// Nếu get tin 10 lần bị lỗi thì tự động gettoken và login lại
        /// </summary>
        protected int NumberGetSMSErrors { get; set; }

        /// <summary>
        /// Số lần cập nhật trạng thái tin nhắn bị lỗi
        /// </summary>
        protected int NumberUpdateSMSErrors { get; set; }

        /// <summary>
        /// Xác nhận Modem hỗ trợ gửi tuần tự từng tin nhắn
        /// </summary>
        public bool SupportSendSequentially { get; set; }

        /// <summary>
        /// Xác nhận Modem hỗ trợ gửi cả ListSMS
        /// </summary>
        public bool SupportSendList { get; set; }

        public List<int> listServiceTypeID { get; set; }

        public bool UseTimeConfig { get; set; }
        public int TimeDelay { get; set; }
        public int TotalSMSAccept { get; set; }
        public int CurrentSMS { get; set; }
        public bool SendMailAlert { get; set; }
        public bool AcceptSendSMS { get; set; }
        public bool SendSMSAlert { get; set; }
        /// <summary>
        /// Khởi tạo tham số
        /// </summary>
        /// <param name="ModemName"></param>
        /// <param name="UserName"></param>
        /// <param name="PassWord"></param>
        /// <param name="ListServiceTye"></param>
        /// <param name="IsGatewayPro"></param>
        /// <param name="logger"></param>
        public BaseSMSProcessFactory(string ModemName, string UserName, string PassWord, bool IsGatewayPro, ILog logger)
        {
            ModemAdaptorName = ModemName;
            this.UserName = UserName;
            this.Password = Password;
            this.IsGatewayAdaptorPro = IsGatewayPro;
            this.logger = logger;
            SupportSendList = false;
            SupportSendSequentially = false;
            NumberGetSMSErrors = 0;
            NumberUpdateSMSErrors = 0;
            SMSListUpdateFail = new List<SMS>();
            SMSListMO = new List<ClientCommingSMSHostingData>();
            dataIncomming = new List<IncommingSMS>();
            UseTimeConfig = false;
            TimeDelay = 0;
            TotalSMSAccept = 200;
            CurrentSMS = 0; 
            SendMailAlert = false;
            AcceptSendSMS = true;
            SendSMSAlert = true;
        }
        protected int GetServiceTypeInQueue()
        {
            try
            {
                int serviceTypeID = -1;
                serviceTypeID = listServiceTypeID[0];

                if (listServiceTypeID != null && listServiceTypeID.Count > 1)
                {
                    listServiceTypeID.RemoveAt(0);
                    listServiceTypeID.Add(serviceTypeID);
                }

                return serviceTypeID;
            }
            catch
            {
                return -1;
            }
        }
        /// <summary>
        /// Hàm lấy Token Trước khi Login
        /// </summary>
        /// <param name="manager"></param>
        public string GetToken()
        {
            try
            {
                managerAdaptor = new ModemAdaptorServiceClient("WSHttpBinding_IModemAdaptorService");
                var request = new TokenRequest();
                request.RequestId = Guid.NewGuid().ToString();
                request.ClientTag = UserName;
                Token = managerAdaptor.GetToken(request);
                return "Get Token Success";
            }
            catch (Exception ex)
            {
                return "Can't Get Token WS"+ex.Message;
            }

        }


        /// <summary>
        /// Hàm login lên WS và lấy modemAdaptor về
        /// </summary>
        /// <returns></returns>
        public ModemAdaptor Login()
        {
            try
            {
                var request = new LoginRequest
                                  {
                    RequestId = Guid.NewGuid().ToString(),
                    LoginName = ModemAdaptorName,
                    Password = Password,
                    ClientTag = UserName,
                    AccessToken = Token.AccessToken

                };
                Logins = managerAdaptor.Login(request);
                if (Logins != null && Logins.ModemAdaptor != null)
                {
                    ModemAdaptor = Logins.ModemAdaptor;
                    listServiceTypeID = ModemAdaptor.ListTelcoModemAdaptor.Select(telcoModemAdaptor => telcoModemAdaptor.ServiceTypeID).Distinct().ToList();
                    return ModemAdaptor;
                }
                logger.Error(Logins.Message);
            }
            catch (Exception ex)
            {
                logger.Error("Can't Connect WS" + ex.Message);
                ViewLogHandle("Can't Connect WS" + ex.Message);
            }
            return null;

        }
		public virtual bool SendMoney(string syntaxt, ref string content,string type)
		{
			return false;
		}

		public virtual bool DeleteMessage(string content)
		{
			return false;
		}

		public virtual bool SendMoneyWithToolkitMLoad(string phonenumber,string money)
		{
			return false;
		}
        /// <summary>
        /// Hàm chuẩn bị một số khởi tạo trước khi gửi tin. string != "" là bị lỗi
        /// </summary>
        /// <param name="ModemAdaptor"></param>
        public virtual string PrepareModemAdapter()
        {
            SMSProcessStatus = SMSProcessStatus.Connecting;
            return "";
        }

        /// <summary>
        /// Tinh số tin nhắn còn có thể gửi
        /// 16/05/2015
        /// NMH
        /// </summary>
        /// <param name="syntax"></param>
        /// <param name="phone"></param>
        /// <returns></returns>
        public virtual int GetSMSCanSent(string syntax, string[] patterns)
        {
            return 0;
        }

        /// <summary>
        /// Hàm End các kết nối sau khi gửi tin nếu cần thiết
        /// </summary>
        /// <param name="ModemAdaptor"></param>
        /// <returns></returns>
        public virtual bool EndConnectModemAdapter()
        {
            SMSProcessStatus = SMSProcessStatus.Disconnect;
            return false;
        }
        public DateTime TimeCheckMoney;

        public long CheckMoney(string Message, string template)
        {
            Func<string, bool> predicate = null;
            try
            {
                long num = -1L;
                string[] source = template.Split(new char[] { '$', '#' });
                string[] strArray2 = template.Split(new char[] { '$' });
                if (predicate == null)
                {
                    predicate = xx => Message.Contains(xx);
                }
                if (source.Where<string>(predicate).ToList<string>().Count == source.Length)
                {
                    string str = Message;
                    for (int i = 0; i < (strArray2.Length - 1); i++)
                    {
                        string[] strArray3 = strArray2[i].Split(new char[] { '#' });
                        int index = str.IndexOf(strArray3[strArray3.Length - 1]);
                        string[] strArray4 = strArray2[i + 1].Split(new char[] { '#' });
                        int startIndex = str.IndexOf(strArray4[0]);
                        string str2 = str.Substring(index + strArray3[strArray3.Length - 1].Length, startIndex - (index + strArray3[strArray3.Length - 1].Length));
                        num += long.Parse(str2.Replace(",", "").Replace(".", ""));
                        if (strArray4.Length > 1)
                        {
                            str = str.Substring(startIndex + strArray4[0].Length);
                        }
                        else
                        {
                            str = str.Substring(startIndex);
                        }
                    }
                }
                else
                {
                    return 0L;
                }
                return num;
            }
            catch (Exception)
            {
                return 0L;
            }
        }

        /*
        public long CheckMoney(string Message, string template)
        {
            long total = -1;
            string[] arrayCheck = template.Split('$', '#');
            string[] array = template.Split('$');
            var x = from xx in arrayCheck where Message.Contains(xx) select xx;
            if (x.ToList().Count == arrayCheck.Length)
            {
                string messagecheck = Message;
                for (int i = 0; i < array.Length - 1; i++)
                {
                    string[] arraystart = array[i].Split('#');
                    int start = messagecheck.IndexOf(arraystart[arraystart.Length - 1]);
                    string[] arrayend = array[i + 1].Split('#');
                    int end = messagecheck.IndexOf(arrayend[0]);
                    total += (long)double.Parse(messagecheck.Substring(start + arraystart[arraystart.Length - 1].Length, end - (start + arraystart[arraystart.Length - 1].Length)));
                    if (arrayend.Length > 1)
                        messagecheck = messagecheck.Substring(end + arrayend[0].Length);
                    else
                        messagecheck = messagecheck.Substring(end);
                }
            }
            else
            {
                return 0;
            }
            return total;
        }
         */

        public  long GetMoney()
        {
            try
            {
                if (smsSender == null) PrepareModemAdapter();
                ViewLogHandle("Bắt đầu kiểm tra tiền trong sim");
                string money = smsSender.CheckMoney("*101#");
                string template = "";
                using (var sr = new StreamReader("Template\\Template.txt"))
                {
                    template = sr.ReadToEnd();
                }
                string[] ArraySuccess = template.Split(new[] { "\n@@@@@\n" }, StringSplitOptions.None);
                long Total = (from t in ArraySuccess where t != "" select CheckMoney(money, t)).Concat(new long[] {0}).Max();
                ViewLogHandle("Kiểm tra tiền kết thúc:\n" + money);
                TimeCheckMoney = DateTime.Now;
                return Total;

            }
            catch (Exception ex)
            {
                ViewLogHandle("Kiểm tra tiền bị lỗi" + ex.Message);
                return 0;
            }        
        }

        #region NMHIEU
        /// <summary>
        /// Lấy danh sách tiền trong tài khoản
        /// </summary>
        /// <returns></returns>
        public List<int> MyGetMoney()
        {
            List<int> moneyList = new List<int>();
            try
            {
                if (smsSender == null) PrepareModemAdapter();
                ViewLogHandle("Bắt đầu kiểm tra tiền trong sim");
                string strMoney = smsSender.CheckMoney("*101#");
                string nameTelCom = GetNameTelCom(strMoney);
                moneyList = GetMoneys(nameTelCom, strMoney);
            }
            catch { }

            return moneyList;
        }
        /// <summary>
        /// Lấy mạng đi động
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string GetNameTelCom(string str)
        {
            string nameTelCom = string.Empty;

            if (str.Contains("Ty Phu"))
            {
                nameTelCom = "beeline";
            }
            else if (str.Contains("Mobi") || str.Contains("Q-Student"))
            {
                nameTelCom = "mobi";
            }
            else if (str.Contains("STUDENT") || str.Contains("VINAXTRA"))
            {
                nameTelCom = "vina";
            }

            return nameTelCom;
        }
        /// <summary>
        /// Lấy tiền trong tài khoản
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        private List<int> GetMoneys(string filename, string str)
        {
            List<int> balance = new List<int>();
            try
            {
                string s = "";
                using (StreamReader r = new StreamReader(@"Template\" + filename + ".txt"))
                {
                    s = r.ReadToEnd();
                }
                string[] style = s.Split('#');

                int indexStart = 0;
                int n = 0;
                int indexEnd = 0;
                int lenght = 0;
                string ob = "0";
                double num = 0;

                for (int i = 1; i < style.Length; i++)
                {
                    if (str.Contains(style[i]))
                    {
                        indexStart = str.IndexOf(style[i]);
                        n = style[i].Length;
                        indexEnd = str.IndexOf(style[0], indexStart);
                        lenght = indexEnd - (indexStart + n);
                        ob = str.Substring(indexStart + n, lenght);

                        if (IsNumeric(ob))
                        {
                            double.TryParse(ob.ToString(), out num);
                            balance.Add(Convert.ToInt32(num));
                        }
                    }
                }

            }
            catch
            {

            }
            return balance;
        }
        /// <summary>
        /// Kiem tra phai số ko\hông
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private bool IsNumeric(object o)
        {
            double num;
            return ((o != null) && double.TryParse(o.ToString(), out num));
        }

        public void SendMailAlertSendConcatenatedSMSStandardError(List<int> old, List<int> neww, int countSMSSuccess,int smsTotal)
        {
            string strOld = "";
            string strNew = "";
            foreach (int item in old)
            {
                strOld += "[" + item.ToString() + "] ";
            }
            foreach (int item in neww)
            {
                strNew += "[" + item.ToString() + "] ";
            }
            sendMail("reportagentjob@gmail.com", "sql@admin", "hieu.nm@fibo.vn", "Kiem tra tien sau loi SendConcatenatedSMSStandar",
                "truoc khi gui: " + strOld + "  Sau khi gui: " + strNew+" Tong sms: "+smsTotal+" so tin thanh cong "+countSMSSuccess);
        }
        #endregion

        /// <summary>
        /// Hàm lấy tin nhắn từ WS để gửi. Tự động gettoken và login lại nếu get 10 lần bị lỗi.Trả về dịch vụ hiện tại đang lấy tin
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="ServiceTypeID"></param>
        public virtual int GetListSMS( )
        {
            int CurrentService = -1;
            try
            {
                bool acceptGetSms = true;
                
                if (EnableCheckMoney &&(TimeCheckMoney == DateTime.MinValue || ((TimeSpan)(DateTime.Now - TimeCheckMoney)).TotalMinutes >=5 ))
                {

                    long money = GetMoney();
                    if (money < 10000)
                    {
                        acceptGetSms = false;
                        ViewLogHandle("Hết tiền");
                    }
                }
                else
                {
                    ViewLogHandle("Không kiểm tra tiền");
                }
                if (acceptGetSms)
                {
                    ViewLogHandle("Bắt đầu hàm GetListSMS");
                    for (int i = 0; i <= listServiceTypeID.Count; i++)
                    {
                        CurrentService = GetServiceTypeInQueue();
                        SMSCriteria crit = new SMSCriteria()
                        {
                            ModemAdaptorName = ModemAdaptorName,
                            ServiceTypeID = CurrentService
                        };
                        SMSRequest request = new SMSRequest()
                        {
                            LoadOptions = new string[] { "GetSMSToSend" },
                            Criteria = crit,
                            ClientTag = UserName,
                            AccessToken = Token.AccessToken
                        };

                        SMSResponse list = managerAdaptor.GetSMS(request);
                        if (list != null && list.ListSMS != null && list.ListSMS.Length > 0)
                        {
                            SMSList = list.ListSMS.ToList();
                            break;
                        }
                        else
                        {
                            SMSList = null;
                        }

                    }

                    ViewLogHandle("Kết thúc hàm GetListSMS");
                    if (SMSList != null)
                    {
                        WriteMessageLog("-----------------------START----------------------");
                        foreach (var item in SMSList)
                        {
                            WriteMessageLog(item.ID.ToString());
                        }
                        ViewLogHandle("SMSList.Count = " + SMSList.Count);
                    }
                    return CurrentService;
                }
                else
                {
                    WriteMessageLog("Không được lấy tin gửi");
                    return -4;
                }
                
            }
            catch (Exception exc)
            {
                ViewLogHandle(exc.Message);
                this.InsertLog(exc);
                NumberGetSMSErrors++;
                if (NumberGetSMSErrors >= 10)
                {
                    ReconnectWS();
                    NumberGetSMSErrors = 0;
                }
                logger.Error("Get Message bi loi");
                logger.Error("Help link: " + exc.HelpLink + ", Message: " + exc.Message + ", TypeError:" + exc.GetType().ToString());
                SMSList = null;
                return -2;
            }

        }

        /// <summary>
        /// Hàm Send 1 tin nhắn. Dùng gửi tin nhắn kiểu tuần tự
        /// </summary>
        /// <param name="sms"></param>
        /// <returns></returns>
        public virtual ResultSendSMS SendMT(SMS sms, ref int countSMSSuccess)
        {
            return null;
        }

        /// <summary>
        /// Update status của 1 tin nhắn. Tự động add tin vào danh sách cập nhật lại trạng thái nếu update bị lỗi
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="sms"></param>
        /// <returns></returns>
        public virtual bool UpdateSMS( SMS sms)
        {
            try
            {
                WriteLog(sms);
                if (sms.Message == null || sms.Message == "")
                {
                    sms.Message = "Noi dung khong hop le";
                }
                SMSCriteria criteria = new SMSCriteria()
                {
                    OperatorStatus = 1
                };
                SMSRequest request = new SMSRequest()
                {
                    Criteria = criteria,
                    SMS = sms,
                    Action = "UpdateSMSBaseOnSentStatus",
                    ClientTag = UserName,
                    AccessToken = Token.AccessToken

                };
                SMSResponse response = managerAdaptor.SetSMS(request);
                if (response.SMS != null && response.SMS.ID == sms.ID)
                {
                    NumberUpdateSMSErrors = 0;
                    return true;
                }
                else
                {
                    NumberUpdateSMSErrors++;
                    SetSMSUpdateError(sms);
                    if (NumberUpdateSMSErrors >= 10)
                    {
                        ReconnectWS();
                        NumberUpdateSMSErrors = 0;

                    }
                    return false;
                }
            }
            catch (Exception exc)
            {
                NumberUpdateSMSErrors++;
                if (NumberUpdateSMSErrors >= 10)
                {
                    ReconnectWS();
                    NumberUpdateSMSErrors = 0;

                }
                SetSMSUpdateError(sms);
                logger.Error("Update Message bi loi"+ DateTime.Now.ToString());
                logger.Error("Help link: " + exc.HelpLink + ", Message: " + exc.Message + ", TypeError:" + exc.GetType().ToString());
                return false;
            }
        }

        /// <summary>
        /// Add sms vào hàng đợi cập nhật lại trạng thái
        /// </summary>
        /// <param name="sms"></param>
        public void SetSMSUpdateError(SMS sms)
        {
            var check = true;
            foreach (var item in SMSListUpdateFail)
            {
                if (item.ID == sms.ID) check = false;
            }
            if (check)
                SMSListUpdateFail.Add(sms);
        }

        /// <summary>
        /// Get Token và Login lại nếu bị lỗi
        /// </summary>
        public void ReconnectWS()
        {
            managerAdaptor = new ModemAdaptorServiceClient("WSHttpBinding_IModemAdaptorService");
            logger.Error("ReconnectWS");
            GetToken();
            Login();
        }

        /// <summary>
        /// Lấy các MO cho dịch vụ hiểu là lấy MO cho mã dịch vụ đầu tiên trong ListServiceType
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="ServiceTypeID"></param>
        public virtual void GetMO( long ServiceTypeID)
        {

        }

        public virtual void GetMO_V2(long ServiceTypeID, ref string status)
        {

        }

        /// <summary>
        /// Submit MO lên WS.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="sms"></param>
        /// <returns></returns>
        public virtual string UpdateMO( ClientCommingSMSHostingData sms)
        {//Nếu gửi quá nhiều lần qua WS không được thì phải login lại phòng tường hợp modem chỉ get không gửi tin
            return "";
        }

        public virtual string UpdateMO_V2(IncommingSMS sms,bool usePushSMSToClient)
        {//Nếu gửi quá nhiều lần qua WS không được thì phải login lại phòng tường hợp modem chỉ get không gửi tin
            return "";
        }

        /// <summary>
        /// Xử lý khi quá trình gửi MT lỗi ở hàm SendMT_DoWork
        /// </summary>SendCMGD
        public virtual string ErrorSendMT()
        {
            mSendMTErrors++;
            if (mSendMTErrors != 0 && mSendMTErrors > ModemAdaptor.SendTime)
            {
                ReconnectWS();
                return "Thực hiện kết nối lại với Server do xảy ra lỗi SendMT "+mSendMTErrors+" lần";
            }
            return "";
        }
        
        /// <summary>
        /// Xử lý khi quá trình get MO lỗi hàm GetMO_DoWork
        /// </summary>
        /// <returns></returns>
        public virtual string ErrorGetMO()
        {
            mGetMOErros++;
            if (mGetMOErros != 0 && mGetMOErros > ModemAdaptor.GetTime)
            {
                ReconnectWS();
                return "Thực hiện kết nối lại với Server do xảy ra lỗi GetMO " + mGetMOErros + " lần";
            }
            return "";
        }


        public bool EnableCheckMoney = false;

        public string sendCMGL(string message, int timeOut, bool usePDU=false)
        {
            return smsSender.SendCMGL(message, timeOut, usePDU);
        }





		public virtual bool SendMoneyWithoutUSD(string message,string phone, ref string content)
		{
			throw new NotImplementedException();
		}

		public virtual bool SendMoneyWithViettelPayLater(string p1, string p2, ref string content)
		{
			throw new NotImplementedException();
		}
	}


}
