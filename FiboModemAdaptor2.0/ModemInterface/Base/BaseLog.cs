﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace ModemInterface
{
    public abstract class BaseLog
    {
        public ILog logger { get; set; }
         
        /// <summary>
        /// Hàm hỗ trợ ghi log
        /// </summary>
        /// <param name="ex"></param>
        public void InsertLog(Exception ex)
        {
            logger.Error("Help link: " + ex.HelpLink + ", Message: " + ex.Message + ", TypeError:" + ex.GetType().ToString());
        }
        public void InsertLog(string mess)
        {
            logger.Error(mess);
        }
    }
}
