﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects;


namespace ModemInterface
{
    public class ClientCommingSMSHostingData
    {
        public ClientCommingSMSHostingData(ClientCommingSMSHosting sms,int index)
        {
            SMS = sms;
            SubmitWS = 0;
            StatusDeleteSMS = 0;
            IndexMessage = index;
            if (sms.Message == "") SubmitWS = 1;//không submit lên WS tin nhắn ko có nội dung
        }
        /// <summary>
        /// Vị trí của sms trong sim
        /// </summary>
        public int IndexMessage { get; set; }

        public ClientCommingSMSHosting SMS { get; set; }

        /// <summary>
        /// 0: Chưa submit lên hoặc submit ko dc, 1: Submit lên WS thành công
        /// </summary>
        public int SubmitWS { get; set; }

        /// <summary>
        /// 0: chưa delete hoặc del chưa dc 1: SMS thất bại
        /// </summary>
        public int StatusDeleteSMS { get; set; }
    }
}
