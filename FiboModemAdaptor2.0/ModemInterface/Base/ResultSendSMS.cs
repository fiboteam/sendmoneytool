﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace ModemInterface
{
    public class ResultSendSMS  
    {
        /// <summary>
        /// trả về các trạng thái của modem sau khi gửi tin nhắn
        /// </summary>
        public ModemAdaptorStatus ModemStatus {get;set;}

        /// <summary>
        /// Trả về trạng thái của thiết bị lúc gửi tin
        /// </summary>
        public ModemDeviceStatus DeviceStatus { get; set; }

        /// <summary>
        /// Thông báo lỗi của modem nếu có
        /// </summary>
        public string ErrorsModem { get;set; }

        /// <summary>
        /// Trả về trạng thái của tin nhắn.
        /// </summary>
        public SMSStatus SMSStatus { get; set; }

        /// <summary>
        /// Trả về lỗi tin nhắn nếu có
        /// </summary>
        public string ErrorsSMS { get; set; }
        
        /// <summary>
        /// Trạng thái thành công hay thất bại khi gửi tin
        /// </summary>
        public bool StatusSend { get; set; }

        /// <summary>
        /// Chuỗi xml truyền vào nếu có
        /// </summary>
        public string xmlRequest { get; set; }

        /// <summary>
        /// Chuỗi xml trả về nếu có
        /// </summary>
        public string xmlResponse { get; set; }

        public void GetResult(ModemAdaptorStatus  adapterstatus, ModemDeviceStatus devicestatus, SMSStatus smsstatus , bool statussend)
        {
            this.ModemStatus = adapterstatus;
            this.DeviceStatus = devicestatus;
            this.SMSStatus = smsstatus;
            this.StatusSend = statussend;
            this.ErrorsModem = adapterstatus.ToString();
            this.ErrorsSMS = devicestatus.ToString();
        }
      
    }
}
