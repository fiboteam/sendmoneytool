﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessObjects;
using Utility;

namespace ModemInterface
{
    public abstract class GSMModem
    {
        public abstract SMSStatus SendSMS(string phoneNumber, string message, ContentType contentType);
        public abstract SMSStatus SendSMS(string phoneNumber, string message);
        public abstract DataTable GetSMS();
        public abstract List<SmsInfo> GetSMS_V2();

        public abstract SMSStatus SendFlashSMS(string phoneNumber, string message);
        public abstract SMSStatus SendConcatenatedSMS(string phoneNumber, string message, int totalsms);
        public abstract SMSStatus SendConcatenatedSMS(string phoneNumber, string message, ContentType contentType, int totalsms);
		public abstract SMSStatus SendByPDU(string message,string phone,SMSHostingContentType contentype,SMSHostingMessageType messagetype);
        public abstract void Dispose();
        public abstract bool IsOpen();

        public abstract bool Reset();

        public abstract bool RestartCOM(string message,string success);

        public abstract bool DeleteMessage(string p);



        public abstract string CheckMoney(string Message);



        public abstract string SendCMGL(string message, int timeOut, bool usePDU=false);
    }
}
