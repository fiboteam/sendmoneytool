﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Utility;
using System.Data;
using BusinessObjects;
using System.Net.Mail;


namespace ModemInterface
{
    public abstract class BaseSupportSMS :BaseLog
    {
        /// <summary>
        /// Chỉ dùng cho smshosting
        /// </summary>
        public GSMModem smsSender = null;
        /// <summary>
        /// Lấy response từ 1 đường link
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string GetStringFromDestinationWeb(string url)
        {
            // Open a connection
            HttpWebRequest WebRequestObject = (HttpWebRequest)HttpWebRequest.Create(url);
            WebRequestObject.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)";
            WebRequestObject.Timeout = 60000;

            // Request response:
            using (WebResponse Response = WebRequestObject.GetResponse())
            {

                // Open data stream:
                Stream WebStream = Response.GetResponseStream();

                // Create reader object:
                StreamReader Reader = new StreamReader(WebStream);

                // Read the entire stream content:
                string PageContent = Reader.ReadToEnd();

                // Cleanup
                Reader.Close();
                WebStream.Close();
                Response.Close();

                return PageContent;
            }
        }
        /// <summary>
        /// Gửi cảnh báo bằng cách dùng modem hiện tại
        /// </summary>
        /// <param name="message"></param>
        /// <param name="phones"></param>
        /// <param name="ModemAdaptor"></param>
        private SMSStatus SendSMSAlertForOwner(string message, string phones, ModemAdaptor ModemAdaptor, int countSMSSuccess=0)
        {
            try
            {
                if (smsSender.IsOpen() == true)
                {
                    return smsSender.SendConcatenatedSMS(GetstandardPhoneNumber(phones), message, ContentType.Text, (int)(message.Length / 160));
                    
                }
            }
            catch (Exception ex) { logger.Error("MessageError: " + ex.Message); }
            return SMSStatus.SentFailed;

        }

        /// <summary>
        /// Gửi cảnh báo bằng cách dùng modem hiện tại
        /// </summary>
        /// <param name="message"></param>
        /// <param name="phones"></param>
        /// <param name="ModemAdaptor"></param>
        private void SendSMSAlertWithModem(string message, string phones, ModemAdaptor ModemAdaptor)
        {
            try
            {
                if (smsSender.IsOpen() == true)
                {
                    string[] listPhoneNumber;
                    SMSStatus status = SMSStatus.SendMTFailed;
                    if (phones == "")
                        phones = GFunction.GetAppConfigParameter("ListAlertPhoneNumber");
                    listPhoneNumber = phones.Split(';');
                    foreach (string phoneNumber in listPhoneNumber)
                    {
                        int length = message.Length;
                        status = smsSender.SendConcatenatedSMS(GetstandardPhoneNumber(phoneNumber), message, ContentType.Text,(int)(message.Length/160));
                    }
                }
            }
            catch (Exception ex) { logger.Error("MessageError: " + ex.Message); }
            finally
            {
                //if (smsSender != null)
                //    smsSender.Dispose();
            }

        }

        /// <summary>
        /// Dùng thêm dấu + trước số dt vì một số sim ko dung được
        /// </summary>
        /// <param name="oldPhone"></param>
        /// <returns></returns>
        public string GetstandardPhoneNumber(string oldPhone)
        {
            bool standardPhoneNumber = Convert.ToBoolean(GFunction.GetAppConfigParameter("StandardPhoneNumber"));
            if (standardPhoneNumber)
            {
                string newPhone = "";
                string firstchar = oldPhone.Substring(0, 1);
                if (firstchar == "0" || firstchar == "+")
                {
                    newPhone = oldPhone;
                }
                else
                {
                    newPhone = "+" + oldPhone;
                }
                return newPhone;
            }
            return oldPhone;
        }

        /// <summary>
        /// Hàm gửi tin nhắn cảnh báo với web admin fibo
        /// </summary>
        /// <param name="smsMessage"></param>
        /// <returns></returns>
        private Boolean SendSMSAlertWithFiboAccount(string smsMessage)
        {
            try
            {
                string phones = GFunction.GetAppConfigParameter("ListAlertPhoneNumber");
                string[] listPhoneNumber = phones.Split(';');
                foreach (string phoneNumber in listPhoneNumber)
                {
                    if (phoneNumber != "")
                    {
                        string sms = this.GetStringFromDestinationWeb("http://center.fibosms.com/Service.asmx/SendSMS?clientNo=CL2449&clientPass=fibosms@admin&phoneNumber=" + phoneNumber + "&smsMessage=" + smsMessage + "&smsGUID=-1&serviceType=1");
                        if (sms.IndexOf("200") == -1) return false;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public void WriteLog(SMS sms)
        {
            try
            {
                if (Convert.ToBoolean(GFunction.GetAppConfigParameter("AutoSaveLogSMS")))
                {
                    if (!Directory.Exists("Log"))
                    {
                        Directory.CreateDirectory("Log");
                    }
                    TextWriter tw = new StreamWriter(String.Format("Log\\{0:yyyy-MM-dd}.{1}", DateTime.Now, "txt"), true);
                    tw.WriteLine(String.Format("Date: {0} SMSID:{1} Status:{2} SendingTime:{3}\r\n", DateTime.Now.ToString(), sms.ID, sms.SMSStatus.ToString(), sms.SendingTime));
                    tw.Close();
                }
            }
            catch (Exception)
            {
            }
        }

        public void WriteLog(IncommingSMS incomSMS, string path,string result)
        {
            try
            {
                if (Convert.ToBoolean(GFunction.GetAppConfigParameter("AutoSaveLogSMS")))
                {
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }                                          
                    TextWriter tw = new StreamWriter(String.Format(path+"\\{0:yyyy-MM-dd}.{1}", DateTime.Now, "txt"), true);
                    
                    string str = string.Format(
                    "Date: {0}-UpdateMO: {1}\n {2}",
                    DateTime.Now.ToString(),
                    result,
                    incomSMS.URLPushSMSToClient
                    );

                    tw.WriteLine(str);
                    tw.Close();
                }
            }
            catch (Exception)
            {

            }
        }
        public void WriteMessageLog(string mess)
        {
            try
            {
                if (Convert.ToBoolean(GFunction.GetAppConfigParameter("AutoSaveLogSMS")))
                {
                    if (!Directory.Exists("Log"))
                    {
                        Directory.CreateDirectory("Log");
                    }
                    TextWriter tw = new StreamWriter(String.Format("Log\\{0:yyyy-MM-dd}.{1}", DateTime.Now, "txt"), true);
                    tw.WriteLine(mess );
                    tw.Close();
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Hỗ trợ built tin nhắn tới của modem
        /// </summary>
        /// <param name="dtrow"></param>
        /// <param name="ModemAdaptor"></param>
        /// <returns></returns>
        public ClientCommingSMSHosting BuiltClientCommingSMS(DataRow dtrow, ModemAdaptor ModemAdaptor, long ServiceTypeID)
        {
            ClientCommingSMSHosting sms = new ClientCommingSMSHosting();
            sms.CheckDate = DateTime.Now;
            sms.ID = -1;
            sms.ClientCommingSMSHostingStatus = ClientCommingSMSHostingStatus.JustReceived;
            sms.ClientID = ModemAdaptor.ClientID;
            if (ModemAdaptor.ClientID == 0) sms.ClientID = 1381;
            sms.CreatedDate = DateTime.Now;
            sms.IsSentMail = IsSentMail.DeActive;
            sms.Message = dtrow["Message"].ToString();
            sms.PhoneNumber = dtrow["PhoneNumber"].ToString();
            sms.ProcessingTime = 0;
            sms.SentDate = DateTime.Now;
            sms.ServiceTypeID = (int)ServiceTypeID;
            sms.UpdatedDate = DateTime.Now;
            sms.GetByModemAdaptor = (int)ModemAdaptor.ID;
            return sms;
        }

        public ClientCommingSMSHosting BuiltClientCommingSMS_V2(IncommingSMS incommingSMS, ModemAdaptor ModemAdaptor, long ServiceTypeID)
        {
            ClientCommingSMSHosting sms = new ClientCommingSMSHosting();
            sms.CheckDate = DateTime.Now;
            sms.ID = -1;
            sms.ClientCommingSMSHostingStatus = ClientCommingSMSHostingStatus.JustReceived;
            sms.ClientID = ModemAdaptor.ClientID;
            if (ModemAdaptor.ClientID == 0) sms.ClientID = 1381;
            sms.CreatedDate = DateTime.Now;
            sms.IsSentMail = IsSentMail.DeActive;
            sms.Message = incommingSMS.Content;
            sms.PhoneNumber = incommingSMS.PhoneNumber.Replace("+", "");
            //sms.PhoneNumber = incommingSMS.PhoneNumber;
            sms.ProcessingTime = 0;
            sms.SentDate = DateTime.Now;
            sms.ServiceTypeID = (int)ServiceTypeID;
            sms.UpdatedDate = DateTime.Now;
            sms.GetByModemAdaptor = (int)ModemAdaptor.ID;
            return sms;
        }

        public SMS BuitlSMSAlert(string message, string phonenumber, ModemAdaptor ModemAdaptor, int ServiceTypeID)
        {
            SMS sms = new SMS();
            sms.ID = -1;
            sms.ClientID = ModemAdaptor.ClientID;
            if (ModemAdaptor.ClientID == 0) sms.ClientID = 1381;
            sms.PhoneNumber = phonenumber;
            sms.Message = message;
            sms.SMSGUID = "-1";
            sms.SMSStatus = SMSStatus.Pending;
            sms.CreatedDate = DateTime.Now;
            sms.SentDate = DateTime.Now;
            sms.UpdatedDate = DateTime.Now;
            sms.ServiceTypeID = ServiceTypeID;
            sms.SendingTime = 0;
            sms.InCommingSMSID = "-1";
            sms.SenderName = "n/a";
            sms.CountryCode = "";
            sms.TelCoID = 0;
            sms.MessageType = SMSHostingMessageType.NormalSMS;
            sms.ContentType = SMSHostingContentType.Text;
            sms.IsSpam = IsSpam.Active;
            sms.DelayDate = DateTime.Now;
            sms.Price = 250;
            sms.IsSubstract = IsSubstract.NotSubstract;
            sms.IsNewService = IsNewService.Active;
            sms.PushSMSStatus = PushSMSStatus.JustReceived;
            sms.ProcessingTimePushStatus = 0;
            sms.TotalSMS = 1;
            sms.ShortCreatedDate = DateTime.Now.Year * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day;
            sms.PhoneNumberStatus = PhoneNumberStatus.Success;
            sms.SpecifiedModem = -1;
            sms.Remark = "";
            return sms;
        }

        /// <summary>
        /// Hàm cảnh báo nếu bị lỗi chuẩn. Sử dụng nhiều phương thức khác nhau nếu như không thành công
        /// </summary>
        /// <param name="smsMessage"></param>
        /// <param name="ModemAdaptor"></param>
        public void SendSMSAlert(string smsMessage)
        {//--> chưa test SendSMSAlertErrorsWithWS
            this.logger.Error(smsMessage);
            //cố gắng gửi bằng nhiều phương thức
               if (SendSMSAlertWithFiboAccount(smsMessage) == false)
                    SendSMSAlertWithModem(smsMessage + " Note: Send By Modem", "0935098995", ((BaseSMSProcessFactory)this).ModemAdaptor);

        }
        public string SendSMSAlert(string smsMessage,string Phone)
        {//--> chưa test SendSMSAlertErrorsWithWS
            if( SendSMSAlertForOwner(smsMessage, Phone, ((BaseSMSProcessFactory)this).ModemAdaptor)== SMSStatus.SentFailed)
            {
                string sms = this.GetStringFromDestinationWeb("http://center.fibosms.com/Service.asmx/SendSMS?clientNo=CL7625&clientPass=a1d2m3i4n5&phoneNumber=" + Phone + "&smsMessage=" + smsMessage + "&smsGUID=-1&serviceType=1");
                return sms;
            }
            return "xong SendSMSAlert";

        }
        public static bool sendMail(string from, string pass, string to, string subject, string body)
        {
            bool result = false;

            try
            {
                SmtpClient host = new SmtpClient("smtp.gmail.com", 587);
                host.EnableSsl = true;
                host.DeliveryMethod = SmtpDeliveryMethod.Network;
                host.UseDefaultCredentials = false;
                host.Credentials = new NetworkCredential(from, pass);

                MailMessage mail = new MailMessage(from, to, subject, body);
                mail.IsBodyHtml = true;
                host.Send(mail);

                result = true;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
