﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModemInterface
{
    public enum ModemDeviceStatus : short
    {
        DisableNetWork = 0, //Disable network registration unsolicited result code
        EnableNetWork = 1, //Enable network registration unsolicited result code
        SimNotRegistry = 2,    //Enable network registration and location information unsolicited result code
        SimBlocked = 3,     //Sim bị khóa
        CantSendAT = 4,
        CantSendCMGF = 5,
        WaitTooLong = 6,
        OtherProgramUsingCOM = 7,
        PortComNotExist = 8,
        CantSendCSMS = 9
    }
    public enum ContentType : short
    {
        Text = 0,
        Ringtone = 1,
        Logo = 2,
        CLIICON = 3,
        PictureMessage = 4,
        CuteTextUTF16 = 5,
        MMSNotify = 6,
        WAPBookmark = 7,
        WAPIndicator = 8,
        BussinessCard = 9,
        VCalendar = 10,
        Unicode = 11,
        ConcatenatedSMS = 15
    }
    public enum StatusUpdateMO : short
    {
        UpdateSuccess = 1,
        UpdatedSuccessCantDelMO = 2,
        UpdateSMSFail = 3
    }
}
