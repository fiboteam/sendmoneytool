﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace ModemInterface
{
    public partial class TestManagerFrom : Form
    {
        public Timer mTimer = new Timer();
        public BackgroundWorker sendMT;
        public BackgroundWorker getMO;
        BaseSMSProcessFactory Base;
        public bool ViewCommand = false;
        public TestManagerFrom(BackgroundWorker sendMT, BackgroundWorker getMO, BaseSMSProcessFactory BaseSMS,bool ViewCommand)
        {
            InitializeComponent();
            this.ViewCommand = ViewCommand;
            this.sendMT = sendMT;
            this.getMO = getMO;
            Base = BaseSMS;
            mTimer.Interval = 500;
            this.Text = "Waitting... To End Main Proccess";
            PanelControl.Enabled = false;
            mTimer.Tick += mTimer_Tick;
            mTimer.Start();
            
            
        }

        void mTimer_Tick(object sender, EventArgs e)
        {
            if (!sendMT.IsBusy && !getMO.IsBusy && PanelControl.Enabled == false)
            {
                Base.PrepareModemAdapter();
                PanelControl.Enabled = true;
                List<object> list = new List<object>();
                list.Add(Base);
                list.Add(this);
                list.Add(ViewCommand);
                object obj = GetNewType(ConfigurationManager.AppSettings["ClassTest"], list.ToArray());
                if (obj == null)
                {
                    MessageBox.Show("Kiểm tra config ClassTest", "Thông báo", MessageBoxButtons.OK);

                }
                PanelControl.Controls.Add((Control)obj);
                this.Text = "Test Manager Control";
            }
        }
        public object GetNewType(string classname, object[] objs)
        {
            Assembly a = Assembly.Load(ConfigurationManager.AppSettings["ImplementDLL"]);
            Type[] types = a.GetTypes();
            foreach (var item in types)
            {
                if (item.Name == classname)
                {
                    object newInstance = Activator.CreateInstance(item, objs);
                    return newInstance;
                }
            }
            return null;
        }

    }
}
