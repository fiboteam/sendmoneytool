﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMSPDULib
{
    public class UTFLib
    {
        static string HexChars = "0123456789ABCDEF";
        static string VietChars = "á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ|đ|é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|í|ì|ỉ|ĩ|ị|ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|ý|ỳ|ỷ|ỹ|ỵ";

        public static byte[] GetBytes(string source, int fromBase)
        {
            List<byte> bytes = new List<byte>();

            for (int i = 0; i < source.Length / 2; i++)
                bytes.Add(Convert.ToByte(source.Substring(i * 2, 2), fromBase));

            return bytes.ToArray();
        }

        public static bool CheckHexChar(string source)
        {
            foreach (char i in source)
            {
                if (!HexChars.Contains(i.ToString())) return false;
            }
            return true;
        }

        public static bool CheckVietChar(string dSource)
        {
            foreach (char i in dSource)
            {
                if (VietChars.Contains(i.ToString())) return true;
            }
            return false;
        }

        public static string DecodeUCS2(string source)
        {
            try
            {
                if (!CheckHexChar(source)) return source;
                byte[] bytes = GetBytes(source, 16);
                string sDecode = Encoding.BigEndianUnicode.GetString(bytes);
                if (!CheckVietChar(sDecode)) return source;
                return sDecode;
            }
            catch (Exception ex)
            {
                return source;
            }
        }
    }
}
