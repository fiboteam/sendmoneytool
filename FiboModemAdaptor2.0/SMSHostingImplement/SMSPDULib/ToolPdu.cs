﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMSPDULib;

namespace SMSPDULib
{
    public class ToolPdu
    {
        /// <summary>
        /// Tạo tin nhắn dạng PDU
        /// Trả về [Độ dài, Pdu Code]
        /// </summary>
        /// <param name="message">Nội dung tin nhắn</param>
        /// <param name="phoneNumber">Số điện thoại</param>
        /// <param name="messageType">
        /// <para>Flash: gửi tin nhắn dạng Flash</para>
        /// <para>Normal: gửi tin nhắn dạng bình thường</para>
        /// </param>
        /// <param name="ContentType">
        /// <para>Text: không dấu</para>
        /// <para>Unicode: Có dấu</para>
        /// </param>
        /// <returns></returns>
        public static Dictionary<string, string> EncodeSMS(string message, string phoneNumber, string messageType = "Normal", string contentType = "Text")
        { 
            try
            {
                Dictionary<string, string> listSMS = new Dictionary<string, string>();

                phoneNumber = phoneNumber.Replace(" ", "");
                if (phoneNumber.StartsWith("84"))
                    phoneNumber = phoneNumber.Remove(0, 2).Insert(0, "0");
                else if (phoneNumber.StartsWith("+84"))
                    phoneNumber = phoneNumber.Remove(0, 3).Insert(0, "0");
                //else if (!phoneNumber.StartsWith("0"))
                //    phoneNumber = phoneNumber.Insert(0, "0");

                SMS sms = new SMS();
                sms.Direction = SMSDirection.Submited;
                sms.PhoneNumber = phoneNumber;
                sms.ValidityPeriodFormat = SMSPDULib.ValidityPeriodFormat.Relative;
                sms.ValidityPeriod = new TimeSpan(255, 0, 0);
                sms.MessageEncoding = contentType.Equals("Unicode") ? SMS.SMSEncoding.UCS2 : SMS.SMSEncoding._7bit;
                sms.Message = message;
                sms.Flash = messageType.Equals("Flash") ? true : false;

                if ((sms.Message.Length <= 160 && sms.MessageEncoding == SMS.SMSEncoding._7bit) || (sms.Message.Length <= 70 && sms.MessageEncoding == SMS.SMSEncoding.UCS2))
                {
                    string pduCode = sms.Compose();
                    listSMS.Add("1: " + (pduCode.Length - 2) / 2, pduCode);
                }
                else
                {
                    string[] pduCode = sms.ComposeLongSMS();

                    for (int i = 0; i < pduCode.Length; i++)
                    {
                        listSMS.Add(i + 1 + ": " + (pduCode[i].Length - 2) / 2, pduCode[i]);
                    }
                }

                return listSMS;
            }
            catch(Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Decode Pdu
        /// <para>0:Total part</para>
        /// <para>1:Part</para>
        /// <para>2:PartID</para>
        /// <para>3:Số điện thoại</para>
        /// <para>4:Ngày nhận</para>
        /// <para>5:Nội dung tin nhắn</para>
        /// </summary>
        /// <param name="pduCode">Chuỗi Pdu</param>
        /// <returns></returns>
        public static List<string> DecodeSMS(string pduCode)
        {
            try
            {
                bool fetchSMS = false;
                SMS sms = new SMS();
                sms.Direction = SMSDirection.Received;

                SMSType smsType = SMSBase.GetSMSType(pduCode);

                if (smsType == SMSType.SMS)
                {
                    fetchSMS = SMS.Fetch(sms, ref pduCode);
                }
                else if (smsType == SMSType.StatusReport)
                {
                    SMSStatusReport.Fetch(sms, ref pduCode);
                }

                List<string> parms = new List<string>();
                parms.Add(sms.GetParamsSMSReceived(2).ToString());
                parms.Add(sms.GetParamsSMSReceived(1).ToString());
                parms.Add(sms.GetParamsSMSReceived(3).ToString());
                parms.Add(sms.PhoneNumber);
                parms.Add(sms.ServiceCenterTimeStamp.ToString("HH:mm:ss dd/MM/yyyy"));

                if (!fetchSMS)
                {
                    parms.Add(String.Empty);
                }
                else
                {
                    parms.Add(sms.Message);
                }

                return parms;
            }
            catch(Exception)
            {
                return null;
            }
        }
    }
}
