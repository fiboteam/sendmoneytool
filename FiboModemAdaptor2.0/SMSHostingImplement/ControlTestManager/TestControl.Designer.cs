﻿namespace ModemAdaptorMutilThreadV2
{
    partial class TestControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSend = new System.Windows.Forms.Button();
            this.grTestSend = new System.Windows.Forms.GroupBox();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbmessage = new System.Windows.Forms.Label();
            this.lbPhone = new System.Windows.Forms.Label();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.tbPhone = new System.Windows.Forms.TextBox();
            this.grAdvantage = new System.Windows.Forms.GroupBox();
            this.cbUSDcollection = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbEnd = new System.Windows.Forms.TextBox();
            this.lbend = new System.Windows.Forms.Label();
            this.btAtcommand = new System.Windows.Forms.Button();
            this.tbATcommand = new System.Windows.Forms.TextBox();
            this.lbATCommand = new System.Windows.Forms.Label();
            this.btExcuteUSD = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbUSDCommand = new System.Windows.Forms.TextBox();
            this.tbLog = new System.Windows.Forms.RichTextBox();
            this.grTestSend.SuspendLayout();
            this.grAdvantage.SuspendLayout();
            this.SuspendLayout();
            // 
            // btSend
            // 
            this.btSend.Location = new System.Drawing.Point(356, 51);
            this.btSend.Name = "btSend";
            this.btSend.Size = new System.Drawing.Size(97, 73);
            this.btSend.TabIndex = 3;
            this.btSend.Text = "Send";
            this.btSend.UseVisualStyleBackColor = true;
            this.btSend.Click += new System.EventHandler(this.btSend_Click);
            // 
            // grTestSend
            // 
            this.grTestSend.Controls.Add(this.cbType);
            this.grTestSend.Controls.Add(this.label4);
            this.grTestSend.Controls.Add(this.lbmessage);
            this.grTestSend.Controls.Add(this.lbPhone);
            this.grTestSend.Controls.Add(this.tbMessage);
            this.grTestSend.Controls.Add(this.tbPhone);
            this.grTestSend.Controls.Add(this.btSend);
            this.grTestSend.Dock = System.Windows.Forms.DockStyle.Top;
            this.grTestSend.Location = new System.Drawing.Point(0, 0);
            this.grTestSend.Name = "grTestSend";
            this.grTestSend.Size = new System.Drawing.Size(470, 138);
            this.grTestSend.TabIndex = 1;
            this.grTestSend.TabStop = false;
            this.grTestSend.Text = "Gửi tin thử";
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "SendText",
            "SendConcatenated",
            "SendConcatenatedStandar",
            "Wap/Push",
            "Flash"});
            this.cbType.Location = new System.Drawing.Point(347, 19);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(106, 21);
            this.cbType.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(270, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Loại tin nhắn:";
            // 
            // lbmessage
            // 
            this.lbmessage.AutoSize = true;
            this.lbmessage.Location = new System.Drawing.Point(21, 51);
            this.lbmessage.Name = "lbmessage";
            this.lbmessage.Size = new System.Drawing.Size(55, 13);
            this.lbmessage.TabIndex = 4;
            this.lbmessage.Text = "Nội Dung:";
            // 
            // lbPhone
            // 
            this.lbPhone.AutoSize = true;
            this.lbPhone.Location = new System.Drawing.Point(21, 24);
            this.lbPhone.Name = "lbPhone";
            this.lbPhone.Size = new System.Drawing.Size(57, 13);
            this.lbPhone.TabIndex = 3;
            this.lbPhone.Text = "Số Phone:";
            // 
            // tbMessage
            // 
            this.tbMessage.Location = new System.Drawing.Point(94, 51);
            this.tbMessage.Name = "tbMessage";
            this.tbMessage.Size = new System.Drawing.Size(247, 73);
            this.tbMessage.TabIndex = 2;
            this.tbMessage.Text = "";
            // 
            // tbPhone
            // 
            this.tbPhone.Location = new System.Drawing.Point(103, 19);
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(121, 20);
            this.tbPhone.TabIndex = 0;
            // 
            // grAdvantage
            // 
            this.grAdvantage.Controls.Add(this.cbUSDcollection);
            this.grAdvantage.Controls.Add(this.label2);
            this.grAdvantage.Controls.Add(this.tbEnd);
            this.grAdvantage.Controls.Add(this.lbend);
            this.grAdvantage.Controls.Add(this.btAtcommand);
            this.grAdvantage.Controls.Add(this.tbATcommand);
            this.grAdvantage.Controls.Add(this.lbATCommand);
            this.grAdvantage.Controls.Add(this.btExcuteUSD);
            this.grAdvantage.Controls.Add(this.label1);
            this.grAdvantage.Controls.Add(this.tbUSDCommand);
            this.grAdvantage.Controls.Add(this.tbLog);
            this.grAdvantage.Location = new System.Drawing.Point(3, 144);
            this.grAdvantage.Name = "grAdvantage";
            this.grAdvantage.Size = new System.Drawing.Size(464, 277);
            this.grAdvantage.TabIndex = 2;
            this.grAdvantage.TabStop = false;
            this.grAdvantage.Text = "Mở rộng";
            // 
            // cbUSDcollection
            // 
            this.cbUSDcollection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUSDcollection.FormattingEnabled = true;
            this.cbUSDcollection.Items.AddRange(new object[] {
            "*101#",
            "*102#",
            "*100*#"});
            this.cbUSDcollection.Location = new System.Drawing.Point(108, 23);
            this.cbUSDcollection.Name = "cbUSDcollection";
            this.cbUSDcollection.Size = new System.Drawing.Size(121, 21);
            this.cbUSDcollection.TabIndex = 0;
            this.cbUSDcollection.SelectedIndexChanged += new System.EventHandler(this.cbUSDcollection_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "USD Collection";
            // 
            // tbEnd
            // 
            this.tbEnd.Location = new System.Drawing.Point(328, 93);
            this.tbEnd.Name = "tbEnd";
            this.tbEnd.Size = new System.Drawing.Size(52, 20);
            this.tbEnd.TabIndex = 4;
            this.tbEnd.Text = "OK";
            // 
            // lbend
            // 
            this.lbend.AutoSize = true;
            this.lbend.Location = new System.Drawing.Point(296, 96);
            this.lbend.Name = "lbend";
            this.lbend.Size = new System.Drawing.Size(26, 13);
            this.lbend.TabIndex = 15;
            this.lbend.Text = "End";
            // 
            // btAtcommand
            // 
            this.btAtcommand.Location = new System.Drawing.Point(394, 91);
            this.btAtcommand.Name = "btAtcommand";
            this.btAtcommand.Size = new System.Drawing.Size(56, 23);
            this.btAtcommand.TabIndex = 5;
            this.btAtcommand.Text = "Excute";
            this.btAtcommand.UseVisualStyleBackColor = true;
            this.btAtcommand.Click += new System.EventHandler(this.btAtcommand_Click);
            // 
            // tbATcommand
            // 
            this.tbATcommand.Location = new System.Drawing.Point(100, 93);
            this.tbATcommand.Name = "tbATcommand";
            this.tbATcommand.Size = new System.Drawing.Size(172, 20);
            this.tbATcommand.TabIndex = 3;
            // 
            // lbATCommand
            // 
            this.lbATCommand.AutoSize = true;
            this.lbATCommand.Location = new System.Drawing.Point(18, 93);
            this.lbATCommand.Name = "lbATCommand";
            this.lbATCommand.Size = new System.Drawing.Size(68, 13);
            this.lbATCommand.TabIndex = 12;
            this.lbATCommand.Text = "ATCommand";
            // 
            // btExcuteUSD
            // 
            this.btExcuteUSD.Location = new System.Drawing.Point(328, 56);
            this.btExcuteUSD.Name = "btExcuteUSD";
            this.btExcuteUSD.Size = new System.Drawing.Size(122, 23);
            this.btExcuteUSD.TabIndex = 2;
            this.btExcuteUSD.Text = "ExcuteUSDCommand";
            this.btExcuteUSD.UseVisualStyleBackColor = true;
            this.btExcuteUSD.Click += new System.EventHandler(this.btExcuteUSD_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "USDCommand(*101#;*102#....):";
            // 
            // tbUSDCommand
            // 
            this.tbUSDCommand.Location = new System.Drawing.Point(183, 57);
            this.tbUSDCommand.Name = "tbUSDCommand";
            this.tbUSDCommand.Size = new System.Drawing.Size(121, 20);
            this.tbUSDCommand.TabIndex = 1;
            // 
            // tbLog
            // 
            this.tbLog.Location = new System.Drawing.Point(6, 129);
            this.tbLog.Name = "tbLog";
            this.tbLog.Size = new System.Drawing.Size(452, 142);
            this.tbLog.TabIndex = 6;
            this.tbLog.Text = "";
            // 
            // TestControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grAdvantage);
            this.Controls.Add(this.grTestSend);
            this.Name = "TestControl";
            this.Size = new System.Drawing.Size(470, 424);
            this.grTestSend.ResumeLayout(false);
            this.grTestSend.PerformLayout();
            this.grAdvantage.ResumeLayout(false);
            this.grAdvantage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btSend;
        private System.Windows.Forms.GroupBox grTestSend;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbmessage;
        private System.Windows.Forms.Label lbPhone;
        private System.Windows.Forms.RichTextBox tbMessage;
        private System.Windows.Forms.TextBox tbPhone;
        private System.Windows.Forms.GroupBox grAdvantage;
        private System.Windows.Forms.Button btAtcommand;
        private System.Windows.Forms.TextBox tbATcommand;
        private System.Windows.Forms.Label lbATCommand;
        private System.Windows.Forms.Button btExcuteUSD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbUSDCommand;
        private System.Windows.Forms.RichTextBox tbLog;
        private System.Windows.Forms.TextBox tbEnd;
        private System.Windows.Forms.Label lbend;
        private System.Windows.Forms.ComboBox cbUSDcollection;
        private System.Windows.Forms.Label label2;
    }
}
