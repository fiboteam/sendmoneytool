﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ModemInterface;
using SMSHostingImplement;

namespace ModemAdaptorMutilThreadV2
{
    public partial class TestControl : UserControl
    {
        public SMSHostingProcess baseSMSProcessFactory;
        public TestControl(BaseSMSProcessFactory gsmmodem, TestManagerFrom parent,bool ViewCommand)
        {
            
            InitializeComponent();
            baseSMSProcessFactory = (SMSHostingProcess)gsmmodem;
            if(ViewCommand)
                baseSMSProcessFactory.ViewLogEvent += baseSMSProcessFactory_ViewLogEvent;
            cbType.SelectedIndex = 0;
            parent.FormClosing += TestControl_FormClosing;
        }

        void baseSMSProcessFactory_ViewLogEvent(string Message)
        {
            tbLog.Text += Message + "\n";
        }

        void TestControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            baseSMSProcessFactory.ViewLogEvent -= baseSMSProcessFactory_ViewLogEvent;
            baseSMSProcessFactory.EndConnectModemAdapter();
        }

        private void btSend_Click(object sender, EventArgs e)
        {
            btSend.Enabled = false;
            if (tbMessage.Text == "")
            {
                MessageBox.Show("Dien noi dung tin nhan");
                return;
            }
            if (tbPhone.Text == "")
            {
                MessageBox.Show("Dien so phone");
                return;
            }
            if (cbType.SelectedIndex == 0)
                SendText();
            else if (cbType.SelectedIndex == 1)
                SendConcatenatedSMS();
            else if (cbType.SelectedIndex == 2)
                SendConcatenatedSMSStandar();
            else if (cbType.SelectedIndex == 3)
                SendWappush();
            else if (cbType.SelectedIndex == 4)
                SendFlash();
            btSend.Enabled = true;
        }

        public void SendConcatenatedSMS()
        {
            MessageBox.Show(((GSMModemSMSHosting)baseSMSProcessFactory.smsSender).SendConcatenatedSMS(tbPhone.Text, tbMessage.Text,(int)((tbMessage.Text.Length-1)/160)+1).ToString());
        }

        public void SendConcatenatedSMSStandar(int countSMSSuccess=0)
        {
            MessageBox.Show(((GSMModemSMSHosting)baseSMSProcessFactory.smsSender).SendConcatenatedSMSStandard(tbPhone.Text, tbMessage.Text, ContentType.Text, ((int)((tbMessage.Text.Length - 1) / 153)) + 1, ref countSMSSuccess).ToString());
        }

        public void SendWappush()
        {
            MessageBox.Show( ((GSMModemSMSHosting)baseSMSProcessFactory.smsSender).SendWapPush(tbPhone.Text, tbMessage.Text).ToString());
        }

        public void SendFlash()
        {
            tbLog.Text += baseSMSProcessFactory.smsSender.SendFlashSMS(tbPhone.Text, tbMessage.Text).ToString() + "\n";
        }
        public void SendText()
        {
            tbLog.Text += baseSMSProcessFactory.smsSender.SendSMS(tbPhone.Text, tbMessage.Text).ToString() + "\n";
        }

        private void btExcuteUSD_Click(object sender, EventArgs e)
        {
            btExcuteUSD.Enabled = false;
            if (tbUSDCommand.Text != "")
            {
                tbLog.Text += ((GSMModemSMSHosting)baseSMSProcessFactory.smsSender).SendCUSD(tbUSDCommand.Text.TrimEnd()) + "\n";
            }
            btExcuteUSD.Enabled = true;
        }

        private void btAtcommand_Click(object sender, EventArgs e)
        {
            btAtcommand.Enabled = false;
            if (tbATcommand.Text != "")
            {
                ((GSMModemSMSHosting)baseSMSProcessFactory.smsSender).SendCommand(tbATcommand.Text.TrimEnd(), tbEnd.Text);
            }
            btAtcommand.Enabled = true;
        }

        private void cbUSDcollection_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbUSDcollection.SelectedItem != null && cbUSDcollection.SelectedItem.ToString() != "")
                tbUSDCommand.Text = cbUSDcollection.SelectedItem.ToString();
        }


    }
}
