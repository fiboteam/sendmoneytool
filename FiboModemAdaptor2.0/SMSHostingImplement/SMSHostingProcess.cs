﻿using System;
using System.Configuration;
using System.Net;
using Utility;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using ModemInterface;
using BusinessObjects;
using log4net;
using ModemInterface.ModemAdaptorService;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using SMSPDULib;
using System.Web;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SMSHostingImplement
{
    public class SMSHostingProcess : BaseSMSProcessFactory
    {
        protected bool SendNormalSMS = true;
        protected bool isSpecialPDU = false;

        //số lần lỗi kết nối
        protected int errorNetWork = 0;

        //số lần lỗi chung của modem
        protected int error = 0;

        //số lần lỗi hiện tại của thiết bị
        protected int ErrorPrepareDevice = 0;

        //Mã số lần khởi tạo bị lỗi chấp nhận dc
        protected const int NumErrorPrepareMax = 50;

        protected bool AlertNetWork = false;
        protected DateTime DateErrorNetwork = DateTime.Now;

        //số tin hiện bị lỗi trong sim
        private const int MaxNumMOSMSCantDel = 50;

        //Gửi tin một lần báo lỗi ko xóa tin trên sim được
        protected bool AlertSMSCantDel = true;

        //số lần get Mo lên bị lỗi
        protected const int MaxNumErrorsGetMO = 30;

        protected int NumErrorsGetMO = 0;
		private int TotalWait { get; set; }

        public SMSHostingProcess(string ModemName, string UserName, string PassWord, bool IsGatewayPro, ILog logger)
            : base(ModemName, UserName, PassWord, IsGatewayPro, logger)
        {

            SendNormalSMS = Convert.ToBoolean(ConfigurationManager.AppSettings["SendNormalSMS"]);
            //chỉ hỗ trợ gửi tuần tự
            SupportSendList = false;
            SupportSendSequentially = true;
			TotalWait = int.Parse(ConfigurationManager.AppSettings["TotalWait"]);
        }

        /// <summary>
        /// Khởi tạo modemAdaptor bị lỗi quá 5 phút => mở port 30 lần không dc sẽ gửi tin nhắn cảnh báo
        /// </summary>
        /// <returns></returns>
        public override string PrepareModemAdapter()
        {
            string result = "";
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                EndConnectModemAdapter();
                SMSList = null;//xoa het list sms cu
                smsSender = new GSMModemSMSHosting();
                ((GSMModemSMSHosting)smsSender).PortCore.WriteATCommandEvent += ViewLogHandle;
                ((GSMModemSMSHosting)smsSender).OpenPort(ModemAdaptor.ComPort, ModemAdaptor.BaudRate, ModemAdaptor.Timeout);
                bool check = smsSender.IsOpen();
                if (check && ((GSMModemSMSHosting)smsSender).modemDeviceStatus == ModemDeviceStatus.EnableNetWork)
                {
                    ErrorPrepareDevice = 0;
                }
                else
                {
                    result = "Lỗi " + ((GSMModemSMSHosting)smsSender).modemDeviceStatus;
                    ErrorPrepareDevice++;
                }
                if (ErrorPrepareDevice >= NumErrorPrepareMax)
                {
                    SendSMSAlert("Khoi tao Modem " + ModemAdaptor.ModemAdaptorName + " that bai " + ErrorPrepareDevice + " lan. Loi" + ((GSMModemSMSHosting)smsSender).modemDeviceStatus.ToString());
                    ErrorPrepareDevice = 0;
                }
                SMSProcessStatus = SMSProcessStatus.Connecting;
                stopwatch.Stop();
                if (ErrorPrepareDevice >= 25 && result.Contains(ModemDeviceStatus.OtherProgramUsingCOM.ToString()))
                {
                    SendSMSAlert("Soft " + ModemAdaptor.ModemAdaptorName + " da restart do loi " + ModemDeviceStatus.OtherProgramUsingCOM.ToString());
                    string arguments = string.Empty;
                    string[] args = Environment.GetCommandLineArgs();
                    for (int i = 1; i < args.Length; i++) // args[0] is always exe path/filename
                        arguments += args[i] + " ";
                    Process.Start(Application.ExecutablePath, arguments);

                }
                ViewLogHandle("Thời gian chạy hàm PrepareModemAdapter:" + stopwatch.Elapsed);
                return result;
            }
            catch (Exception)
            {
                stopwatch.Stop();
                ViewLogHandle("Thời gian chạy hàm PrepareModemAdapter khi lỗi:" + stopwatch.Elapsed);
                SMSProcessStatus = SMSProcessStatus.Disconnect;
                return "Modem kết nối bị lỗi";
            }
        }

        public override bool EndConnectModemAdapter()
        {//có thể send ESC command ở đây
            try
            {
                if (smsSender != null)
                {
                    ((GSMModemSMSHosting)smsSender).PortCore.WriteATCommandEvent -= ViewLogHandle;
                    if (((GSMModemSMSHosting)smsSender).PortCore.serialPort != null) ((GSMModemSMSHosting)smsSender).PortCore.ClosePort();
                    smsSender.Dispose();
                    smsSender = null;
                }
                SMSProcessStatus = SMSProcessStatus.Disconnect;
                ViewLogHandle("EndConnectModemAdapter");
                return true;
            }
            catch (Exception ex)
            {
                InsertLog("Errors EndConnectModemAdapter");
                InsertLog(ex);
                SMSProcessStatus = SMSProcessStatus.Disconnect;
                ViewLogHandle("Errors EndConnectModemAdapter");
                return false;
            }

        }
		/// <summary>
		/// Ham sent sms củ, có lỗi
		/// </summary>
		/// <param name="sms"></param>
		/// <param name="countSMSSuccess"></param>
		/// <returns></returns>
        public  ResultSendSMS SendMT_bk(BusinessObjects.SMS sms, ref int countSMSSuccess)
       {
            try
            {
                //ViewLogHandle("Đã vào hàm sendMT");
                //int countSMSSuccess = 0;
                if (SMSProcessStatus == SMSProcessStatus.Disconnect)
                {
                    PrepareModemAdapter();
                    ViewLogHandle("UpdateMO bị disconnect đã mở lại kết nối");
                }
                DateErrorNetwork = DateTime.Now;
                var result = new ResultSendSMS();

                if (smsSender != null && smsSender.IsOpen() == false)
                {
                    ViewLogHandle("SMSSender " + smsSender.IsOpen().ToString());
                    ResultSendSMS rsult = ProcessException(new ModemException(ModemAdaptorName, ((GSMModemSMSHosting)smsSender).modemDeviceStatus));
                    return rsult;
                }
                var status = SMSStatus.SentFailed;
                if (SendNormalSMS)
                    status = smsSender.SendSMS(sms.PhoneNumber, sms.Message, ((ContentType)((int)sms.ContentType)));
                else
                {
                    //ViewLogHandle("da vao ham gui tin");
                    ViewLogHandle(string.Format("SMS.TotalSMS: {0}", sms.TotalSMS));
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    if (sms.Message.Trim().Length <= 160 && !isSpecialPDU) 
                    {
                        status = smsSender.SendConcatenatedSMS(GetstandardPhoneNumber(sms.PhoneNumber), sms.Message.Trim().Length > 600 ? sms.Message.Trim().Substring(0, 600) : sms.Message.Trim(), ((ContentType)((int)sms.ContentType)), sms.TotalSMS);
                        #region NMHIEU
                        if(status==SMSStatus.SentSuccess)
                        {
                            countSMSSuccess = 1;
                        }
                        #endregion
                    }
                    else
                    {
                        //ViewLogHandle("Đã vào hàm gửi tin dài");
                        List<int> beforebalance = new List<int>();
                        //beforebalance = MyGetMoney();
                        status =
                            ((GSMModemSMSHosting)smsSender).SendConcatenatedSMSStandard(
                                GetstandardPhoneNumber(sms.PhoneNumber),
                                sms.Message.Trim().Length > 600
                                    ? sms.Message.Trim().Substring(0, 600)
                                    : sms.Message.Trim(), ((ContentType)((int)sms.ContentType)), sms.TotalSMS, ref countSMSSuccess);
                        if(status!=SMSStatus.SentSuccess)
                        {
                            ViewLogHandle("Gửi tin dài không thành công.");
                            List<int> afterbalance = new List<int>();
                            //afterbalance = MyGetMoney();
                            //SendMailAlertSendConcatenatedSMSStandardError(beforebalance, afterbalance, countSMSSuccess, sms.TotalSMS);
                            //ViewLogHandle("Đã gửi mail cảnh báo.");
                        }
                    }
                    stopwatch.Stop();
                    ViewLogHandle("Thời gian chạy hàm SendMT:" + stopwatch.Elapsed);
                }
                result.GetResult(ModemAdaptor.ModemAdaptorStatus, ModemDeviceStatus.EnableNetWork, status, true);
                if (status == SMSStatus.SentSuccess && UseTimeConfig)
                {
                    ViewLogHandle("Thread.Sleep: " + (TimeDelay * 1000));
                    Thread.Sleep(TimeDelay * 1000);
                    ViewLogHandle("Thread.Sleep END");
                    WriteMessageLog("CurrentSMS: " + CurrentSMS);
                    CurrentSMS++;
                    ViewLogHandle("Tổng tin hiện tại là: " + CurrentSMS);
                    if (CurrentSMS >= TotalSMSAccept && SendMailAlert == false)
                    {
                        AcceptSendSMS = false;
                        //SendMailAlert = sendMail("reportagentjob@gmail.com", "sql@admin", "vu.d@fibo.vn", "Cảnh Báo Giới Hạn Số Lượng Tin " + ModemAdaptor.ModemAdaptorName, "ModemAdaptorName:" + ModemAdaptor.ModemAdaptorName + " Số tin nhắn là: " + CurrentSMS + " Tổng số chấp nhận: " + TotalSMSAccept);
                        //sendMail("reportagentjob@gmail.com", "sql@admin", "it@fibo.vn", "Cảnh Báo Giới Hạn Số Lượng Tin " + ModemAdaptor.ModemAdaptorName, "ModemAdaptorName:" + ModemAdaptor.ModemAdaptorName + " Số tin nhắn là: " + CurrentSMS + " Tổng số chấp nhận: " + TotalSMSAccept);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                ViewLogHandle(ex.Message);
                ResultSendSMS rsult = ProcessException(ex);
                return rsult;
            }
        }

		public override ResultSendSMS SendMT(BusinessObjects.SMS sms, ref int countSMSSuccess)
		{
			try
			{
				//ViewLogHandle("Đã vào hàm sendMT");
				//int countSMSSuccess = 0;
				if (SMSProcessStatus == SMSProcessStatus.Disconnect)
				{
					PrepareModemAdapter();
					ViewLogHandle("UpdateMO bị disconnect đã mở lại kết nối");
				}
				DateErrorNetwork = DateTime.Now;
				var result = new ResultSendSMS();

				if (smsSender != null && smsSender.IsOpen() == false)
				{
					ViewLogHandle("SMSSender " + smsSender.IsOpen().ToString());
					ResultSendSMS rsult = ProcessException(new ModemException(ModemAdaptorName, ((GSMModemSMSHosting)smsSender).modemDeviceStatus));
					return rsult;
				}
				var status = SMSStatus.SentFailed;
				if (SendNormalSMS)
					status = smsSender.SendSMS(sms.PhoneNumber, sms.Message, ((ContentType)((int)sms.ContentType)));
				else
				{
					//ViewLogHandle("da vao ham gui tin");
					ViewLogHandle(string.Format("SMS.TotalSMS: {0}", sms.TotalSMS));
					var stopwatch = new Stopwatch();
					stopwatch.Start();

					Dictionary<string, bool> listSend = new Dictionary<string, bool>();

					status = smsSender.SendByPDU(sms.Message, sms.PhoneNumber, sms.ContentType, sms.MessageType);
					if(status==SMSStatus.SentSuccess)
					{
						countSMSSuccess = sms.TotalSMS;
					}
					stopwatch.Stop();

					stopwatch.Stop();
					ViewLogHandle("Thời gian chạy hàm SendMT:" + stopwatch.Elapsed);
				}
				result.GetResult(ModemAdaptor.ModemAdaptorStatus, ModemDeviceStatus.EnableNetWork, status, true);
				if (status == SMSStatus.SentSuccess && UseTimeConfig)
				{
					ViewLogHandle("Thread.Sleep: " + (TimeDelay * 1000));
					Thread.Sleep(TimeDelay * 1000);
					ViewLogHandle("Thread.Sleep END");
					WriteMessageLog("CurrentSMS: " + CurrentSMS);
					CurrentSMS++;
					ViewLogHandle("Tổng tin hiện tại là: " + CurrentSMS);
					if (CurrentSMS >= TotalSMSAccept && SendMailAlert == false)
					{
						AcceptSendSMS = false;
						//SendMailAlert = sendMail("reportagentjob@gmail.com", "sql@admin", "vu.d@fibo.vn", "Cảnh Báo Giới Hạn Số Lượng Tin " + ModemAdaptor.ModemAdaptorName, "ModemAdaptorName:" + ModemAdaptor.ModemAdaptorName + " Số tin nhắn là: " + CurrentSMS + " Tổng số chấp nhận: " + TotalSMSAccept);
						//sendMail("reportagentjob@gmail.com", "sql@admin", "it@fibo.vn", "Cảnh Báo Giới Hạn Số Lượng Tin " + ModemAdaptor.ModemAdaptorName, "ModemAdaptorName:" + ModemAdaptor.ModemAdaptorName + " Số tin nhắn là: " + CurrentSMS + " Tổng số chấp nhận: " + TotalSMSAccept);
					}
				}
				return result;
			}
			catch (Exception ex)
			{
				ViewLogHandle(ex.Message);
				ResultSendSMS rsult = ProcessException(ex);
				return rsult;
			}
		}

		

        public ResultSendSMS ProcessException(Exception exc)
        {
            ResultSendSMS result = new ResultSendSMS();
            result.ModemStatus = ModemAdaptorStatus.Active;
            result.SMSStatus = SMSStatus.SentFailed;
            result.StatusSend = false;

            DateTime dateTimeError = DateTime.Now;
            logger.Error("Help link: " + exc.HelpLink + ", Message: " + exc.Message + ", TypeError:" + exc.GetType().ToString());
            Type typeofException = exc.GetType();
            if (typeofException == typeof(UnauthorizedAccessException))
            {
                ModemAdaptor.CurrentErrorType = CurrentErrorType.OpenPortError;
                ModemAdaptor.ModemAdaptorStatus = ModemAdaptorStatus.Blocked;
                result.ModemStatus = ModemAdaptorStatus.Blocked;
                SendSMSAlert("Modem " + ModemAdaptor.ModemAdaptorName + " da stop vi bi loi UnauthorizedAccessException");
                logger.Error("Message: " + exc.Message);
            }
            else if (typeofException == typeof(NullReferenceException))
            {
                error++;
                result.ErrorsModem = "NullReferenceException";
                logger.Error("NullReferenceException");
                return result;
            }
            else if (exc.Message.Equals("Connection to database server failed.") ||
                exc.Message.Contains("Unable to connect to the remote server") || typeofException == typeof(WebException))//|| typeofException == typeof(System.Web.)
            {
                errorNetWork++;
                ModemAdaptor.CurrentErrorType = CurrentErrorType.ConnectionError;
                result.ErrorsModem = exc.Message;
            }
            else if (typeofException == typeof(SendATCommand) || typeofException == typeof(SendCPINCommand) ||
                typeofException == typeof(SendCMGFCommand))
            {
                error++;
                errorNetWork++;
                ModemAdaptor.CurrentErrorType = CurrentErrorType.ConnectModemError;

            }
            else if (typeofException == typeof(SendCMGSCommand) || typeofException == typeof(SendMessageType) ||
                typeofException == typeof(sendCTRLZ))
            {
                error++;
                dateTimeError = DateTime.Now;
                ModemAdaptor.CurrentErrorType = CurrentErrorType.SendSMSError;
            }
            else if (typeofException == typeof(System.IO.IOException))
            {
                dateTimeError = DateTime.Now;
                error++;
                ModemAdaptor.CurrentErrorType = CurrentErrorType.WrongCom;
                ModemAdaptor.ModemAdaptorStatus = ModemAdaptorStatus.Blocked;
                result.ModemStatus = ModemAdaptorStatus.Blocked;

                if (ModemAdaptor.ModemType == ModemType.PublicModem)
                    result.ModemStatus = ModemAdaptorStatus.Blocked;
                SendSMSAlert("Modem " + ModemAdaptor.ModemAdaptorName + " da stop vi bi loi IOException");
            }
            else if (typeofException == typeof(System.ArgumentException))
            {
                //hàm này đã bị xóa. Thử ghi lại lỗi nếu có trường hợp này
                logger.Error("Loi ArgumentException: " + exc.StackTrace);
            }
            else if (typeofException == typeof(ModemException))
            {
                var modemExc = (ModemException)exc;
                string message = "Modem: " + modemExc.ModemAdapterName + " have errors: " + modemExc.modemDeviceStatus;
                if (modemExc.modemDeviceStatus == ModemDeviceStatus.CantSendAT ||
                    modemExc.modemDeviceStatus == ModemDeviceStatus.CantSendCMGF ||
                    modemExc.modemDeviceStatus == ModemDeviceStatus.WaitTooLong ||
                    modemExc.modemDeviceStatus == ModemDeviceStatus.OtherProgramUsingCOM)//nếu xảy ra các trường hợp bị lỗi AT,AT+SMGF,thời gian gửi tin quá lâu
                {
                    error++;
                    logger.Error("Cac loi do modem: " + modemExc.modemDeviceStatus);
                    result.ErrorsModem = modemExc.modemDeviceStatus.ToString();
                    result.DeviceStatus = modemExc.modemDeviceStatus;
                }
                else if (modemExc.modemDeviceStatus == ModemDeviceStatus.DisableNetWork
                   || modemExc.modemDeviceStatus == ModemDeviceStatus.SimBlocked
                   || modemExc.modemDeviceStatus == ModemDeviceStatus.SimNotRegistry
                    || modemExc.modemDeviceStatus == ModemDeviceStatus.PortComNotExist)//nếu xảy ra các trường hợp bị lỗi bị khóa mạng, sim chưa đăng kí, sim bị khóa puk
                {
                    SendSMSAlert(message);
                    result.ModemStatus = ModemAdaptorStatus.Blocked;
                    result.DeviceStatus = modemExc.modemDeviceStatus;
                    logger.Error("Cac loi do modem: " + modemExc.modemDeviceStatus);
                }
            }
            else
            {
                logger.Error("Loi khac cua modem: " + typeofException + "\r\nDong bi loi" + exc.StackTrace);
                errorNetWork++;
                error++;
                ModemAdaptor.CurrentErrorType = CurrentErrorType.ConnectModemError;
            }
            if (error >= ModemAdaptor.NumberOfError)
            {
                if (exc.GetType() == typeof(ModemException))
                {
                    var modemExc = (ModemException)exc;
                    string message = "Modem: " + modemExc.ModemAdapterName + " have errors: " + modemExc.modemDeviceStatus + "Number errors: " + error;
                    SendSMSAlert(message);
                }
                else
                {
                    SendSMSAlert(ModemAdaptor.ModemAdaptorName + " have total " + error + ". Please config this modem");
                }
                error = 0;

            }
            if (errorNetWork > 1)
            {
                try
                {
                    IPAddress[] addClient = Dns.GetHostAddresses(ConfigurationSettings.AppSettings["PingClient"]);

                    if (addClient != null && addClient.Length > 0)
                    {
                        try
                        {
                            IPAddress[] addServer = Dns.GetHostAddresses(ConfigurationSettings.AppSettings["PingServer"]);
                            if (addServer != null && addServer.Length > 0)
                            {
                                errorNetWork = 0;
                                AlertNetWork = false;
                                if (ModemAdaptor.CurrentErrorType == CurrentErrorType.ConnectionError)
                                {
                                    ModemAdaptor.CurrentErrorType = CurrentErrorType.Normal;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message == "No such host is known")
                            {
                                AlertNetwork(" bi loi mang Server");
                            }
                            else
                            {
                                InsertLog(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message == "No such host is known")
                    {
                        AlertNetwork(" bi loi ADSL/Cap Quang");
                    }
                    else
                    {
                        InsertLog(ex);
                    }
                }
            }
            result.ErrorsModem = result.ModemStatus.ToString();
            result.ErrorsSMS = result.DeviceStatus.ToString();
            return result;

        }

        private void AlertNetwork(string MessageAlert)
        {
            if (errorNetWork >= int.Parse(ConfigurationSettings.AppSettings["TimesForAlertNetwork"]))
            {
                TimeSpan ts = DateTime.Now - DateErrorNetwork;
                if (!AlertNetWork && ts.Minutes >= 10)
                {
                    AlertNetWork = true;
                    DateErrorNetwork = DateTime.Now;
                    SendSMSAlert(ModemAdaptor.ModemAdaptorName + MessageAlert + " 10p");
                }
                else
                {
                    if (ts.Minutes >= 30)
                    {
                        DateErrorNetwork = DateTime.Now;
                        SendSMSAlert(ModemAdaptor.ModemAdaptorName + MessageAlert + " 30p");
                    }
                }

            }

        }

        /// <summary>
        /// Lấy tin nhắn cho dịch vụ private sẽ được hiểu là lấy tin nhắn cho dịch vụ đầu tiên trong list
        /// </summary>
        /// <param name="ServiceTypeID"></param>
        public override void GetMO(long ServiceTypeID)
        {
            try
            {
                //insert into tblClientCommingSMSHosting(ServiceTypeID,PhoneNumber,Message,ClientID,ClientCommingSMSHostingStatus,SentDate)
                //values(@ServiceTypeID,@PhoneNumber,@Message,@ClientID,0,getdate())                
                DataTable dtSMSList = smsSender.GetSMS();
                if (dtSMSList != null && dtSMSList.Rows.Count > 0)
                {
                    for (int i = 0; i < dtSMSList.Rows.Count; i++)
                    {
                        ClientCommingSMSHosting smsdata = BuiltClientCommingSMS(dtSMSList.Rows[i], ModemAdaptor, ServiceTypeID);
                        bool check = true;
                        foreach (var item in SMSListMO)
                        {   //nếu tin nhắn không có cùng id cùng số phone cùng nội dung đó trong smsdata
                            if (item.SMS.PhoneNumber == smsdata.PhoneNumber && smsdata.Message == item.SMS.Message && int.Parse(dtSMSList.Rows[i]["SMSID"].ToString()) == item.IndexMessage) check = false;
                        }
                        if (check)
                        {
                            var sms = new ClientCommingSMSHostingData(smsdata, int.Parse(dtSMSList.Rows[i]["SMSID"].ToString()));
                            SMSListMO.Add(sms);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                NumErrorsGetMO++;
                if (NumErrorsGetMO == MaxNumErrorsGetMO)
                {
                    SendSMSAlert(ModemAdaptor.ModemAdaptorName + " get MO bi loi " + NumErrorsGetMO + " lan");
                }
                ViewLogHandle("Số lần lỗi hàm GetMO: " + NumErrorsGetMO + " Lỗi: " + err.Message);
                logger.Error("GetMo Errors: Help link: " + err.HelpLink + ", Message: " + err.Message + ", TypeError:" + err.GetType());

            }
        }

        public override void GetMO_V2(long ServiceTypeID, ref string status)
        {
            try
            {
                //insert into tblClientCommingSMSHosting(ServiceTypeID,PhoneNumber,Message,ClientID,ClientCommingSMSHostingStatus,SentDate)
                //values(@ServiceTypeID,@PhoneNumber,@Message,@ClientID,0,getdate())                
                List<SmsInfo> dtSMSList = smsSender.GetSMS_V2();

                if (dtSMSList != null && dtSMSList.Count > 0)
                {
                    for (int i = 0; i < dtSMSList.Count; i++)
                    {
                        IncommingSMS sms = new IncommingSMS(dtSMSList[i].phonenumber, dtSMSList[i].message, dtSMSList[i].index);
                        sms.ServiceTypeID = ServiceTypeID;

                        if (!string.IsNullOrEmpty(dtSMSList[i].userdataheader) && dtSMSList[i].total > 1)
                        {
                            sms.TotalPart = dtSMSList[i].total;
                            for (int j = i + 1; j < dtSMSList.Count; j++)
                            {
                                try
                                {
                                    if (dtSMSList[j].userdataheader.Substring(0, dtSMSList[j].userdataheader.Length - 1) ==
                                        dtSMSList[i].userdataheader.Substring(0, dtSMSList[i].userdataheader.Length - 1) &&
                                        dtSMSList[j].phonenumber == dtSMSList[i].phonenumber)
                                    {
                                        sms.UpdateFullMessage(dtSMSList[j].message);
                                        //ViewLogHandle("Tin dài => "+sms.Content);
                                        sms.IndexMessageList.Add(new DeleteMessage() { IndexMessage = dtSMSList[j].index, IsDelete = false });
                                        dtSMSList.RemoveAt(j);
                                        j--;
                                    }
                                }
                                catch (Exception)
                                {
                                    status = "Message Not Null";
                                    ViewLogHandle("Có tin dài mà chưa đọc được khúc đuôi. đợi đọc tin lần sau ");

                                    //ServiceReference1.ServiceSoapClient services = new ServiceReference1.ServiceSoapClient();
                                    //services.SendSMS("CL7625", ConfigurationManager.AppSettings["Pass7625"].ToString().Trim(), "01656244909", "CaiThuocLa moi co tin nhan dai ma chua doc khuc duoi", Guid.NewGuid().ToString(), 1);
                                    return;
                                }

                            }
                            if (sms.IndexMessageList.Count == sms.TotalPart)
                            {
                                bool check = true;
                                if (dataIncomming != null || dataIncomming.Count > 0)
                                {
                                    foreach (var item in dataIncomming)
                                    {   //nếu tin nhắn không có cùng id cùng số phone cùng nội dung đó trong smsdata
                                        if (item.PhoneNumber == sms.PhoneNumber && sms.Content == item.Content) check = false;
                                    }
                                    if (check)
                                    {
                                        ViewLogHandle("Tin ngan => " + sms.Content);
                                        dataIncomming.Add(sms);
                                    }
                                }
                                
                                
                            }
                        }
                        else
                        {
                            //ViewLogHandle("Tin ngắn => " + sms.Content);
                            sms.TotalPart = 1;
                            bool check = true;
                            if (dataIncomming != null || dataIncomming.Count > 0)
                            {
                                foreach (var item in dataIncomming)
                                {   //nếu tin nhắn không có cùng id cùng số phone cùng nội dung đó trong smsdata
                                    if (item.PhoneNumber == sms.PhoneNumber && sms.Content == item.Content) check = false;
                                }
                                if (check)
                                {
                                    ViewLogHandle("Tin ngan => " + sms.Content);
                                    dataIncomming.Add(sms);
                                }
                            }
                            
                        }
                    }
                }
            }
            catch (Exception err)
            {
                NumErrorsGetMO++;
                if (NumErrorsGetMO == MaxNumErrorsGetMO)
                {
                    SendSMSAlert(ModemAdaptor.ModemAdaptorName + " get MO bi loi " + NumErrorsGetMO + " lan");
                }
                ViewLogHandle("Số lần lỗi hàm GetMO: " + NumErrorsGetMO + " Lỗi: " + err.Message);
                logger.Error("GetMo Errors: Help link: " + err.HelpLink + ", Message: " + err.Message + ", TypeError:" + err.GetType());
            }
        }

        public int UpdateMOErrors = 0;
        public override string UpdateMO(ClientCommingSMSHostingData sms)
        {
            WriteMessageLog("PhoneNumber: " + sms.SMS.PhoneNumber + " Message:" + sms.SMS.Message + "Datetime: " +
                DateTime.Now.ToLongDateString() + " Index: " + sms.IndexMessage);
            try
            {
                if (SMSProcessStatus == SMSProcessStatus.Disconnect)
                {
                    PrepareModemAdapter();
                    ViewLogHandle("UpdateMO bị disconnect đã mở lại kết nối");
                }
                long index = sms.IndexMessage;
                //nếu tin nhắn chưa được submit hoặc submit fal
                if (sms.SubmitWS == 0)
                {

                    var criteria = new ClientCommingSMSHostingCriteria
                                       {
                                           GetByModemAdaptorID = (int)ModemAdaptor.ID
                                       };
                    GetToken();
                    Login();
                    var request = new ClientCommingSMSHostingRequest
                                      {
                                          ClientCommingSMSHosting = sms.SMS,
                                          Action = "Insert",
                                          ClientTag = UserName,
                                          AccessToken = Token.AccessToken,
                                          Criteria = criteria,
                                          RequestId = Guid.NewGuid().ToString(),

                                      };
                    request.ClientCommingSMSHosting.ID = -1;
                    var response = managerAdaptor.SetClientCommingSMSHosting(request);
                    if (response.ClientCommingSMSHosting != null)//nếu submit tin thành công
                    {
                        bool delstatus = smsSender.DeleteMessage(index.ToString());
                        //nếu del thành công thì xóa tin nhắn đó luôn
                        for (int i = SMSListMO.Count - 1; i >= 0; i--)
                        {
                            sms.SubmitWS = 1;
                            if (sms == SMSListMO[i])
                            {
                                if (delstatus)
                                {
                                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["AllowCheckMoney"]))
                                    {
                                        if (SMSListMO[i].SMS.Message.StartsWith(ConfigurationManager.AppSettings["TemplateCheckMoney"]))
                                        {
                                            string[] listphone = ConfigurationManager.AppSettings["ListAllowCheckMoney"].Split(';');
                                            foreach (var item in listphone)
                                            {
                                                if (item != "" && SMSListMO[i].SMS.PhoneNumber.EndsWith(item))
                                                {
                                                    string message = smsSender.CheckMoney(ConfigurationManager.AppSettings["TemplateCheckMoney"]);
                                                    ViewLogHandle(message);
                                                    if (message.Contains("OK\r\n"))
                                                        message = message.Split(new[] { "OK\r\n" }, StringSplitOptions.None)[1];
                                                    ViewLogHandle(message + "SDT" + item);
                                                   ViewLogHandle( SendSMSAlert(message, "0" + item));
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    SMSListMO.Remove(SMSListMO[i]);

                                    return "Update tin nhắn đã thành công và xóa tin thành công";
                                }
                                SMSListMO[i].StatusDeleteSMS = 2;
                                if (SMSListMO.Count >= MaxNumMOSMSCantDel && AlertSMSCantDel)
                                {
                                    AlertSMSCantDel = false;
                                    SendSMSAlert(ModemAdaptor.ModemAdaptorName + " khong xoa dc MO. So tin trong sim la " + SMSListMO.Count + " tin");
                                    return "TurnOff";
                                }
                                return "Update tin nhắn đã thành công và không xóa được tin nhắn";
                            }
                        }

                        return "Update tin nhắn đã thành công và xóa tin thành công";
                    }
                    sms.SubmitWS = 0;
                    return "Update tin nhắn thất bại và chuyển về trạng thái chờ gửi lại" + response.Message + response.Acknowledge.ToString();
                }
                else //nếu tin nhắn đã cập nhật rồi mà chưa xóa được trong điện thoại
                {
                    bool delstatus = smsSender.DeleteMessage(index.ToString());

                    //nếu del thành công thì xóa tin nhắn đó luôn
                    for (int i = SMSListMO.Count - 1; i >= 0; i--)
                    {
                        if (sms == SMSListMO[i])
                        {
                            if (delstatus)
                            {
                                SMSListMO.Remove(SMSListMO[i]);
                                return "Đã update tin nhắn.Xóa lại tin nhắn thành công.";
                            }
                            SMSListMO[i].StatusDeleteSMS = 2;
                            if (SMSListMO.Count >= MaxNumMOSMSCantDel && AlertSMSCantDel)
                            {
                                AlertSMSCantDel = false;
                                SendSMSAlert(ModemAdaptor.ModemAdaptorName + " khong xoa dc MO. So tin trong sim la " + SMSListMO.Count + " tin");
                                return "TurnOff";
                            }

                            return "Đã update tin nhắn.Xóa lại tin nhắn này thất bại.";
                        }
                    }
                    return "Update lại tin nhắn thành công";
                }
            }
            catch (Exception exc)
            {
                UpdateMOErrors++;
                ReconnectWS();
                if (UpdateMOErrors >= 10)
                {
                    sendMail("reportagentjob@gmail.com", "sql@admin", "vu.d@fibo.vn", "Cảnh Báo Cập nhật MO bị lỗi" + DateTime.Now.ToString(), "ModemAdaptorName:" + ModemAdaptor.ModemAdaptorName);
                    sendMail("reportagentjob@gmail.com", "sql@admin", "it@fibo.vn", "Cảnh Báo Cập nhật MO bị lỗi" + DateTime.Now.ToString(), "ModemAdaptorName:" + ModemAdaptor.ModemAdaptorName);
                    UpdateMOErrors = 0;
                }
                logger.Error("Cập nhật MO bi loi");
                logger.Error("PhoneNumber: " + sms.SMS.PhoneNumber + " Message:" + sms.SMS.Message + "Datetime: " + DateTime.Now.ToLongDateString());
                ViewLogHandle("Số lần lỗi hàm Update: " + UpdateMOErrors + " Lỗi: " + exc.Message);
                WriteMessageLog("PhoneNumber: " + sms.SMS.PhoneNumber + " Message:" + sms.SMS.Message + "Datetime: " +
                                DateTime.Now.ToLongDateString());
                logger.Error("Help link: " + exc.HelpLink + ", Message: " + exc.Message + ", TypeError:" + exc.GetType());
                return "Update xảy ra lỗi";
            }

        }

		

		public override bool SendMoneyWithoutUSD(string message,string phone, ref string content)
		{
			try
			{
				string statusPrepareModem = "";
				string source = "";
				if (smsSender == null)
				{
					statusPrepareModem = PrepareModemAdapter();
				}

				if (statusPrepareModem != "")
				{
					ViewLogHandle("GetSMSCanSent have error: " + statusPrepareModem);
					content = statusPrepareModem;
					return false;
				}

				BusinessObjects.SMS sms = new BusinessObjects.SMS();
				sms.Message=message;
				sms.PhoneNumber = phone;
				sms.ContentType = SMSHostingContentType.Text;
				sms.MessageType=SMSHostingMessageType.NormalSMS;
				sms.TotalSMS = 1;
				int count=0;
				bool allow = false;
				var result=SendMT(sms, ref count);

				if (result.SMSStatus == SMSStatus.SentSuccess)
				{
					List<SmsInfo> dtSMSList;
					int countTostop = 0;
					while (countTostop < TotalWait)
					{
						dtSMSList = smsSender.GetSMS_V2();
						if (dtSMSList != null)
						{
							foreach (var item in dtSMSList)
							{
								ViewLogHandle(item.message);
								if (item.message.Contains(ConfigurationManager.AppSettings["SyntaxSendMoneyNext"].ToString()))
								{
									content = item.message;
									ViewLogHandle("=>Đã tim thấy: "+content + "\n");
									allow = true;
									break;
								}
							}

							if(allow)
							{
								break;
							}
						}
						Thread.Sleep(5000);
						countTostop += 1;
					}

				}
				else
				{
					content = result.ErrorsSMS;
					return false;
				}

				if (allow)
				{
					BusinessObjects.SMS smsAllow = new BusinessObjects.SMS();
					smsAllow.Message = "C " + ConfigurationManager.AppSettings["PasswordSendMoney"].ToString();
					smsAllow.PhoneNumber = phone;
					smsAllow.ContentType = SMSHostingContentType.Text;
					smsAllow.MessageType = SMSHostingMessageType.NormalSMS;
					smsAllow.TotalSMS = 1;
					var resultAllow = SendMT(smsAllow, ref count);

					allow = false;
					if (resultAllow.SMSStatus == SMSStatus.SentSuccess)
					{
						List<SmsInfo> dtSMSList=null;
						int countTostop = 0;
						while (countTostop < TotalWait)
						{
							dtSMSList = smsSender.GetSMS_V2();
							if (dtSMSList != null)
							{
								foreach (var item in dtSMSList)
								{
									ViewLogHandle(item.message);
									if (item.message.Contains(ConfigurationManager.AppSettings["SyntaxSendMoneyEnd"].ToString()))
									{
										content = item.message;
										ViewLogHandle("=>Đã tim thấy: " + content + "\n");
										allow=true;
										break;
									}
									else if (item.message.Contains(ConfigurationManager.AppSettings["MessageWrongTypePhone"].ToString()))
									{
										content = item.message;
										ViewLogHandle("=>Đã tim thấy: " + content + "\n");
										allow = true;
										break;
									}
									//else
									//{
									//	content += "\n" + item.message;
									//}
								}
							}
							if (allow)
							{
								return true;
							}
							Thread.Sleep(5000);
							countTostop += 1;
						}

						if (dtSMSList != null)
						{
							foreach (var item in dtSMSList)
							{
								content += "\n" + item.message;
							}
							return false;
						}

					}
					else
					{
						content = result.ErrorsSMS;
						return false;
					}
				}
				else
				{
					ViewLogHandle("Không tìm thấy tin nhắn bước tiếp theo. \n");
				}

				return false;
			}
			catch(Exception ex)
			{
				content = ex.Message;
				return false;
			}
			
		}

		public override bool DeleteMessage(string content)
		{
			return smsSender.DeleteMessage(content);
		}

		public override bool SendMoney(string syntax, ref string content,string type)
		{
			try
			{
				string statusPrepareModem = "";
				string source = "";
				if (smsSender == null)
				{
					statusPrepareModem = PrepareModemAdapter();
				}

				if (statusPrepareModem != "")
				{
					ViewLogHandle("GetSMSCanSent have error: " + statusPrepareModem);
					content = statusPrepareModem;
					return false;
				}
				//ViewLogHandle(syntax + "\n");
				source = smsSender.CheckMoney(syntax);
				ViewLogHandle(source + "\n");
				bool isStop = false;

				if ((source.Contains("+CMTI:") || source.Contains(ConfigurationManager.AppSettings["SyntaxSendMoneyNext"].ToString())) && ConfigurationManager.AppSettings["SyntaxSendMoneyNext"].ToString() != "")
				{
					if (type == "1")
					{
						source = smsSender.CheckMoney("1");
						ViewLogHandle("=> Đã tìm thấy: "+source + "\n");
						if (source.Contains(ConfigurationManager.AppSettings["SyntaxSendMoneyEnd"].ToString()))
						{
							content = source;
							return true;
						}
						else if (source.Contains(ConfigurationManager.AppSettings["MessageWrongTypePhone"].ToString()))
						{
							content = source;
							return true;
						}
						else
						{
							//Ghi lại luon de xem nhận dc message gi.
							content = source;
						}
					}
					else
					{
						List<SmsInfo> dtSMSList=null;
						int countTostop = 0;
						while (countTostop < TotalWait)
						{
							dtSMSList = smsSender.GetSMS_V2();
							if (dtSMSList != null)
							{
								foreach (var item in dtSMSList)
								{
									if (item.message.Contains(ConfigurationManager.AppSettings["SyntaxSendMoneyEnd"].ToString()))
									{
										content = item.message;
										ViewLogHandle("=>Đã tim thấy: " + content + "\n");
										isStop = true;
										break;
									}
									else if (item.message.Contains(ConfigurationManager.AppSettings["MessageWrongTypePhone"].ToString()))
									{
										content = item.message;
										ViewLogHandle("=>Đã tim thấy: " + content + "\n");
										isStop = true;
										break;
									}
									//else
									//{
									//	//Ghi lại luon de xem nhận dc message gi.
									//	content +="\n" + item.message;
									//}
								}
							}
							if (isStop)
							{
								return true;
							}
							
							Thread.Sleep(5000);
							countTostop += 1;
						}

						if (dtSMSList != null)
						{
							foreach (var item in dtSMSList)
							{
								content += "\n" + item.message;
							}
							return false;
						}

					}
				}
				else if ((source.Contains(ConfigurationManager.AppSettings["SyntaxSendMoneyEnd"].ToString())) && ConfigurationManager.AppSettings["SyntaxSendMoneyNext"].ToString() == "")
				{
					content = source;
					ViewLogHandle("=>Đã tim thấy: " + content + "\n");
					return true;
				}
				
				
				content = source;
				return false;

				//Random rd = new Random();
				//Thread.Sleep(rd.Next(2000, 10000));
				
				//int values = rd.Next(0, 3);
				//return values > 1 ? true : false;
				
			}
			catch(Exception ex)
			{
				content = ex.Message;
				return false;
			}
		}

		public override bool SendMoneyWithViettelPayLater(string phone, string money, ref string content)
		{
			try
			{
				string statusPrepareModem = "";
				string source = "";
				if (smsSender == null)
				{
					statusPrepareModem = PrepareModemAdapter();
				}

				if (statusPrepareModem != "")
				{
					ViewLogHandle("GetSMSCanSent have error: " + statusPrepareModem);
					content = statusPrepareModem;
					return false;
				}

				source = smsSender.CheckMoney(ConfigurationManager.AppSettings["SyntaxSendMoney"].ToString());

				if(source.Contains("1. Chuyen tien"))
				{
					source = smsSender.CheckMoney(ConfigurationManager.AppSettings["Syntax2SendMoney"].ToString());

					if (source.Contains("Ten nguoi thuc hien"))
					{
						source = smsSender.CheckMoney(ConfigurationManager.AppSettings["PhoneSyntax2SendMoney"].ToString());

						if (source.Contains("So dien thoai nguoi nhan:"))
						{
							source = smsSender.CheckMoney(phone);

							if (source.Contains("Toi da 20.000d/giao dich, Toi thieu 1.000d/giao dich"))
							{
								source = smsSender.CheckMoney(money);

								if (source.Contains("Nhap MAT KHAU de thuc hien giao dich"))
								{
									source = smsSender.CheckMoney(ConfigurationManager.AppSettings["PasswordSendMoney"].ToString());

									if (source.Contains("Quy khach da chuyen thanh cong"))
									{
										content = source;
										return true;
									}
									else if (source.Contains(ConfigurationManager.AppSettings["MessageWrongTypePhone"].ToString()))
									{
										content = source;
										return true;
									}
									else
									{
										content = source;
										return false;
									}
								}
							}
						}
					}
				}

				content = source;
				return false;

			}
			catch (Exception ex)
			{
				content = ex.Message;
				return false;
			}
		}

		public override bool SendMoneyWithToolkitMLoad(string phonenumber, string money)
		{
			try
			{
				
				return true;
			}
			catch
			{
				return false;
			}
		}

        /// <summary>
        /// Implements
        /// Tính số tin nhắn có thể gửi
        /// 16/05/2015
        /// NMH
        /// </summary>
        /// <returns></returns>
        public override int GetSMSCanSent(string syntax,string[] pts)
        {
            try
            {
                //int sms = 0;
                string statusPrepareModem = "";
                string source = "";
                //List<string> patterns = new List<string>();

                if (smsSender == null)
                {
                    statusPrepareModem = PrepareModemAdapter();
                }

                if (statusPrepareModem != "")
                {
                    ViewLogHandle("GetSMSCanSent have error: " + statusPrepareModem);
                    return 0;
                }

                source = smsSender.CheckMoney(syntax);

                //source="+CUSD: 2,\"MobiQ TKC:626d,17//07//2015 KM1T:400d,05//06//2015\",15";

                ViewLogHandle("GetSMSCanSent have " + syntax + " is: " + source);

                //patterns.Add("TKC:{0}d");

                List<string> match = SubStringByPattern(
                                                        source,
                                                        pts
                                                        );

                List<int> money = GetMoneyByPattern(match);

                if(money.Count<=0)
                {
                    ViewLogHandle("GetSMSCanSent not found Balance ");
                    return 0;
                }

                ViewLogHandle("GetSMSCanSent found " + money.Count + " balance : "+money[0]);

                return money[0];
            }
            catch(Exception ex)
            {
                ViewLogHandle("GetSMSCanSent have exception: " + ex.ToString());
                return 0;
            }
        }



        private List<string> SubStringByPattern(string source,string[] patterns)
        {
            List<string> str = new List<string>();

            //Parallel.For(0, patterns.Length, i =>
            //{
            for (int i = 0; i < patterns.Length; i++)
            {
                try
                {
                    //for (int i = 0; i < patterns.Count; i++)
                    //{
                    MatchCollection mc;
                    string pattern = patterns[i].Replace("{0}", @"[\d,]+");
                    Regex regex = new Regex(pattern);
                    mc = regex.Matches(source);
                    if (mc.Count > 0)
                    {
                        foreach (Match item in mc)
                        {
                            str.Add(item.Value);
                        }
                    }
                    //}
                }
                catch
                {

                }
            }
            //});

            return str;
        }

        private List<int> GetMoneyByPattern(List<string> list)
        {
            List<int> moneys = new List<int>();
            Parallel.For(0, list.Count, i =>
            {
                try
                {
                    string pattern = @"\D|,";
                    Regex regex = new Regex(pattern);
                    string money = regex.Replace(list[i], "");
                    moneys.Add(Convert.ToInt32(money));
                }
                catch
                {

                }
            });
            return moneys;
        }

		private string Base64Encode(string value)
		{
			var plainTextBytes = System.Text.ASCIIEncoding.UTF8.GetBytes(value);
			string result= System.Convert.ToBase64String(plainTextBytes);
			//ViewLogHandle("Base64Encode: " + result);
			return result;
		}

        public override string UpdateMO_V2(IncommingSMS incomming, bool usePushSMSToClient)
        {
            try
            {
                string result = "";
                string phone = incomming.PhoneNumber.Replace("+", "");
                string typeOfService = ConfigurationManager.AppSettings["TypeOfService"].ToString();
                bool _allowPushAllMO = Convert.ToBoolean(ConfigurationManager.AppSettings["AllowPushAllMO"]);
                bool allowCheckMoney = Convert.ToBoolean(ConfigurationManager.AppSettings["AllowCheckMoney"]);
                string templateMessageCheckMoney = ConfigurationManager.AppSettings["TemplateCheckMoney"];
                string[] listphoneAlloeCheckMoney = ConfigurationManager.AppSettings["ListAllowCheckMoney"].Split(';');

                if ((incomming.IndexMessageList.Count != (int)incomming.TotalPart))
                {
                    ViewLogHandle("Tin dài nhưng chưa nhận đủ hết các tin");
                    //ServiceRef.ServiceSoapClient services = new ServiceRef.ServiceSoapClient();
                    //services.SendSMS("CL7625", ConfigurationManager.AppSettings["Pass7625"].ToString().Trim(), "01656244909", "CaiThuocLa moi co tin nhan dai ma chua doc khuc duoi", Guid.NewGuid().ToString(), 1);
                    result = "<SMS not full>";
                    return result;
                }

                if (usePushSMSToClient)
                {
                    if (!incomming.IsSubmit
                        && incomming.IndexMessageList.Count == (int)incomming.TotalPart
                        )
                    {
                        string url = "";
                        //string pattern = @"^survey(\d\s\w\W)*";
                        string pattern = System.Configuration.ConfigurationManager.AppSettings["PatternSMSSurvey"].ToString();
                        Regex myRegex = new Regex(pattern);
                        //phone = incomming.PhoneNumber.Replace("+", "");

                        if (myRegex.IsMatch(incomming.Content.ToLower()))
                        {
                            //survey
                            string LinkProccessSurvey = System.Configuration.ConfigurationManager.AppSettings["LinkProccessSurvey"].ToString();
                            string UserNamePushSurvey = System.Configuration.ConfigurationManager.AppSettings["UserNamePushSurvey"].ToString();
                            string PasswordPushSurvey = System.Configuration.ConfigurationManager.AppSettings["PasswordPushSurvey"].ToString();
                            url = string.Format("{0}?userName={1}&password={2}&phoneNumber={3}&guid={5}&content={4}",
                                                LinkProccessSurvey,
                                                UserNamePushSurvey,
                                                HttpUtility.UrlEncode(PasswordPushSurvey),
                                                phone,
                                                //HttpUtility.UrlEncode(incomming.Content),
												Base64Encode(incomming.Content),
                                                HttpUtility.UrlEncode(incomming.GuidId));
                        }
                        else
                        {
                            string LinkProccess = System.Configuration.ConfigurationManager.AppSettings["LinkProccess"].ToString();
                            string UserNamePush = System.Configuration.ConfigurationManager.AppSettings["UserNamePush"].ToString();
                            string PasswordPush = System.Configuration.ConfigurationManager.AppSettings["PasswordPush"].ToString();
                            url = string.Format("{0}?userName={1}&password={2}&phoneNumber={3}&guid={5}&content={4}",
                                                LinkProccess,
                                                HttpUtility.UrlEncode(UserNamePush), HttpUtility.UrlEncode(PasswordPush),
                                //HttpUtility.UrlEncode(incomming.PhoneNumber.Trim()),
                                                phone,
								//HttpUtility.UrlEncode(incomming.Content),
												Base64Encode(incomming.Content),
                                                HttpUtility.UrlEncode(incomming.GuidId));
                        }

                        incomming.URLPushSMSToClient = url;
                        ViewLogHandle("URL Push MO to client is: " + url);
                        string response = "";
                        //if (incomming.PhoneNumber.Length >= 10 || typeOfService == "TraSau")
                        //{
                        if (incomming.PhoneNumber.Length >= 10 || typeOfService == "5" || _allowPushAllMO)
                        {
                            response = GetStringFromDestinationWeb(url);

                            if (response.Contains("OK"))
                            {
                                result = "<SubmitClient OK>";
                                incomming.IsSubmit = true;
                            }
                            else
                            {
                                result = "<SubmitClient Fail>";
                                incomming.AmountOfPushSMSToClient += 1;
                                //incomming.IsSubmit = false;
                            }
                        }
                        else
                        {
                            incomming.IsSubmit = true;
                            result = "<No Need SubmitClient>";
                        }

                       

                    }
                    else if (incomming.IsSubmit)
                    {
                        result = "<SubmitClient Done>";
                    }
                }
                else
                {
                    incomming.IsSubmit = true;
                    result = "<No Need SubmitClient>";
                }

                if (!incomming.IsSubmitFibo && incomming.IsSubmit)
                {
                    ClientCommingSMSHosting smsdata = BuiltClientCommingSMS_V2(incomming, ModemAdaptor, incomming.ServiceTypeID);

                    var sms = new ClientCommingSMSHostingData(smsdata, 0);

                    var criteria = new ClientCommingSMSHostingCriteria
                                           {
                                               GetByModemAdaptorID = (int)ModemAdaptor.ID
                                           };
                    GetToken();
                    Login();
                    var request = new ClientCommingSMSHostingRequest
                                      {
                                          ClientCommingSMSHosting = smsdata,
                                          Action = "Insert",
                                          ClientTag = UserName,
                                          AccessToken = Token.AccessToken,
                                          Criteria = criteria,
                                          RequestId = Guid.NewGuid().ToString(),

                                      };
                    request.ClientCommingSMSHosting.ID = -1;

                    if (typeOfService == "5" || incomming.PhoneNumber.Length >= 10 || _allowPushAllMO)
                    {
                        var response = managerAdaptor.SetClientCommingSMSHosting(request);
                        if (response.ClientCommingSMSHosting != null)//nếu submit tin thành công
                        {
                            incomming.IsSubmitFibo = true;
                            result += " <SubmitFibo OK>";
                        }
                        else
                        {
                            //incomming.IsSubmitFibo = false;
                            incomming.AmountSubmitFibo += 1;
                            result += " <SubmitFibo Fail>";
                        }
                    }
                    else
                    {
                        incomming.IsSubmitFibo = true;
                        result += " <No Need SubmitFibo>";
                    }

                    
                }
                else if (incomming.IsSubmitFibo)
                {
                    result += " <SubmitFibo done>";
                }

                if (incomming.IsSubmit && incomming.IsSubmitFibo)
                {
                    for (int k = 0; k < incomming.IndexMessageList.Count; k++)
                    {

                        if (smsSender.DeleteMessage(incomming.IndexMessageList[k].IndexMessage.ToString()))
                        {
                            ViewLogHandle("Delete SMS Success: " + incomming.IndexMessageList[k].IndexMessage);
                            incomming.IndexMessageList[k].IsDelete = true;
                        }
                        else
                        {
                            incomming.NumberTryDelete++;
                        }
                    }

                    if (!incomming.IndexMessageList.Any(t => t.IsDelete == false))
                    {
                        if (allowCheckMoney)
                        {
                            if (incomming.Content.StartsWith(templateMessageCheckMoney))
                            {
                                foreach (var item in listphoneAlloeCheckMoney)
                                {
                                    if (item != "" && incomming.PhoneNumber.EndsWith(item))
                                    {
                                        string message = smsSender.CheckMoney(ConfigurationManager.AppSettings["TemplateCheckMoney"]);
                                        ViewLogHandle(message);
                                        if (message.Contains("OK\r\n"))
                                            message = message.Split(new[] { "OK\r\n" }, StringSplitOptions.None)[1];
                                        ViewLogHandle(message + "SDT" + item);
                                        ViewLogHandle(SendSMSAlert(message, "0" + item));
                                        break;
                                    }
                                }
                            }
                        }

                        result += " <Delete MO Success>";
                    }
                    else
                    {
                        incomming.AmountDeleteMO += 1;
                        result += " <Delete MO Fail>";
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                ViewLogHandle("UpdateMO_V2 error:\n" + ex.ToString());
                incomming.AmoutUpdateMO += 1;
                return "UpdateMO_V2 Error";
            }
        }
    }
}

