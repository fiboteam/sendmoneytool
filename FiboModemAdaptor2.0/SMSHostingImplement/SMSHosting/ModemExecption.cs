﻿using System;
using System.Collections.Generic;
using System.Text;
using ModemInterface;



public class ModemException : ApplicationException
{
    public string customMessage;
    public ModemDeviceStatus modemDeviceStatus;
    public string ModemAdapterName;
    public ModemException()
    {
    }
    public ModemException(String modemName, ModemDeviceStatus status)
    {
        this.ModemAdapterName = modemName;
        this.modemDeviceStatus = status;
    }
}

public class SendATCommand : ModemException
{
    public SendATCommand()
    {
        this.customMessage = "AT Command timed out without receiving 'OK'.";
    }

    public SendATCommand(string strCustomMessage)
    {
        this.customMessage = strCustomMessage;
    }
}

public class SendCPINCommand : ModemException
{
    public SendCPINCommand()
    {
        this.customMessage = "AT+CPIN? Command timed out without receiving 'READY' or 'SIM PIN'.";
    }

    public SendCPINCommand(string strCustomMessage)
    {
        this.customMessage = strCustomMessage;
    }
}

public class SendCMGFCommand : ModemException
{
    public SendCMGFCommand()
    {
        this.customMessage = "AT+CMGF=1 Command timed out without receiving 'OK'.";
    }

    public SendCMGFCommand(string strCustomMessage)
    {
        this.customMessage = strCustomMessage;
    }
}

public class SendCMGSCommand : ModemException
{
    public SendCMGSCommand()
    {
        this.customMessage = "CMGS Command timed out without receiving '>'.";
    }

    public SendCMGSCommand(string strCustomMessage)
    {
        this.customMessage = strCustomMessage;
    }
}

public class SendMessageType : ModemException
{
    public SendMessageType()
    {
        this.customMessage = "Message timed out without receiving '>'.";
    }

    public SendMessageType(string strCustomMessage)
    {
        this.customMessage = strCustomMessage;
    }
}

public class sendCTRLZ : ModemException
{
    public sendCTRLZ()
    {
        this.customMessage = "Message timed out without receiving '+CMGS:'.";
    }

    public sendCTRLZ(string strCustomMessage)
    {
        this.customMessage = strCustomMessage;
    }
}

