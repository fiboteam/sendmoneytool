using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Diagnostics;

using SMSPDULib;
using log4net;
using Utility;
using System.Threading;
using ModemInterface;
using SMSHostingImplement;
using System.IO;
using System.Configuration;

public class PortUtilities : ExcuteATResult
{

    public double dtimeout = 20;
    public  int timeoutstandar = 3000;
    public SerialPort serialPort;

    private AutoResetEvent receiveNow;
    private readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public delegate void WriteATCommand(string Message);
    public event WriteATCommand WriteATCommandEvent;

    public void WriteATCommandHandle(string Message)
    {
        if (WriteATCommandEvent != null)
            WriteATCommandEvent(Message);
    }

    /// <summary>
    /// Mở Port cùng với một số kiểm tra sim
    /// </summary>
    /// <param name="portName">The Name of the port, "COM?"</param>
    /// <param name="pinNumber">The Pin Number for the GSM Modem SIM Card</param>
    /// <returns>Opened Serial Port Object</returns>
    public void OpenPort(string portName, int baudRate, ref ModemDeviceStatus simStatus)
    {
        try
        {
            var stopwatch = new Stopwatch();
            
            simStatus = ModemDeviceStatus.EnableNetWork;
            receiveNow = new AutoResetEvent(false);
            serialPort = new SerialPort(portName, baudRate, Parity.None, 8, StopBits.One);
            serialPort.ReadTimeout = 500;
            serialPort.WriteTimeout = 500;
            serialPort.Encoding = Encoding.GetEncoding("iso-8859-1");
            serialPort.Open();
            serialPort.DtrEnable = true;
            serialPort.RtsEnable = true;
            serialPort.DataReceived += new SerialDataReceivedEventHandler(this.SerialPort_DataReceived);
            //SendESCCommand();

            stopwatch.Start();
            if (!SendATCommand())
            {
                simStatus = ModemDeviceStatus.CantSendAT;
                return;
            }
            stopwatch.Stop();
            WriteATCommandHandle("SendATCommand -> " + stopwatch.Elapsed);

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["AllowFullPrepareModem"]))
            {
                stopwatch.Restart();
                simStatus = SendCREGCommand();
                if (simStatus != ModemDeviceStatus.EnableNetWork) return;
                stopwatch.Stop();
                WriteATCommandHandle("SendCREGCommand -> " + stopwatch.Elapsed);

                stopwatch.Restart();
                simStatus = SendCSMSCommand();
                if (simStatus != ModemDeviceStatus.EnableNetWork) return;
                stopwatch.Stop();
                WriteATCommandHandle("SendCSMSCommand -> " + stopwatch.Elapsed);
            }

            stopwatch.Restart();
            if (!SendCMGFCommand(1)) simStatus = ModemDeviceStatus.CantSendCMGF;
            stopwatch.Stop();
            WriteATCommandHandle("SendCMGFCommand -> " + stopwatch.Elapsed);
        }
        catch (Exception ex)
        {
            this.ClosePort();
            simStatus = ModemDeviceStatus.OtherProgramUsingCOM;
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
        }
    }

    public void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
        try
        {
            if (e.EventType == SerialData.Chars)
            {
                this.receiveNow.Set();
            }
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
        }
    }
    /// <summary>
    /// Hàm excute command với time out truyền vào. Ngoài đợi kết quả truyền vào còn đợi thêm 1 số cái khác
    /// </summary>
    /// <param name="command"></param>
    /// <param name="responseTimeout"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private string ExecCommandEx(string command, int responseTimeout, string end)
    {
        string str = string.Empty;
        try
        {
            this.resetPort();
            serialPort.Write(command + "\r");
            str = this.ReadResponseEx(responseTimeout, end);
            WriteATCommandHandle(str);
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
        }
        return str;
    }
    /// <summary>
    /// Hàm excute command với time out truyền vào. Ngoài đợi kết quả truyền vào còn đợi thêm 1 số cái khác
    /// </summary>
    /// <param name="command"></param>
    /// <param name="responseTimeout"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private string ExecCommand(string command, int responseTimeout, string end)
    {
        string str = string.Empty;
        try
        {
            this.resetPort();
            serialPort.Write(command + "\r");
            str = this.ReadResponse(responseTimeout, end);
            WriteATCommandHandle(str);
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
        }
        return str;
    }
    /// <summary>
    /// Hàm excute chuẩn với timeout cấu hình trong admin fibo.
    /// </summary>
    /// <param name="command">lệnh AT Command</param>
    /// <param name="strSuccess">Kết quả mong muốn</param>
    /// <param name="strFail">Kết quả thất bại</param>
    /// <returns></returns>
    private bool ExecCommandStandar(string command, string strSuccess, string strFail)
    {
        string str = string.Empty;
        try
        {
            this.resetPort();
            serialPort.Write(command + "\r");
            str = this.ReadResponse(timeoutstandar, strSuccess);
            WriteATCommandHandle(str);
            if (str.Contains(strSuccess)) return true;
            if (strFail != null && strFail != "" && str.Contains(strFail)) return false;
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
            return false;
        }
        return false;
    }
    private string ReadResponseHaveOK(int timeout, string end)
    {
        string str = "";
        try
        {
            do
            {
                if (this.receiveNow.WaitOne(timeout, false))
                {
                    string str2 = this.serialPort.ReadExisting();
                    str = str + str2;
                }
                else
                {
                    return str;
                }
            }
            while ((!str.Contains("\r\nOK\r\n") || !str.Contains(end)) && !str.Contains("\r\nERROR\r\n"));
        }
        catch (Exception ex)
        {
            this.WriteATCommandHandle(ex.Message);
            this.logger.Error(ex.ToString());
        }
        return str;
    }

    private string ExecCommandHaveOK(string command, int responseTimeout, string end)
    {
        string str = "";
        try
        {
            this.WriteATCommandHandle("ExecCommand" + command);
            this.resetPort();
            this.serialPort.Write(command + "\r");
            str = this.ReadResponseHaveOK(responseTimeout, end);
            this.WriteATCommandHandle(str);
        }
        catch (Exception ex)
        {
            this.WriteATCommandHandle(ex.Message);
            this.logger.Error(ex.ToString());
        }
        return str;
    }

 

    

    /// <summary>
    /// excute command chỉ đợi kết quả mong muốn
    /// </summary>
    /// <param name="command"></param>
    /// <param name="responseTimeout"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private string ExecCommandSpecial(string command, int responseTimeout, string end)
    {
        string str = string.Empty;
        try
        {
            this.resetPort();
            serialPort.Write(command + "\r");
            str = this.ReadResponseSpecial(responseTimeout, end);
            WriteATCommandHandle(str);
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
        }
        return str;
    }

    private string ReadResponseSpecial(int timeout, string end)
    {
        string str = string.Empty;
        try
        {
            do
            {
                if (this.receiveNow.WaitOne(timeout, false))
                {
                    string str2 = serialPort.ReadExisting();
                    str = str + str2;
                }
                else
                {
                    return str;
                }
            }
			while (!str.EndsWith(end) && !str.EndsWith(",15\r\n") && !str.EndsWith(",64\r\n") && !str.EndsWith(",15\r\n\r\n") && !str.EndsWith(",64\r\n\r\n"));
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
        }
        return str;
    }

    /// <summary>
    /// Đọc buffer từ Port
    /// </summary>
    /// <param name="timeout">Thời gian cho phép</param>
    /// <param name="end"> Kết quả mong muốn</param>
    /// <returns></returns>
    private string ReadResponseEx(int timeout, string end)
    {
        string str = string.Empty;
        try
        {
            do
            {
                if (this.receiveNow.WaitOne(timeout, false))
                {
                    string str2 = serialPort.ReadExisting();
                    str = str + str2;
                }
                else
                {
                    return str;
                }
            }
            while ( !str.EndsWith(end));
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
        }
        return str;
    }

    /// <summary>
    /// Đọc buffer từ Port
    /// </summary>
    /// <param name="timeout">Thời gian cho phép</param>
    /// <param name="end"> Kết quả mong muốn</param>
    /// <returns></returns>
    private string ReadResponse(int timeout, string end)
    {
        string str = string.Empty;
        try
        {
            do
            {
                if (this.receiveNow.WaitOne(timeout, false))
                {
                    string str2 = serialPort.ReadExisting();
                    str = str + str2;
                }
                else
                {
                    return str;
                }
            }
            while ((!str.EndsWith(ExcuteOK2) && !str.EndsWith("\r\n> ")) && !str.EndsWith(ExcuteErrors) && !str.EndsWith(end));
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
        }
        return str;
    }

    /// <summary>
    /// Xóa buffer hiện tại trên port
    /// </summary>
    public void resetPort()
    {
        serialPort.DiscardOutBuffer();
        serialPort.DiscardInBuffer();
    }

    /// <summary>
    ///Send ESC để close các lệnh lỗi trước đó nếu có 
    /// </summary>
    /// <param name="serialPort"></param>
    /// <returns></returns>
    public bool SendESCCommand()
    {
        string vaa = "" + (char)27;
        return ExecCommandStandar(vaa, vaa, null);
    }

    /// <summary>
    /// Sends an AT Command to the supplied port
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    /// <returns>True if successful</returns>
    public bool SendATCommand()
    {
        //return ExecCommandStandar("AT", ExcuteOK, null);
        ///NMH 08/04/2015
        return ExecCommandStandar("AT", "OK\r\n", null);
    }

    /// <summary>
    /// Send một câu lệnh AT command xuống port
    /// </summary>
    /// <param name="command"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public string SendCommand(string command, string end)
    {
        try
        {
            return ExecCommand(command, timeoutstandar, end);
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
            return "";
        }

    }

    /// <summary>
    /// Sets the Pin for the device is it is not already set.
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    /// <param name="pinNumber">The Pin Number for the GSM Modem SIM Card</param>
    /// <returns>True if successful</returns>
    private bool SendCPINCommand(string pinNumber)
    {
        DateTime timeout = DateTime.Now.AddSeconds(dtimeout);
        string buffer = "";
        //Check if need to enter PIN
        try
        {
            serialPort.Write("AT+CPIN?" + (char)13);
            buffer = "";
            do
            {
                buffer += serialPort.ReadExisting();
                if (DateTime.Now > timeout)
                {
                    logger.Error("Errors Buffer CPIN" + buffer.ToString());
                    return false;
                }
            }
            while ((!buffer.Contains(ExcuteReady)) && (!buffer.Contains("SIM PIN")));
        }
        catch (SendCPINCommand ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
            return false;
        }
        if (buffer.Contains("SIM PIN"))
        {
            try
            {
                serialPort.Write("AT+CPIN=\"" + pinNumber + "\"" + (char)13);
                buffer = "";
                do
                {
                    buffer += serialPort.ReadExisting();
                    if (DateTime.Now > timeout)
                    {
                        logger.Error("Errors Buffer CPIN" + buffer.ToString());
                        //throw new Exception("AT+CPIN Command timed out without receiving 'OK'.");
                        return false;
                    }
                    if (buffer.Contains(ExcuteErrors))
                        //throw new Exception("APIN return 'ERROR'.");
                        return false;
                }
                while (!buffer.Contains(ExcuteOK));
            }
            catch (Exception ex)
            {
                WriteATCommandHandle(ex.Message);
                logger.Error(ex.ToString());
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Gửi CMGFCommand để chuyển đổi kiểu send at command
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    /// <returns>True if successful</returns>
    private bool SendCMGFCommand()
    {
        return SendCMGFCommand(1);
    }

    /// <summary>
    /// Gửi CSMSCommand để check sim có bị khóa chiều gửi tin ko
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    public ModemDeviceStatus SendCSMSCommand()
    {
        //string result = ExecCommand("AT+CSMS=1", timeoutstandar, ExcuteOK);
        string result = ExecCommand("AT+CSMS=1", timeoutstandar, "OK\r\n");
        if (result.Contains(ExcuteOK)) return ModemDeviceStatus.EnableNetWork;
        else return ModemDeviceStatus.CantSendCSMS;

    }

    /// <summary>
    /// Sends the CMGFCommand to set the input type
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    /// <param name="mode">Mode 0 : PDU; 1: Text Mode</param>
    /// <returns>True if successful</returns>
    public bool SendCMGFCommand(int mode)
    {
        //return ExecCommandStandar("AT+CMGF=" + mode, ExcuteOK, ExcuteErrors);
        return ExecCommandStandar("AT+CMGF=" + mode, "OK\r\n", ExcuteErrors);
    }

    /// <summary>
    /// Sends the CREGCommand to check connect network
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    private ModemDeviceStatus SendCREGCommand()
    {
        string result = this.ExecCommandHaveOK("AT+CREG=1", this.timeoutstandar, "+CREG: 1");
        if (result.IndexOf("+CREG: 0") > -1)
        {
            return ModemDeviceStatus.DisableNetWork;
        }
        else if (result.IndexOf("+CREG: 2") > -1)
        {
            return ModemDeviceStatus.SimNotRegistry;
        }
        //+CREG: 1
        return ModemDeviceStatus.EnableNetWork;
    }


    /// <summary>
    /// Change Modem SMS  text mode
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    /// <param name="textMode">The text mode of message  : ("GSM","PCCP437","CUSTOM","HEX")</param>
    /// <returns>True if successful</returns>
    private bool SendCSCSCommand(string textMode)
    {
        return ExecCommandStandar("AT+CSCS=\"" + textMode + "\"", ExcuteOK, ExcuteErrors);
    }

    /// <summary>
    /// To specify the correct DCS (Data Coding Scheme) :
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    /// <param name="dcs">DCS (Data Coding Scheme)for example : Unicode messages, which is 0x08.We can set this value by changing the fourth parameter of the AT+CSMP command to '8'</param>
    /// <returns>True if successful</returns>
    private bool SendCSMPCommand(string dcs)
    {
        return ExecCommandStandar("AT+CSMP=" + dcs, ExcuteOK, ExcuteErrors);
    }

    /// <summary>
    /// Initiates the GSM Modem to send a message to the supplied mobile number
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    /// <param name="mobileNumber">The mobile number to send the message to</param>
    /// <returns>True if successful</returns>
    private bool SendCMGSCommand(string mobileNumber)
    {
        return ExecCommandStandar("AT+CMGS=" + mobileNumber, ">", ExcuteErrors);
    }

    public string sendCMGD_DeleteMessage(string messageID)
    {
        return ExecCommand("AT+CMGD=" + messageID, timeoutstandar, ExcuteOK);

    }

    /// <summary>
    /// Để 30000 cho hongthienmy
    /// </summary>
    /// <param name="messageType"></param>
    /// <returns></returns>
    public string sendCMGL(string messageType, int timeout)
    {
        //return ExecCommandEx("AT+CMGL=\"" + messageType + "\"", timeout, "\r\n\r\nOK\r\n");
        return ExecCommandEx("AT+CMGL=\"" + messageType + "\"", timeout, "\r\nOK\r\n");
    }


    /// <summary>
    /// Finalises message send routing by sending the messge to the mobile number
    /// </summary>
    /// <param name="serialPort">Port to use to send command</param>
    /// <returns>True if successful</returns>
    private bool sendCTRLZ()
    {
        return ExecCommandStandar("" + (char)26, "+CMGS:", ExcuteErrors);
    }
    private bool sendCTRLZ(int time)
    {
        this.WriteATCommandHandle("sendCTRLZ" + time);
        string result = ExecCommandHaveOK("" + (char)26, time, "+CMGS:");
        this.WriteATCommandHandle(result);
        return result.Contains("+CMGS:");
    }

    /// <summary>
    /// Send the messge to the supplied mobile number via the supplied serialport
    /// </summary>
    /// <param name="serialPort">Port to use to send message</param>
    /// <param name="mobileNumber">The GSM Modem Phone Number for send the message to,</param>
    /// <param name="message">The message to be sent</param>
    /// <returns>True if successful</returns>
    public SMSStatus SendMSG(string mobileNumber, string message, ContentType contentType)
    {
        if (contentType == ContentType.Unicode)
        {
            if (!SendCMGFCommand()) { return SMSStatus.SentFailed; }
            if (!SendCSCSCommand("HEX")) { return SMSStatus.SentFailed; }
            if (!SendCSMPCommand("1,167,0,8")) { return SMSStatus.SentFailed; }

            if (SendUniCodeMessage(serialPort, message, mobileNumber))
            {
                return SMSStatus.SentFailed;
            }

            if (!sendCTRLZ())
            {
                return SMSStatus.SentFailed;
            }
        }
        else
        {
            //LogMessage(message, EventLogEntryType.Error);
            if (!SendCMGSCommand(mobileNumber))
            {
                SendESCCommand();
                return SMSStatus.SentFailed;
            }
            if (!SendMessageType(message))
            {
                SendESCCommand();
                return SMSStatus.SentFailed;
            }

            if (!sendCTRLZ())
            {
                SendESCCommand();
                return SMSStatus.SentFailed;
            }
        }
        return SMSStatus.SentSuccess;
    }
    public string StringToHexString(string str)
    {
        Encoding unicode = Encoding.Unicode;
        Byte[] bytes = unicode.GetBytes(str);
        StringBuilder hexString = new StringBuilder(bytes.Length);
        for (int i = 0; i < bytes.Length; i++)
        {
            if (i % 2 == 0)
                hexString.Append(bytes[i].ToString("X2"));
            else
                hexString.Insert(hexString.Length - 2, bytes[i].ToString("X2"));

        }
        return hexString.ToString();
    }
    private bool SendUniCodeMessage(SerialPort serialPort, string message, string phone)
    {
        string hexMessage = StringToHexString(message);
        try
        {
            if (!SendCMGSCommand(phone))
            {
                SendESCCommand();
                return false;
            }
            if (!ExecCommandStandar(hexMessage, ">", ExcuteErrors))
            {
                SendESCCommand();
                return false;
            }

            if (!sendCTRLZ(timeoutstandar))//đợi có kết quả nếu sau 10s mà ko gửi dc thì fail
            {
                SendESCCommand();
                return false;
            }
        }
        catch (SendMessageType ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.Message);
            return false;
        }
        return true;
    }


    private bool SendMessageType(string message)
    {
        return ExecCommandStandar(message, ">", ExcuteErrors);
    }

    /// <summary>
    /// Send the concatenated messge to the supplied mobile number via the supplied serialport
    /// </summary>
    /// <param name="serialPort">Port to use to send message</param>
    /// <param name="mobileNumber">The GSM Modem Phone Number for send the message to,</param>
    /// <param name="message">The message to be sent</param>
    /// <param name="contentType">Normal or Unicode </param>
    /// <returns>True if successful</returns>
    public SMSStatus SendConcatenatedSMS(string mobileNumber, string message, ContentType contentType, int totalsms)
    {
        //LogMessage(message, EventLogEntryType.Error);
        if (!SendCMGFCommand(0)) { return SMSStatus.SentFailed; }
        try
        {
            string oldmess = message;
         
            try
            {
                string newmessage = GSM0338Charset.ConvertToGSM0338String(message);
                //nếu là tin dạng text và nhỏ hơn 160
                if (contentType == ContentType.Text && ((int)((newmessage.Length -1) / 160)+1) == totalsms && oldmess.Length <= 160)
                    message = newmessage;
                //Nếu tin là tin dạng text ma >160
                else if (contentType == ContentType.Text && ((int)((newmessage.Length-1) / 153) + 1) == totalsms && oldmess.Length > 160)
                    message = newmessage;
                else if (contentType == ContentType.Unicode && ((int)((newmessage.Length-1) / 153) + 1) == totalsms)
                    message = newmessage;
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(GSMEncodingException))
                    message = oldmess; 
            }
            
            PduEncode.Encoder.SMS.ENUM_TP_DCS textType = PduEncode.Encoder.SMS.ENUM_TP_DCS.DefaultAlphabet;
            if (contentType == ContentType.Unicode)
                textType = PduEncode.Encoder.SMS.ENUM_TP_DCS.UCS2;
            string[] pduCode = GetPDU("0", mobileNumber, textType, PduEncode.Encoder.SMS.ENUM_TP_VALID_PERIOD.OneHour, 0, false, message);

            for (int i = 0; i < pduCode.Length; i++)
            {
                string pdu = pduCode[i];
                pdu = "00" + pdu.Substring(4);
                int len = (pdu.Length - 2) / 2;
                if (!SendCMGSCommand(len.ToString()))
                {
                    SendESCCommand();
                    return SMSStatus.SentFailed;
                }
                if (!ExecCommandStandar(pdu, ">", ExcuteErrors))
                {
                    SendESCCommand();
                    return SMSStatus.SentFailed;
                }

                if (!sendCTRLZ(timeoutstandar))//đợi có kết quả nếu sau 10s mà ko gửi dc thì fail
                {
                    SendESCCommand();
                    return SMSStatus.SentFailed;
                }
          
            }
        }
        catch (SendMessageType ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
            return SMSStatus.SentFailed;
        }
        return SMSStatus.SentSuccess;
    }

    public SMSStatus SendConcatenatedSMSStandard(string mobileNumber, string message, ContentType contentType, int totalsms, ref int countSMSSuccess)
    {
        //LogMessage(message, EventLogEntryType.Error);
        if (!SendCMGFCommand(0)) { return SMSStatus.SentFailed; }
        SMSPDULib.SMS sms = new SMSPDULib.SMS();
        try
        {
            string oldmess = message;
            try
            {
                string newmessage = GSM0338Charset.ConvertToGSM0338String(message);
                //nếu là tin dạng text và nhỏ hơn 160
                if (contentType == ContentType.Text && ((int)((newmessage.Length - 1) / 160) + 1) == totalsms && oldmess.Length <= 160)
                    message = newmessage;
                //Nếu tin là tin dạng text ma >160
                else if (contentType == ContentType.Text && ((int)((newmessage.Length - 1) / 153) + 1) == totalsms && oldmess.Length > 160)
                    message = newmessage;
                else if (contentType == ContentType.Unicode && ((int)((newmessage.Length - 1) / 153) + 1) == totalsms)
                    message = newmessage;
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(GSMEncodingException))
                    message = oldmess;
            }
            //Setting direction of sms
            sms.Direction = SMSDirection.Submited;
            //Set the recipient number
            sms.PhoneNumber = mobileNumber;
            sms.ValidityPeriodFormat = SMSPDULib.ValidityPeriodFormat.Relative;
            sms.ValidityPeriod = new TimeSpan(255, 0, 0);
            sms.Message = message;

            string[] pduCode;

            if (contentType == ContentType.Unicode)
                //pduCode = sms.ComposeLongSMS(SMSPDULib.SMS.SMSEncoding.UCS2);
                pduCode = sms.ComposeLongSMS();
            else
                //pduCode = sms.ComposeLongSMS(SMSPDULib.SMS.SMSEncoding._7bit);
                pduCode = sms.ComposeLongSMS();

            for (int i = 0; i < pduCode.Length; i++)
            {
                string pdu = pduCode[i];
                int len = (pdu.Length - 2) / 2;
                if (!SendCMGSCommand(len.ToString()))
                {
                    SendESCCommand();
                    return SMSStatus.SentFailed;
                }
                if (!ExecCommandStandar(pdu, ">", ExcuteErrors))
                {
                    SendESCCommand();
                    return SMSStatus.SentFailed;
                }

                if (!sendCTRLZ(timeoutstandar))//đợi có kết quả nếu sau 10s mà ko gửi dc thì fail
                {
                    SendESCCommand();
                    return SMSStatus.SentFailed;
                }
                countSMSSuccess++;
            }
        }
        catch (SendMessageType ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
            return SMSStatus.SentFailed;
        }
        return SMSStatus.SentSuccess;
    }
	public SMSStatus SentByPDU(string message, string phone, SMSHostingContentType contentype, SMSHostingMessageType messagetype)
	{
		var status = SMSStatus.SentFailed;
		SMSPDULib.SMS smspdu = EncodeMessageToPdu(message, phone, contentype, messagetype);

		if (smspdu == null)
		{
			return SMSStatus.CanNotSend;
		}
		else
		{
			string[] pduMessage;
			if ((smspdu.Message.Length <= 160 && smspdu.MessageEncoding == SMSPDULib.SMS.SMSEncoding._7bit)
			 || (smspdu.Message.Length <= 70 && smspdu.MessageEncoding == SMSPDULib.SMS.SMSEncoding.UCS2)) //Tin nhắn dài hơn 160 ký tự
				pduMessage = new string[] { smspdu.Compose() };
			else
				pduMessage = smspdu.ComposeLongSMS();

			for (int i = 0; i < pduMessage.Length; i++)
			{
				string pdu = pduMessage[i];
				//pdu = "00" + pdu.Substring(4);
				int len = (pdu.Length - 2) / 2;
				if (!SendCMGFCommand(0)) 
				{ 
					return SMSStatus.SentFailed; 
				}
				if (!SendCMGSCommand(len.ToString()))
				{
					SendESCCommand();
					return SMSStatus.SentFailed;
				}
				if (!ExecCommandStandar(pdu, ">", ExcuteErrors))
				{
					SendESCCommand();
					return SMSStatus.SentFailed;
				}

				if (!sendCTRLZ(timeoutstandar))//đợi có kết quả nếu sau 10s mà ko gửi dc thì fail
				{
					SendESCCommand();
					return SMSStatus.SentFailed;
				}
			}
			return SMSStatus.SentSuccess;
		}
	}
	/// <summary>
	/// Chuyễn nội dung tin nhắn thành SMS để gửi tin
	/// </summary>
	/// <param name="message"></param>
	/// <param name="phoneNumber"></param>
	/// <returns></returns>
	private SMSPDULib.SMS EncodeMessageToPdu(string message, string phoneNumber, SMSHostingContentType messageContentType = SMSHostingContentType.Text, SMSHostingMessageType messageType = SMSHostingMessageType.NormalSMS)
	{
		try
		{
			phoneNumber = phoneNumber.Replace(" ", "");
			if (phoneNumber.StartsWith("84"))
				phoneNumber = phoneNumber.Remove(0, 2).Insert(0, "0");
			else if (phoneNumber.StartsWith("+84"))
				phoneNumber = phoneNumber.Remove(0, 3).Insert(0, "0");
			//else if (!phoneNumber.StartsWith("0"))
			//    phoneNumber = phoneNumber.Insert(0, "0");

			SMSPDULib.SMS sms = new SMSPDULib.SMS();
			sms.Direction = SMSDirection.Submited;
			sms.PhoneNumber = phoneNumber;
			sms.ValidityPeriodFormat = SMSPDULib.ValidityPeriodFormat.Relative;
			sms.ValidityPeriod = new TimeSpan(255, 0, 0);
			sms.MessageEncoding = messageContentType == SMSHostingContentType.Unicode ? SMSPDULib.SMS.SMSEncoding.UCS2 : SMSPDULib.SMS.SMSEncoding._7bit;
			sms.Message = message;
			sms.Flash = messageType == SMSHostingMessageType.Flash ? true : false;
			return sms;
		}
		catch (Exception ex)
		{
			//WriteATCommandEvent(exception: ex);
			WriteATCommandHandle("EncodeMessageToPdu fail:\n" + ex.ToString());
			return null;
		}
	}
    public string[] GetPDU(string ServiceCenterNumber,
                           string DestNumber,
                           PduEncode.Encoder.SMS.ENUM_TP_DCS DataCodingScheme,
                           PduEncode.Encoder.SMS.ENUM_TP_VALID_PERIOD ValidPeriod,
                           int MsgReference,
                           bool StatusReport,
                           string UserData)
    {
        //Check for SMS type
        int type = 0;// '0 for SMS;1 For ConcatenatedShortMessage
        string[] result;
        PduEncode.Encoder.SMS smsObject = new PduEncode.Encoder.SMS();
        switch (DataCodingScheme)
        {
            case PduEncode.Encoder.SMS.ENUM_TP_DCS.DefaultAlphabet:
                if (UserData.Length > 160)
                {
                    smsObject = new PduEncode.Encoder.ConcatenatedShortMessage();
                    type = 1;
                }
                break;
            case PduEncode.Encoder.SMS.ENUM_TP_DCS.UCS2:
                if (UserData.Length > 70)
                {
                    smsObject = new PduEncode.Encoder.ConcatenatedShortMessage();
                    type = 1;
                }
                break;
            case PduEncode.Encoder.SMS.ENUM_TP_DCS.WapPush:
                smsObject = new PduEncode.Encoder.WapPushMessage();

                break;
        }


        smsObject.ServiceCenterNumber = ServiceCenterNumber;
        if (StatusReport == true)
            smsObject.TP_Status_Report_Request = PduEncode.Encoder.SMS.ENUM_TP_SRI.Request_SMS_Report;
        else
            smsObject.TP_Status_Report_Request = PduEncode.Encoder.SMS.ENUM_TP_SRI.No_SMS_Report;

        smsObject.TP_Destination_Address = DestNumber;
        smsObject.TP_Data_Coding_Scheme = DataCodingScheme;
        smsObject.TP_Message_Reference = MsgReference;
        smsObject.TP_Validity_Period = ValidPeriod;
        smsObject.TP_User_Data = UserData;
        if (type == 0)
        {
            result = new string[1];
            if (DataCodingScheme == PduEncode.Encoder.SMS.ENUM_TP_DCS.WapPush)
                result[0] = ((PduEncode.Encoder.WapPushMessage)smsObject).GetSMSPDUCodeXoa();
            else
                result[0] = smsObject.GetSMSPDUCode();
        }
        else
        {
            result = ((PduEncode.Encoder.ConcatenatedShortMessage)smsObject).GetEMSPDUCode();            //'Note here must use GetEMSPDUCode to get right PDU codes
        }
        return result;
    }

    /// <summary>
    /// Send the concatenated messge to the supplied mobile number via the supplied serialport
    /// </summary>
    /// <param name="serialPort">Port to use to send message</param>
    /// <param name="mobileNumber">The GSM Modem Phone Number for send the message to,</param>
    /// <param name="message">The message to be sent</param>
    /// <returns>True if successful</returns>
    public SMSStatus WapPushMessage(string mobileNumber, string message)
    {
        //LogMessage(message, EventLogEntryType.Error);
        if (!SendCMGFCommand(0)) { return SMSStatus.SentFailed; }

        if (!SendWapPush2(message, mobileNumber))
        {
            return SMSStatus.SentFailed;
        }
        return SMSStatus.SentSuccess;
    }
    #region Core Send Flash
    private string createFlash(string mobile, string sms)
    {
        string str = (((string.Empty + "000101") + "0" + mobile.Length.ToString("X")) + "91") + this.pduMobile(mobile) + "0018";
        if ((sms.Length * 2) < 0x11)
        {
            int num = sms.Length * 2;
            str = str + "0" + num.ToString("X");
        }
        else
        {
            str = str + ((sms.Length * 2)).ToString("X");
        }
        return (str + this.charToUcs2(sms));
    }
    private string charToUcs2(string str)
    {
        string str2 = string.Empty;
        int num = 0;
        foreach (char ch in str)
        {
            int num2 = ch;
            if ((num2 < 0xd800) || (num2 > 0xdfff))
            {
                str2 = str2 + num2.ToString("X4");
            }
            else if (num2 < 0xdc00)
            {
                num = num2 - 0xd800;
            }
            else
            {
                str2 = str2 + ((num << ((10 + (num2 - 0xdc00)) + 0x10000))).ToString("X6");
            }
        }
        return str2;
    }
    private string pduMobile(string mobile)
    {
        if (mobile.Length == 11)
        {
            mobile = mobile + "F";
        }
        char[] chArray = mobile.ToCharArray();
        return (Convert.ToString(chArray[1]) + Convert.ToString(chArray[0]) + Convert.ToString(chArray[3]) + Convert.ToString(chArray[2]) + Convert.ToString(chArray[5]) + Convert.ToString(chArray[4]) + Convert.ToString(chArray[7]) + Convert.ToString(chArray[6]) + Convert.ToString(chArray[9]) + Convert.ToString(chArray[8]) + Convert.ToString(chArray[11]) + Convert.ToString(chArray[10]));
    }
    #endregion
    //bắt buộc phải có 84 đầu
    public SMSStatus sendFlash(string mobile, string sms)
    {
        bool flag = false;
        string command = this.createFlash(mobile, sms);
        int num = (command.Length / 2) - 1;
        command = string.Concat(new object[] { "AT+CMGS=", num, char.ConvertFromUtf32(13), command, char.ConvertFromUtf32(0x1a) });
        if (!SendCMGFCommand(0)) return SMSStatus.SentFailed;
        if (!ExecCommandStandar(command, "CMGS", null))
        {
            return SMSStatus.SentFailed;
        }
        return SMSStatus.SentSuccess;
    }

    private bool SendWapPush1(SerialPort serialPort, string message, string phone)
    {
        try
        {
            string[] pduCode = GetPDU("0", phone, PduEncode.Encoder.SMS.ENUM_TP_DCS.WapPush,
                PduEncode.Encoder.SMS.ENUM_TP_VALID_PERIOD.Maximum, 0, false, message);

            for (int i = 0; i < pduCode.Length; i++)
            {
                string pdu = pduCode[i];
                //pdu = "00" + pdu.Substring(4);
                int len = (pdu.Length - 2) / 2;
                if (!SendCMGSCommand(len.ToString()))
                {
                    SendESCCommand();
                    return false;
                }
                if (!ExecCommandStandar(pdu, ">", ExcuteErrors))
                {
                    SendESCCommand();
                    return false;
                }

                if (!sendCTRLZ(timeoutstandar))//đợi có kết quả nếu sau 10s mà ko gửi dc thì fail
                {
                    SendESCCommand();
                    return false;
                }
            }

        }
        catch (SendMessageType ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
            //throw ex;
            return false;
        }
        return true;
    }

    private bool SendWapPush2(string message, string phone)
    {
        try
        {
            message = message.Substring(0, message.IndexOf("http://")).Trim() + message.Substring(message.IndexOf("http://")).Trim();
            string[] pduCode = GetPDU("0", phone, PduEncode.Encoder.SMS.ENUM_TP_DCS.WapPush, PduEncode.Encoder.SMS.ENUM_TP_VALID_PERIOD.Maximum, 0, false, message);

            for (int i = 0; i < pduCode.Length; i++)
            {
                string pdu = pduCode[i];
                //pdu = "00" + pdu.Substring(4);
                int len = (pdu.Length - 2) / 2;
                if (!SendCMGSCommand(len.ToString()))
                {
                    SendESCCommand();
                    return false;
                }
                if (!ExecCommandStandar(pdu, ">", ExcuteErrors))
                {
                    SendESCCommand();
                    return false;
                }

                if (!sendCTRLZ(timeoutstandar))//đợi có kết quả nếu sau 10s mà ko gửi dc thì fail
                {
                    SendESCCommand();
                    return false;
                }
            }

        }
        catch (SendMessageType ex)
        {
            return false;
        }
        return true;
    }

    public string sendCUSD(string message)
    {
        //return ExecCommandSpecial("AT+CUSD=1,\"" + message + "\"", timeoutstandar, ",15");
        return ExecCommandSpecial("AT+CUSD=1,\"" + message + "\"", timeoutstandar, ",15\r\n");
    }

    /// <summary>
    /// Reset Modem
    /// </summary>
    /// <returns></returns>
    public bool ResetModem()
    {
        SendESCCommand();
        return ExecCommandStandar("ATZ\r\n", ExcuteOK, ExcuteErrors);
    }
    public bool SenCFUN(string message,string success)
    {
        return ExecCommandStandar(message, success, "");
    }

    public void ClosePort()
    {
        try
        {
            serialPort.DataReceived -= new SerialDataReceivedEventHandler(this.SerialPort_DataReceived);
            if(serialPort.IsOpen)
                serialPort.BaseStream.Flush();
            serialPort.Close();
            serialPort.Dispose();
        }
        catch (Exception ex)
        {
            WriteATCommandHandle(ex.Message);
            logger.Error(ex.ToString());
        }
    }

    
}