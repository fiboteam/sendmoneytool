using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Data;
using System.Data.SqlClient;
using log4net;
using Utility;
using ModemInterface;
using SMSHostingImplement;
using System.Text.RegularExpressions;
using BusinessObjects;
using System.Linq;
using SMSPDULib;

public class GSMModemSMSHosting : GSMModem
{
    private int BaudRate = 9600;
    public ModemDeviceStatus modemDeviceStatus = ModemDeviceStatus.EnableNetWork;
    public PortUtilities PortCore;
    public int timeoutconfig = 1800;
    public override bool IsOpen()
    {

        if (PortCore == null || PortCore.serialPort == null)
            return false;
        else
        {
            return PortCore.serialPort.IsOpen;
        }
    }
    public GSMModemSMSHosting()
    {
        PortCore = new PortUtilities();
    }
    public void OpenPort(string comport, int baudRate,int time)
    {
        TimeSpan t = TimeSpan.FromSeconds(time);
        PortCore.timeoutstandar = (int)t.TotalMilliseconds;
        PortCore.OpenPort(comport, baudRate, ref this.modemDeviceStatus);
        timeoutconfig = time * 1000;
    }
 
    public override SMSStatus SendSMS(string phoneNumber, string message, ContentType contentType)
    {
        //return PortCore.SendMSG(phoneNumber, message.Replace('\r', '\n'), contentType);
		return PortCore.SentByPDU(message, phoneNumber, SMSHostingContentType.Text, SMSHostingMessageType.NormalSMS);
    }
    public override SMSStatus SendSMS(string phoneNumber, string message)
    {
        //return PortCore.SendMSG(phoneNumber, message.Replace('\r','\n'), ContentType.Text);
		return PortCore.SentByPDU(message, phoneNumber, SMSHostingContentType.Text, SMSHostingMessageType.NormalSMS);
    }

    public override SMSStatus SendConcatenatedSMS(string phoneNumber, string message,int totalsms)
    {
        return SendConcatenatedSMS(phoneNumber, message.Replace('\r', '\n'), ContentType.Text,totalsms);
    }

	public override SMSStatus SendByPDU(string message,string phone,SMSHostingContentType contentype,SMSHostingMessageType messagetype)
	{
		return PortCore.SentByPDU(message,phone,contentype,messagetype);
	}
    public override SMSStatus SendFlashSMS(string phoneNumber, string message)
    {
        return PortCore.sendFlash(phoneNumber, message);
    }

    public override SMSStatus SendConcatenatedSMS(string phoneNumber, string message, ContentType contentType, int totalsms)
    {
        SMSStatus sta = SMSStatus.SentFailed;
        try
        {
            sta = PortCore.SendConcatenatedSMS(phoneNumber, message.Replace('\r', '\n'), contentType,totalsms);
        }
        catch
        {
            return SMSStatus.SentFailed;
        }
        return sta;
    }

    public SMSStatus SendConcatenatedSMSStandard(string phoneNumber, string message, ContentType contentType, int totalsms, ref int countSMSSuccess)
    {
        SMSStatus sta = SMSStatus.SentFailed;
        try
        {
            sta = PortCore.SendConcatenatedSMSStandard(phoneNumber, message.Replace('\r', '\n'), contentType, totalsms, ref countSMSSuccess);
        }
        catch
        {
            return SMSStatus.SentFailed;
        }
        return sta;
    }

    public override void Dispose()
    {
        PortCore.ClosePort();
    }

    public override System.Data.DataTable GetSMS()
    {
        int modemType = 1;

        string portStr = "";

        portStr = PortCore.sendCMGL("ALL",timeoutconfig);//REC UNREAD

        //return null;
        //+CMGL: 1,\"REC READ\",\"+9221\",,\"07/12/14,14:42:40+28\"\r\nSo 0906864307 da duoc 0903842740 nap 200,000 dong vao 14-12-2007 02:39PM. So giao dich:140400230.\r\n 

        string[] smsList = GFunction.SplitByString(portStr, "+CMGL:");

        if (smsList != null && smsList.Length < 2)
            return null;

        DataTable dt = new DataTable();
        dt.Columns.Add("SMSID");
        dt.Columns.Add("SMSStatus");
        dt.Columns.Add("PhoneNumber");
        dt.Columns.Add("Message");
        dt.Columns.Add("ReceivedDate");

        for (int i = 1; i < smsList.Length; i++)
        {
            string[] sms = GFunction.SplitByString(smsList[i], ",\"");
            DataRow dr = dt.NewRow();
            dr["SMSID"] = sms[0].Trim();
            dr["SMSStatus"] = sms[1].Replace("\"", "").Trim();
            dr["PhoneNumber"] = sms[2].Replace("\"", "").Replace(",", "").Trim();
            try
            {
                dr["ReceivedDate"] = sms[3].Substring(0, sms[3].IndexOf("\"\r\n")).Trim();
            }
            catch
            {
                dr["ReceivedDate"] = DateTime.Now.ToShortDateString() + "\"\r\n";
            }
            if (modemType == 1)
                //lỗi tin nhắn tới có giá trị STO SENT và STO UNSENT thì tin nhắn chỉ có 3 phần gây lỗi hệ thống
                if (sms[1].IndexOf("STO SENT") > -1 || sms[1].IndexOf("STO UNSENT") > -1)
                {
                    dr["Message"] = "Message Errors";
                }
                else
                {
                    try
                    {
                        
                        string message = sms[3].Substring(sms[3].IndexOf("\"\r\n") + "\"\r\n".Length).Trim().Replace("\0", "@");
                        if (message.EndsWith("\r\n\r\nOK"))
                            message = message.Substring(0, message.Length - "\r\n\r\nOK".Length);
                        if (Regex.IsMatch(message.ToString().Trim(), @"^[A-Z0-9]{26,600}$") && message.Trim().Split(' ').Length == 1)
                        {
                            int lenght = message.Length;
                            message = SMSPDULib.SMS.DecodeUCS2(message, lenght / 2);
                        }
                        dr["Message"] = message.Trim();
                        
                    }
                    catch (Exception)
                    {
                        dr["Message"] = sms[3].Substring(sms[3].IndexOf("\"\r\n") + "\"\r\n".Length).Trim();
                    }
                    
                    
                }
            else if (modemType == 2)
                if (sms[1].IndexOf("STO SENT") > -1 || sms[1].IndexOf("STO UNSENT") > -1)
                {
                    dr["Message"] = "Message Errors";
                }
                else
                {
                    dr["Message"] = sms[3].Substring(sms[3].IndexOf("\"\r\n") + "\"\r\n".Length).Trim();
                }
            dt.Rows.Add(dr);
        }
        return dt;

    }

    public override List<SmsInfo> GetSMS_V2()
    {
        
            ////////////////////////////////////////

        List<SmsInfo> dataList = new List<SmsInfo>();

        string portStr = "";

        portStr = SendCMGL("4", 30000, true);//REC UNREAD

        string[] smsList = GFunction.SplitByString(portStr, "+CMGL:");

        if (smsList != null && smsList.Length < 2)
        {
            return null;
        }

        for (int i = 1; i < smsList.Length; i++)
        {
            string[] sms = GFunction.SplitByString(smsList[i], ",");
            SmsInfo smsinfo = new SmsInfo()
            {
                index = int.Parse(sms[0].Trim()),
                stat = int.Parse(sms[1].Trim()),
                alpha = sms[2].Trim(),
                length = int.Parse(sms[3].Trim().Replace("\r", "").Split('\n')[0])
            };

            if (int.Parse(sms[1].ToString().Trim()) == (short)statCMGL.StoredSentMessage ||
                int.Parse(sms[1].ToString().Trim()) == (short)statCMGL.StoredUnsentMessage)
            {
                smsinfo.message = "Message Errors";
            }
            else
            {
                try
                {
                    SMSPDULib.SMS statusReport = new SMSPDULib.SMS();
                    string message = sms[3].Trim().Replace("\r", "").Split('\n')[1];
                    if (message.Contains("OK") || message.Contains("CMTI"))
                    {
                        return null;
                    }
                    int lenght = int.Parse(sms[3].Trim().Replace("\r", "").Split('\n')[0]);
                    SMSType smsType = SMSBase.GetSMSType(message);

                    switch (smsType)
                    {
                        case SMSType.SMS:
                            SMSPDULib.SMS.Fetch(statusReport, ref message);
                            break;
                        case SMSType.StatusReport:
                            SMSStatusReport.Fetch(statusReport, ref message);
                            break;
                    }
                    smsinfo.message = statusReport.Message;
                    smsinfo.phonenumber = statusReport.PhoneNumber;
                    smsinfo.receiveddate = statusReport.ServiceCenterTimeStamp;
                    smsinfo.total = statusReport.TotalParts;
                    smsinfo.part = statusReport.Part;
                    if (statusReport.UserDataHeader != null && statusReport.UserDataHeader.Length > 0)
                    {
                        foreach (var item in statusReport.UserDataHeader)
                        {
                            smsinfo.userdataheader += item;
                        }
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
            dataList.Add(smsinfo);
        }
        dataList = dataList.OrderBy(x => x.userdataheader).ToList();
        return dataList;
    }

    public override string CheckMoney(string Message)
    {
        try
        {
            string result = PortCore.sendCUSD(Message);
            return result;
        }
        catch
        {
            return "";
        }
    }
    public override bool DeleteMessage(string smsID)
    {
        try
        {
            string result = PortCore.sendCMGD_DeleteMessage(smsID);
            return result.Contains("\r\nOK\r\n");
        }
        catch
        {
            return false;
        }
        return false;
    }

    public SMSStatus SendWapPush(string phoneNumber, string message)
    {
        SMSStatus sta = SMSStatus.SentFailed;
        try
        {
            sta = PortCore.WapPushMessage(phoneNumber, message);
        }
        catch
        {
            sta = SMSStatus.SentFailed;

        }
        return sta;
    }
    /// <summary>
    /// Use to deposite money and check balance of SIM, mesage will be *101#, *100*13123123213#
    /// </summary>
    /// <param name="message"></param>
    /// <returns>return the message that sent back from Telco</returns>
    public string SendCUSD(string message)
    {
        return PortCore.sendCUSD(message);

    }
    public override bool Reset()
    {
        try
        {
            return PortCore.ResetModem();
        }
        catch (Exception ex)
        {
            return false;
        }
    }



    public override string SendCMGL(string message, int timeOut,bool usePDU=false)
    {
        if (!usePDU)
        {
            return PortCore.sendCMGL("ALL", timeOut);
        }
        else
        {
            if (PortCore.SendCMGFCommand(0))
            {
                string str= PortCore.sendCMGL("4", timeOut);
                PortCore.SendCMGFCommand(1);
                return str;
            }
            else
            {
                return string.Empty;
            }
        }
    }

    public void SendCommand(string Message,string end)
    {
        try
        {
            PortCore.SendCommand(Message, end);
        }
        catch (Exception)
        {
            
        }
    }


    public override bool RestartCOM(string message, string success)
    {
        try
        {
            return PortCore.SenCFUN(message, success);
        }
        catch
        {
            return false;
        }
    }
}
