﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using Utility;
using System.IO;

namespace ModemAdaptorMutilThreadV2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            if (PriorProcess() != null)
            {
                //MessageBox.Show("Another instance is already running.");
                return;
            }
            AppDomain.CurrentDomain.AppendPrivatePath("ModemInterface");
            AppDomain.CurrentDomain.AppendPrivatePath("ModemImplements");
            AppDomain.CurrentDomain.AppendPrivatePath("BussinessObject");
            AppDomain.CurrentDomain.AppendPrivatePath("Filters");
            AppDomain.CurrentDomain.AppendPrivatePath("Utility");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
        public static Process PriorProcess()
        {
            Process curr = Process.GetCurrentProcess();
            Process[] procs = Process.GetProcessesByName(curr.ProcessName);
            foreach (Process p in procs)
            {
                if ((p.Id != curr.Id) &&
                    (p.MainModule.FileName == curr.MainModule.FileName))
                {
                    p.Kill();
                }
            }
            return null;
        }
    }
}
