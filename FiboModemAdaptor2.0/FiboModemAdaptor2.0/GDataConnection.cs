using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Reflection;

namespace ModemAdaptorMutilThreadV2
{

    public class GDataConnection
    {
        private SqlConnection m_Conn = null;

        public GDataConnection(string conStr)
        {
            try
            {
                m_Conn = new SqlConnection(conStr);
                m_Conn.Open();
            }
            catch (Exception ex)
            {
                m_Conn.Close();
            }
        }

        public void Open()
        {
            if (m_Conn.State == ConnectionState.Closed)
                m_Conn.Open();
        }

        public void Close()
        {
            if (m_Conn.State == ConnectionState.Open)
                m_Conn.Close();
        }

        public SqlConnection Conn
        {
            get { return m_Conn; }
            set { m_Conn = value; }
        }

        public object ExecuteScalar(string strSql)
        {
            object objReturn = null;
            try
            {
                SqlCommand command = new SqlCommand(strSql, m_Conn);
                command.CommandTimeout = 300;
                objReturn = command.ExecuteScalar();
                if (objReturn == null)
                    objReturn = "null";

                command.Dispose();
            }
            catch (Exception e)
            {
                return null;
            }
            return objReturn;
        }

        public object ExecuteScalarThrowException(string strSql)
        {
            object objReturn = null;
            try
            {
                SqlCommand command = new SqlCommand(strSql, m_Conn);
                command.CommandTimeout = 300;
                objReturn = command.ExecuteScalar();
                if (objReturn == null)
                    objReturn = "null";

                command.Dispose();
            }
            catch (Exception err)
            {
                m_Conn.Close();
            }
            return objReturn;
        }

        public bool ExecuteNoneQueryThrowException(string strSql)
        {
            try
            {
                SqlCommand command = new SqlCommand(strSql, m_Conn);
                command.CommandTimeout = 300;
                command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                m_Conn.Close();
            }
            return true;
        }

        public bool ExecuteNoneQuery(string strSql)
        {
            try
            {
                SqlCommand command = new SqlCommand(strSql, m_Conn);
                command.CommandTimeout = 300;
                command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public int ExecuteNoneQueryWithResult(string strSql)
        {
            int mintResult;
            try
            {
                SqlCommand command = new SqlCommand(strSql, m_Conn);
                command.CommandTimeout = 300;
                mintResult = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                return -1;
            }
            return mintResult;
        }

        public DataTable GetDataTable(string strSql)
        {

            SqlDataAdapter da = null;
            DataTable tTable = new DataTable();
            string strError = "";
            try
            {
                da = new SqlDataAdapter(strSql, m_Conn);
                da.SelectCommand.CommandTimeout = 300;

                da.Fill(tTable);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                da.Dispose();
            }
            return tTable;
        }

        /// <summary>
        /// Execute a Store Procedure
        /// </summary>
        /// <param name="strStoreProcName">StoreProc Name</param>
        /// <param name="ParamsList">An array of InputParameters</param>
        /// <returns>True if successful</returns>
        public bool ExecuteStoreProcedure(string strStoreProcName, SqlParameter[] paraList)
        {
            SqlCommand sqlCMD = null;

            try
            {
                sqlCMD = new SqlCommand(strStoreProcName, m_Conn);
                sqlCMD.CommandTimeout = 300;
                sqlCMD.CommandType = CommandType.StoredProcedure;
                foreach (SqlParameter para in paraList)
                    sqlCMD.Parameters.Add(para);
                sqlCMD.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                sqlCMD.Dispose();
            }

            return true;
        }
        public DataTable ExecuteStoreProcedureReturnTable(string strStoreProcName, SqlParameter[] paraList)
        {
            SqlCommand sqlCMD = null;

            try
            {
                sqlCMD = new SqlCommand(strStoreProcName, m_Conn);
                sqlCMD.CommandTimeout = 300;
                sqlCMD.CommandType = CommandType.StoredProcedure;
                foreach (SqlParameter para in paraList)
                    sqlCMD.Parameters.Add(para);

                SqlDataAdapter DAdpt = new SqlDataAdapter(sqlCMD);
                DataSet ds = new DataSet();
                DAdpt.Fill(ds, "table");
                return ds.Tables["table"];

            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }

        public SqlParameter[] Take(BusinessObject businessObject)
        {
            List<SqlParameter> dataParam = new List<SqlParameter>();
            Type type = businessObject.GetType();
            foreach (PropertyInfo info in type.GetProperties())
            {
                if (info.PropertyType.FullName != null && (info.MemberType == MemberTypes.Property && info.PropertyType != typeof(IDictionary<string, object>)
                                                           && info.PropertyType.FullName.IndexOf("BusinessObject", StringComparison.Ordinal) == -1 ))
                {
                    SqlParameter para = new SqlParameter();

                    string fieldName = info.Name;
                    var obj = "@" + fieldName;
                    para.ParameterName = obj;

                    if (info.PropertyType.BaseType != null && info.PropertyType.BaseType.ToString().IndexOf("Enum", StringComparison.Ordinal) >= 0)
                    {
                        para.Value = (short)Enum.Parse(info.GetValue(businessObject, null).GetType(), info.GetValue(businessObject, null).ToString());
                    }
                    else
                    {
                        if (info.PropertyType.Name.Contains("Nullable"))
                        {
                            var data = info.GetValue(businessObject, null);
                            para.Value = data != null ? data : DBNull.Value;
                        }
                        else
                        {
                            para.Value = info.GetValue(businessObject, null);
                        }

                    }
                    dataParam.Add(para);
                }
            }
            return dataParam.ToArray();
        }
    }
}