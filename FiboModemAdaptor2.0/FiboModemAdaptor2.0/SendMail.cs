﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace ClientSite.Code
{
    public class SendMail
    {
        protected string strErrorMessage = "", strEmailTo, strEmailFrom, strEmailFromName, strEmailSubject, strEmailBody, strCC;
        protected bool isBodyHtml;
        protected bool isUseSSL = true;
        protected List<string> listCCs = new List<string>();

        public string ErrorMessage
        {
            get { return strErrorMessage; }
            set { strErrorMessage = value; }
        }

        public string EmailTo
        {
            get { return strEmailTo; }
            set { strEmailTo = value; }
        }

        public string EmailFrom
        {
            get { return strEmailFrom; }
            set { strEmailFrom = value; }
        }

        public string EmailFromName
        {
            get { return strEmailFromName; }
            set { strEmailFromName = value; }
        }

        public string EmailSubject
        {
            get { return strEmailSubject; }
            set { strEmailSubject = value; }
        }

        public string EmailBody
        {
            get { return strEmailBody; }
            set { strEmailBody = value; }
        }

        public bool IsBodyHtml
        {
            get { return isBodyHtml; }
            set { isBodyHtml = value; }
        }

        public List<string> CCs
        {
            get { return listCCs; }
            set { listCCs = value; }
        }

        public bool IsUseSSL
        {
            get { return isUseSSL; }
            set { isUseSSL = value; }
        }

        public string CC
        {
            get
            {
                return strCC;
            }
            set
            {
                strCC = value;
            }
        }

        public bool send()
        {
            try
            {
                SmtpSection smtpSec = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                //BinhDuongJobs'admin@binhduongjobs.com'
                string emailFromName = smtpSec.From.Substring(0, smtpSec.From.IndexOf("'"));
                string emailFrom = smtpSec.From.Substring(smtpSec.From.IndexOf("'") + 1, smtpSec.From.LastIndexOf("'") - smtpSec.From.IndexOf("'") - 1);

                MailAddress mailFrom = new MailAddress(emailFrom, emailFromName);

                MailAddress mailTo = new MailAddress(strEmailTo);

                MailMessage objEmail = new MailMessage(mailFrom, mailTo);

                objEmail.Subject = strEmailSubject;
                objEmail.IsBodyHtml = isBodyHtml;
                objEmail.BodyEncoding = Encoding.GetEncoding("utf-8");
                objEmail.Body = strEmailBody;
                if (!string.IsNullOrEmpty(strCC))
                    objEmail.CC.Add(strCC);

                SmtpClient objEmailSender = new SmtpClient();
                objEmailSender.Host = smtpSec.Network.Host;
                objEmailSender.Port = smtpSec.Network.Port;
                objEmailSender.Credentials = new System.Net.NetworkCredential(smtpSec.Network.UserName, smtpSec.Network.Password);
                objEmailSender.EnableSsl = false;
                //objEmailSender.ClientCertificates
                //SmtpDeliveryMethod dm = new SmtpDeliveryMethod();
                //objEmailSender.Credentials = new NetworkCredential(emailFrom, smtpSec.);
                //objEmailSender.DeliveryMethod= new SmtpDeliveryMethod
                objEmailSender.Send(objEmail);

                return true;
            }
            catch (Exception error)
            {
                this.ErrorMessage = error.ToString();
                return false;
            }
        }

        public static bool sendMail(string from, string pass, string to, string subject, string body)
        {
            bool result = false;

            try
            {
                SmtpClient host = new SmtpClient("smtp.gmail.com", 587);
                host.EnableSsl = true;
                host.DeliveryMethod = SmtpDeliveryMethod.Network;
                host.UseDefaultCredentials = false;
                host.Credentials = new NetworkCredential(from, pass);

                MailMessage mail = new MailMessage(from, to, subject, body);
                mail.IsBodyHtml = true;
                host.Send(mail);

                result = true;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}