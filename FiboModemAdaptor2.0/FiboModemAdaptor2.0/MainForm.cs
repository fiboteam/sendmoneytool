﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using log4net;
using System.Reflection;
using Utility;
using BusinessObjects;

using ModemInterface;
using ModemInterface.ModemAdaptorService;
using System.Globalization;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Web;
using System.Net;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using ClientSite.Code;
using DTO.Objects;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using DAO.Interface;
using DAO;

namespace ModemAdaptorMutilThreadV2
{
	public partial class MainForm : Form
	{
		private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private readonly ISMSSendMoney _smsSendMoneyRespository = DataAccess.SMSSendMoneyDao;

		#region System Variables

		public delegate void WriteRichTextBoxDelegate(string str, RichTextBox rtb);
		public WriteRichTextBoxDelegate mWriteRichTextBox = null;

		public delegate void WriteLabelDelegate(string str, Label lbl);
		public WriteLabelDelegate mWriteLabel = null;

		public delegate void WriteLabelMoneyDelegate(int money, Label lbltotal);
		public WriteLabelMoneyDelegate mWriteLabelMoney = null;

		public delegate void ChangeNumberOfThreadDelegate(int t);
		public ChangeNumberOfThreadDelegate mChangeNumberOfThread = null;

		public delegate void AutoWriteLogDelegate(RichTextBox rtb, string fileName);
		public AutoWriteLogDelegate mAutoWriteLog = null;

		#region NMHIEU
		public delegate void WriteMyLabelDelegate(int amount, Label lbl);
		public WriteMyLabelDelegate mWriteMyLabel = null;

		public delegate string ReadMyLabelDelegate(Label lbl);
		public ReadMyLabelDelegate mReadMyLabel = null;

		public delegate void WriteAmountPromotionSMSDelegate(int amount, string fileName);
		public WriteAmountPromotionSMSDelegate mWriteAmountPromotionSMS = null;

		public delegate int ReadAmountPromotionSMSDelegate(string fileName);
		public ReadAmountPromotionSMSDelegate mReadAmountPromotionSMS = null;
		#endregion

		ModemAdaptor modemAdaptor;
		ModemAdaptorServiceClient managerAdaptor;

		/// <summary>
		/// Tên modem adaptor
		/// </summary>
		private string mModemAdaptorName = "";

		/// <summary>
		/// Value true gửi tuần tự. Value false gửi cả ListSMS
		/// </summary>
		private bool mIsMultiThread;

		private string mUserName = "";
		private string mPassword = "";
		private int mMaxTextBoxLength = 1000000;
		bool mIsGetwayAdaptorPro;

		/// <summary>
		/// Xác định có khởi tạo modem hay không
		/// </summary>
		private bool mIsInitialModemAdaptor = true;

		/// <summary>
		/// Xác định có lấy tin nhắn khi modem bị lỗi hay không
		/// </summary>
		private bool mGetSMSWithModemError = false;

		private bool ViewCommand = false;




		#endregion

		#region Định nghĩa các biến dùng cho thread

		private bool mContinuteGetSMS = true;
		private object lockSendObject = new object();
		private object lockGetObject = new object();
		private object locktime = new object();
		private object locksendMT = new object();
		private object lockupdateMT = new object();
		private int mSMSPackage = 20;

		private int mCurSendTime = 0;
		private int mCurGetTime = 0;

		/// <summary>
		/// số lần sendMt của thiết bị
		/// </summary>
		private int mSendTime = 6;

		/// <summary>
		/// số lần getMO của thiết bị
		/// </summary>
		private int mGetTime = 1;

		private int mMaxThread = 1;
		private int mCurrentThread = 0;

		#endregion

		public BaseSMSProcessFactory SMSProcessFactory = null;

		private bool _usePushSMSToClient = false;

		private int _limitFail = 5;
		private bool _AllowSendSMSAlert = false;

		/// <summary>
		/// Lấy số tin nhắn có thể gửi theo tiền
		/// 16/05/2015
		/// NMH
		/// </summary>
		private BackgroundWorker _bgwGetSMSCanSent;

		private BackgroundWorker _bgwRestartCOM;

		/// <summary>
		/// Danh sách các bgw
		/// 16/05/2015
		/// NMH
		/// </summary>
		private List<BackgroundWorker> _bgws;

		/// <summary>
		/// Cần check lại số sms
		/// 16/05/2015
		/// NMH
		/// </summary>
		private bool _requestGetSMSCanSent = true;
		/// <summary>
		/// Số sms có thể send
		/// 18/05/2015
		/// NMH
		/// </summary>
		private int _smsCanSent = 0;
		/// <summary>
		/// Thời gian lần sau lấy số tin nhắn có thể gửi
		/// 18/05/2015
		/// NMH
		/// </summary>
		private DateTime _nextTimeGetSMSCanSent = DateTime.Now;
		/// <summary>
		/// Gửi mail báo hết tiền
		/// 18/05/2015
		/// NMH
		/// </summary>
		private bool _sentAlertMail = true;
		/// <summary>
		/// can restart com
		/// </summary>
		private bool _RequiredRestartCOM = false;
		/// <summary>
		/// đếm số tin nhắn
		/// </summary>
		private int _countCunrrentSMSSentFail = 0;
		/// <summary>
		/// Modem này chỉ dùng đế lấy tin nhắn
		/// </summary>
		public bool _ThisOnlyGetMO { get; set; }
		public DateTime NextSendMoney { get; set; }

		private List<SMSSendMoney> _ListSendMoney { get; set; }

		public int TotalSendMoney 
		{
			get 
			{
				if (_ListSendMoney == null)
				{
					return 0;
				}
				else
				{
					return _ListSendMoney.Select(sms => sms.SMSSendMoneyStatus == SMSSendMoneyStatus.New).ToList().Count();
				}
			}
		}

		public SMSSendMoney GetToSendMoney 
		{ 
			get
			{
				try
				{
					if (_ListSendMoney == null)
					{
						return null;
					}
					else
					{
						(from sms in _ListSendMoney where sms.SMSSendMoneyStatus == SMSSendMoneyStatus.New select sms).First().Update(sms => sms.SMSSendMoneyStatus = SMSSendMoneyStatus.Sending);

						//int index = _ListSendMoney.FindIndex(sms => sms.SMSSendMoneyStatus == SMSSendMoneyStatus.Sending);

						//SMSSendMoney smssned = _ListSendMoney[index];
						return _ListSendMoney.Where(sms => sms.SMSSendMoneyStatus == SMSSendMoneyStatus.Sending).Select(sms => sms).First();
						//return smssned;
					}
				}
				catch(Exception ex)
				{
					return null;
				}
			}
		}

		public int GetTotalSendSuccess
		{
			get
			{
				try
				{
					if (_ListSendMoney == null)
					{
						return 0;
					}
					else
					{
						return _ListSendMoney.Where(sms => sms.SMSSendMoneyStatus == SMSSendMoneyStatus.Success).Select(sms => sms).ToList().Count();
					}
				}
				catch (Exception ex)
				{
					return 0;
				}
			}
		}

		public int GetTotalSendFail
		{
			get
			{
				try
				{
					if (_ListSendMoney == null)
					{
						return 0;
					}
					else
					{
						return _ListSendMoney.Where(sms => sms.SMSSendMoneyStatus == SMSSendMoneyStatus.Fail).Select(sms => sms).ToList().Count();
					}
				}
				catch (Exception ex)
				{
					return 0;
				}
			}
		}

		public SMSSendMoney UpdateSendStatus
		{
			set
			{
				try
				{
					_ListSendMoney.Where(sms => sms.PhoneNumber == value.PhoneNumber && sms.Money == value.Money && sms.SMSSendMoneyStatus == SMSSendMoneyStatus.Sending).Select(sms => sms).First().Update(sms => { sms.SMSSendMoneyStatus = value.SMSSendMoneyStatus; });
				}
				catch
				{

				}
			}
		}

		public SMSSendMoney UpdatePushStatus
		{
			set
			{
				try
				{
					_ListSendMoney.Where(sms => sms.PhoneNumber == value.PhoneNumber && sms.Money == value.Money && sms.SMSSendMoneyStatus == value.SMSSendMoneyStatus && sms.SMSSendMoneyPushStatus == SMSSendMoneyPushStatus.New).Select(sms => sms).First().Update(sms => { sms.SMSSendMoneyPushStatus = value.SMSSendMoneyPushStatus; });
				}
				catch
				{

				}
			}
		}

		public MainForm()
		{
			InitializeComponent();

			InitialParameters();
			InitialDelegate();
			LoadSystemConfig();
			bool autoStart = Convert.ToBoolean(GFunction.GetAppConfigParameter("AutoStart"));

			if (autoStart)
			{
				trSend.Enabled = true;
				btnStartSend.Text = "Stop";
			}
			UpdateSendTimer();
			bool EnableDelaySMS = Convert.ToBoolean(GFunction.GetAppConfigParameter("EnableDelaySMS"));
			cbAccept.Checked = EnableDelaySMS;


			_bgwGetSMSCanSent = new BackgroundWorker();
			_bgwGetSMSCanSent.DoWork += _bgwGetSMSCanSend_DoWork;
			_bgwGetSMSCanSent.RunWorkerCompleted += _bgwGetSMSCanSend_RunWorkerCompleted;

			_bgwRestartCOM = new BackgroundWorker();
			_bgwRestartCOM.DoWork += _bgwRestartCOM_DoWork;
			_bgwRestartCOM.RunWorkerCompleted += _bgwRestartCOM_RunWorkerCompleted;

			_bgws = new List<BackgroundWorker>();
			_bgws.Add(_bgwGetSMSCanSent);
			_bgws.Add(bgwSendMT);
			_bgws.Add(_bgwRestartCOM);

			test();

		}

		private void _bgwRestartCOM_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			_RequiredRestartCOM = false;
			_countCunrrentSMSSentFail = 0;
		}

		private void _bgwRestartCOM_DoWork(object sender, DoWorkEventArgs e)
		{
			RestartCOM();
		}
		/// <summary>
		/// bgw lấy số tin nhắn từ tiền
		/// 16/05/2015
		/// NMH
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void _bgwGetSMSCanSend_DoWork(object sender, DoWorkEventArgs e)
		{
			//if(_nextTimeGetSMSCanSent > DateTime.Now)
			//{
			//    TimeSpan time = _nextTimeGetSMSCanSent - DateTime.Now;
			//    BeginInvoke(mWriteRichTextBox, new object[] { "Wait " + (int)time.TotalMinutes + " minutes before Get SMS Can Send", rtbSend });
			//    Thread.Sleep(time);
			//}

			//Invoke(mWriteRichTextBox, new object[] { "Get SMS by balance", rtbSend });
			BeginInvoke(mWriteRichTextBox, new object[] { "Get SMS by balance", rtbSend });

			//Doc danh sach pattern cua main balance
			//List<string> patterns = new List<string>();
			string content = "";
			using (StreamReader reader = new StreamReader("Template\\MainBalance.txt"))
			{
				content = reader.ReadToEnd();
			}

			content = content.Replace("\r", "");
			string[] pts = content.Split('\n');

			e.Result = SMSProcessFactory.GetSMSCanSent("*101#", pts);
		}
		/// <summary>
		/// bgw lấy số tin nhắn từ tiền - completed
		/// 16/05/2015
		/// NMH
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void _bgwGetSMSCanSend_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			_requestGetSMSCanSent = false;
			int balance = Convert.ToInt32(e.Result);
			int price = Convert.ToInt32(ConfigurationManager.AppSettings["Price"]);
			_smsCanSent = balance / price;
			BeginInvoke(mWriteRichTextBox, new object[] { "Balance is " + balance + " => " + _smsCanSent + " sms", rtbSend });
			BeginInvoke(mWriteRichTextBox, new object[] { "End Get SMS by balance", rtbSend });
			Invoke(mWriteLabel, new object[] { _smsCanSent.ToString(), lblAmountSMSKM });
			_nextTimeGetSMSCanSent = _nextTimeGetSMSCanSent.AddMinutes(10);

			if (_smsCanSent <= Int32.Parse(ConfigurationManager.AppSettings["MinOfPromotionSMS"]))
			{
				//hết tin thì báo mail
				#region Send mail alert
				if (_sentAlertMail)
				{
					///send mail báo hết tiền
					try
					{
						var oMail = new SendMail
						{
							EmailFromName = ConfigurationManager.AppSettings["ModemAdaptorName"],
							EmailTo = "hieu.nm@fibo.vn",
							IsBodyHtml = true,
							EmailSubject = "Modem: " + ConfigurationManager.AppSettings["ModemAdaptorName"].ToString() + " Hết tiền gửi tin, stop modem",
							EmailBody = "Modem: " + ConfigurationManager.AppSettings["ModemAdaptorName"].ToString() + " Hết tiền gửi tin, stop modem."
						};
						oMail.send();
					}
					catch (Exception ex)
					{
						this.BeginInvoke(mWriteRichTextBox, new object[] { "Send mail alert fail: " + ex.ToString(), rtbSend });
					}
					finally
					{
						_sentAlertMail = false;
					}
				}
				#endregion End Send mail alert
			}
			//_smsCanSent = 100;
		}

		/// <summary>
		/// Lấy class hiện thực theo tên trong app config
		/// </summary>
		/// <param name="projectimplement"> </param>
		/// <param name="classname"></param>
		/// <param name="objs"></param>
		/// <returns></returns>
		public object GetNewType(string projectimplement, string classname, object[] objs)
		{
			Assembly a = Assembly.Load(projectimplement);
			Type[] types = a.GetTypes();
			foreach (var item in types)
			{
				if (item.Name == classname)
				{
					object newInstance = Activator.CreateInstance(item, objs);
					return newInstance;
				}
			}
			return null;
		}

		/// <summary>
		/// Load Các tham số trong file app.config
		/// </summary>
		public void InitialParameters()
		{
			_AllowSendSMSAlert = Convert.ToBoolean(ConfigurationManager.AppSettings["AllowSendSMSAlert"]);
			_limitFail = Convert.ToInt32(ConfigurationManager.AppSettings["LimitFail"]);
			_usePushSMSToClient = Convert.ToBoolean(ConfigurationManager.AppSettings["UsePushSMSToClient"]);
			mGetSMSWithModemError = Convert.ToBoolean(ConfigurationManager.AppSettings["GetSMSWithModemError"]);
			mMaxThread = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfThread"]);
			mModemAdaptorName = ConfigurationManager.AppSettings["ModemAdaptorName"];
			mUserName = ConfigurationManager.AppSettings["UserName"];
			mPassword = ConfigurationManager.AppSettings["Password"];
			mMaxTextBoxLength = Convert.ToInt32(GFunction.GetSystemConfigParameter("LengthOfFile"));
			lblModemName.Text = mModemAdaptorName;
			lblModemPartner.Text = ConfigurationManager.AppSettings["ModemAdaptorPartner"];
			mIsInitialModemAdaptor = Convert.ToBoolean(ConfigurationManager.AppSettings["InitialModemAdaptor"]);
			mIsGetwayAdaptorPro = Convert.ToBoolean(ConfigurationManager.AppSettings["GatewayAdaptorPro"]);
			_ThisOnlyGetMO = Convert.ToBoolean(ConfigurationManager.AppSettings["ThisOnlyGetMO"]);
 			

			var list = new List<object>
                           {
                               mModemAdaptorName, 
                               mUserName, 
                               mPassword, 
                               mIsGetwayAdaptorPro, 
                               logger
                           };
			string ImplementDLL = ConfigurationManager.AppSettings["ImplementDLL"];
			object obj = GetNewType(ImplementDLL, ConfigurationManager.AppSettings["ClassProcess"], list.ToArray());
			if (obj == null)
			{
				DialogResult result = MessageBox.Show("Kiểm tra config tên modem adaptor", "Thông báo", MessageBoxButtons.OK);
				if (result == DialogResult.OK)
					Environment.Exit(0);
			}
			Text = string.Format("{0} - {1}", ConfigurationManager.AppSettings["ModemAdaptorPartner"], ConfigurationManager.AppSettings["ModemAdaptorName"]);

			managerAdaptor = new ModemAdaptorServiceClient("WSHttpBinding_IModemAdaptorService");
			SMSProcessFactory = (BaseSMSProcessFactory)obj;
			SMSProcessFactory.managerAdaptor = managerAdaptor;
			rtbSend.Text += SMSProcessFactory.GetToken() + "\r\n";
			SMSProcessFactory.ModemAdaptor = new ModemAdaptor()
			{
				BaudRate = int.Parse(ConfigurationManager.AppSettings["BaudRate"].ToString()),
				ClientID = 6570,
				ComPort = ConfigurationManager.AppSettings["ComPort"].ToString(),
				ModemAdaptorName = ConfigurationManager.AppSettings["ModemAdaptorName"].ToString(),
				ModemAdaptorStatus = ModemAdaptorStatus.Active,
			};
			//modemAdaptor = SMSProcessFactory.Login();
			//if (modemAdaptor != null)
			//{
			//	lblTypeOfModem.Text = modemAdaptor.ModemType.ToString();
			//	this.Text = string.Format("Bắn tiền {0}",
			//		ConfigurationManager.AppSettings["ModemAdaptorName"]);
			//	mSendTime = modemAdaptor.SendTime;
			//	mGetTime = modemAdaptor.GetTime;
			//	lblListServiceType.Text = string.Join("--", SMSProcessFactory.listServiceTypeID);
			//	lblCurrentServiceType.Text = SMSProcessFactory.listServiceTypeID[0] + "";
			//	SMSProcessFactory.ViewLogEvent += SMSProcessFactory_ViewLogEvent;
			//}

			lblTypeOfModem.Text = ConfigurationManager.AppSettings["ModemAdaptorName"];
			this.Text = string.Format("Bắn tiền {0}",
				ConfigurationManager.AppSettings["ModemAdaptorName"]);
			mSendTime = 10;
			mGetTime = 0;
			lblListServiceType.Text = "bắn tiền";
			lblCurrentServiceType.Text = "bắn tiền";
			SMSProcessFactory.ViewLogEvent += SMSProcessFactory_ViewLogEvent;
		}

		void SMSProcessFactory_ViewLogEvent(string Message)
		{
			//if (ViewCommand)
			//	this.BeginInvoke(mWriteRichTextBox, new object[] { Message, rtbSend });
			//if (Message == "Hết tiền")
			//	this.BeginInvoke(mWriteRichTextBox, new object[] { Message, rtbSend });
			this.BeginInvoke(mWriteRichTextBox, new object[] { Message, rtbSend });

		}

		/// <summary>
		/// Khởi tạo Delegate
		/// </summary>
		public void InitialDelegate()
		{
			mWriteRichTextBox = WriteRichTextBox;
			mWriteLabel = WriteLabel;
			mWriteMyLabel = WriteMyLabel;
			mWriteLabelMoney = WriteLabelMoney;
			mChangeNumberOfThread = ChangeNumberOfThread;
			mAutoWriteLog = WriteLog;
			mReadMyLabel = ReadMyLabel;
			//mWriteAmountPromotionSMS = WriteAmountPromotionSMSFromFile;
			//mReadAmountPromotionSMS = ReadAmountPromotionSMSFromFile;
		}
		public void WriteLabelMoney(int money, Label lbl)
		{
			try
			{
				lbl.Text = (Convert.ToInt32(lbl.Text) - money).ToString();
			}
			catch { }
		}
		public void UpdateSendTimer()
		{
			trSend.Interval = Convert.ToInt32(txtInterval.Text);
			lblSendTimerStatus.Text = trSend.Enabled.ToString();
		}

		public void CheckKeyPress_Number(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar > 31 && (e.KeyChar < '0' || e.KeyChar > '9'))
				e.Handled = true;
		}

		#region Implement Delegate Function

		public void WriteRichTextBox(string str, RichTextBox rtb)
		{
			try
			{
				rtb.AppendText(str + "\n");
			}
			catch { }
		}

		public void WriteMyLabel(int amount, Label lbl)
		{
			int oldAmount;
			int.TryParse(lbl.Text.ToString(), out oldAmount);
			int newAmount = oldAmount - amount;
			if (newAmount < 0)
			{
				newAmount = 0;
			}
			try
			{
				lbl.Text = newAmount.ToString();
			}
			catch { }
		}

		public void WriteLabel(string str, Label lbl)
		{
			try
			{
				lbl.Text = str;
			}
			catch { }
		}

		public string ReadMyLabel(Label lbl)
		{
			string text = "";
			try
			{
				text = lbl.Text;
			}
			catch { }
			return text;
		}

		public void ChangeNumberOfThread(int t)
		{
			try
			{
				lbTotalThread.Text = ((int)(Convert.ToInt32(lbTotalThread.Text) + t)).ToString();
			}
			catch
			{
			}
		}

		public void WriteLog(RichTextBox rtb, string fileName)
		{
			try
			{
				if (rtb.TextLength >= mMaxTextBoxLength)
				{
					DateTime currentDate = DateTime.Now;
					string file = GFunction.GetSystemConfigParameter("LogFilePath") + fileName + "-" + currentDate.ToLongDateString() + ".rtf";
					rtb.SaveFile(file, RichTextBoxStreamType.RichNoOleObjs);
					rtb.Clear();
				}
			}
			catch (Exception)
			{
			}
		}

		#endregion

		#region Config System

		public void LoadSystemConfig()
		{
			txtInterval.Text = GFunction.GetSystemConfigParameter("Interval");
			txtNumberOfThread.Text = GFunction.GetSystemConfigParameter("NumberOfThread");
			txtLengOfFile.Text = GFunction.GetSystemConfigParameter("LengthOfFile");
			txtLogFilePath.Text = GFunction.GetSystemConfigParameter("LogFilePath");
			mIsMultiThread = Convert.ToBoolean(GFunction.GetSystemConfigParameter("MultiThread"));
			cbMultiThread.Checked = mIsMultiThread;
		}

		private void btnSaveConfig_Click(object sender, EventArgs e)
		{
			bool check = true;

			//Nếu modem không hỗ trợ chay tuần tự
			if (cbMultiThread.Checked == true && SMSProcessFactory.SupportSendSequentially == false)
			{
				DialogResult result = MessageBox.Show("Soft hiện tại không hỗ trợ chạy tuần tự", "Thông báo", MessageBoxButtons.OKCancel);
				cbMultiThread.Checked = false;
				check = false;
			}
			else if (cbMultiThread.Checked == false && SMSProcessFactory.SupportSendList == false)
			{   //Nếu modem không hỗ trợ send cả ListSMS
				DialogResult result = MessageBox.Show("Soft hiện tại không hỗ trợ send cả ListSMS", "Thông báo", MessageBoxButtons.OKCancel);
				cbMultiThread.Checked = true;
				check = false;
			}
			if (check)
			{
				if (txtInterval.Text.Length > 0)
					GFunction.SetValueForSystemConfigParameter("Interval", txtInterval.Text);
				if (txtNumberOfThread.Text.Length > 0)
					GFunction.SetValueForSystemConfigParameter("NumberOfThread", txtNumberOfThread.Text);
				if (txtLengOfFile.Text.Length > 0)
					GFunction.SetValueForSystemConfigParameter("LengthOfFile", txtLengOfFile.Text);
				if (txtLogFilePath.Text.Length > 0)
					GFunction.SetValueForSystemConfigParameter("LogFilePath", txtLogFilePath.Text);

				mIsMultiThread = cbMultiThread.Checked;
				GFunction.SetValueForSystemConfigParameter("MultiThread", mIsMultiThread.ToString());
				UpdateSendTimer();
				MessageBox.Show("SaveSuccess");
			}
		}

		#endregion

		#region Timer

		public bool checkFirstTime = Convert.ToBoolean(ConfigurationManager.AppSettings["checkFirstTime"]);
		private void SendMT_DoWork(object sender, DoWorkEventArgs e)
		{
			lock (locksendMT)
			{
				if (mIsInitialModemAdaptor && modemAdaptor == null)
					return;

				int maxThread = Convert.ToInt32(GFunction.GetSystemConfigParameter("NumberOfThread"));
				int curThread = Convert.ToInt32(lbTotalThread.Text);

				this.Invoke(mAutoWriteLog, new object[] { rtbSend, rtbSend.Name });


				try
				{
					if (curThread > maxThread)
						return;
					// cập nhật trạng thái những tin gửi fail nếu có. 
					//Trong lúc gửi nếu update status thất bại tn sẽ được add vào SMSListUpdateFail để update trạng thái lại vào lần sau
					if (SMSProcessFactory.SMSListUpdateFail != null && SMSProcessFactory.SMSListUpdateFail.Count != 0)
					{
						this.BeginInvoke(mWriteRichTextBox, new object[] { "Update lại tin bị lỗi. \n", rtbSend });
						for (int i = SMSProcessFactory.SMSListUpdateFail.Count - 1; i >= 0; i--)
						{
							SMS sms = SMSProcessFactory.SMSListUpdateFail[i];
							SMSProcessFactory.SMSListUpdateFail.RemoveAt(i);
							var task = Task.Factory.StartNew(() => UpdateSMSStatusAgain(sms));
						}
					}

					///Không còn sms để gửi thì ko lấy tin
					///NMH
					///25/05/2015
					if (_smsCanSent <= Int32.Parse(ConfigurationManager.AppSettings["MinOfPromotionSMS"]))
					{
						this.BeginInvoke(mWriteRichTextBox, new object[] { "Don't Get SMS To Send Because: _smsCanSent{" + _smsCanSent + "}\n", rtbSend });
					}
					else
					{
						//Khởi tạo thiết bị thành công mới lấy tin nhắn về. Bước chuẩn bị gửi tin
						string getsmsresult = SMSProcessFactory.PrepareModemAdapter();
						if (SMSProcessFactory.AcceptSendSMS)
						{
							if (checkFirstTime)
							{

								this.BeginInvoke(mWriteRichTextBox, new object[] { "Đang kiểm tra tiền lần đầu sử dụng \n", rtbSend });
								long money = SMSProcessFactory.GetMoney();
								this.Invoke(mWriteLabel, new object[] { money + "", lblCurrentMoney });
								this.Invoke(mWriteLabel, new object[] { money + "", lblCaculatorMoney });
								checkFirstTime = false;
							}
							if (getsmsresult != "")
							{
								this.BeginInvoke(mWriteRichTextBox, new object[] { "Khởi tạo thiết bị để gửi tin thất bại. " + getsmsresult + "\n", rtbSend });

								//nếu prepare fail thi restart com thử
								RestartCOM();

								//Nếu chấp nhận lấy tin kể cả lúc modem bị hỏng
								//Cho phép gửi để update SendFail cho tin nhắn thì cứ gọi lấy tin
								if (mGetSMSWithModemError)
								{
									this.BeginInvoke(mWriteRichTextBox, new object[] { "Vẫn lấy tin để gửi\n", rtbSend });
									int CurrentServiceType = SMSProcessFactory.GetListSMS();
									string label = CurrentServiceType + "";
									this.Invoke(mWriteLabel, new object[] { label, lblCurrentServiceType });
								}
								else
								{
									this.BeginInvoke(mWriteRichTextBox, new object[] { "Không lấy tin để gửi\n", rtbSend });
								}

							}
							else
							{   //lấy tin khi modem hoạt động tốt
								int CurrentServiceType = SMSProcessFactory.GetListSMS();
								string label = CurrentServiceType + "";
								this.Invoke(mWriteLabel, new object[] { label, lblCurrentServiceType });
							}
						}
						else
						{
							SMSProcessFactory.SMSList = null;
							this.BeginInvoke(mWriteRichTextBox, new object[] { "Không lấy tin để gửi do bị chặn time config. Số sms: " + SMSProcessFactory.CurrentSMS + "\n", rtbSend });
						}

						if (SMSProcessFactory.SMSList != null && SMSProcessFactory.SMSList.Count != 0)
						{
							this.BeginInvoke(mWriteRichTextBox, new object[] { "Sending total " + SMSProcessFactory.SMSList.Count + " SMS\n", rtbSend });

							//duyệt và gửi tuần tự
							for (int i = 0; i < SMSProcessFactory.SMSList.Count; i++)
							{
								SMS sms = SMSProcessFactory.SMSList[i];
								this.Invoke(mChangeNumberOfThread, 1);

								//if (amountSMSPromotion < sms.TotalSMS)
								//{
								//    //nêu gui theo so luong sms khuyen mai
								//    //và so tin cần gui lớn hơn so smm còn lại thì không cho gửi
								//    this.BeginInvoke(mWriteRichTextBox, new object[] { "This SMS have total more than amount Promotion sms. Send Error\n", rtbSend });
								//    ResultSendSMS result = new ResultSendSMS();
								//    //result.SMSStatus = SMSStatus.SentFailed;
								//    result.GetResult(ModemAdaptorStatus.Active, ModemDeviceStatus.EnableNetWork, SMSStatus.SentFailed, true);
								//    var task = Task.Factory.StartNew(() => UpdateSMSStatus(result, sms, 0));
								//}
								//else
								//{
								//Kiểm tra số phone. chỉ chấp nhân số phone chỉ có chử số
								if (!CheckPhoneNumber(sms.PhoneNumber))
								{
									//xử lý khi số phone bị lỗi
									var taskWrongPhoneNumber = Task.Factory.StartNew(() => WrongPhoneNumber(sms));
									continue;
								}
								//gửi tin
								int countSMSSuccess = 0;
								var task = Task.Factory.StartNew(() => SMSProcessFactory.SendMT(sms, ref countSMSSuccess));

								if (mIsMultiThread)//nếu lựa chọn gửi tuần tự
								{
									Task.WaitAll(task);//đặt vào task để gửi tin tuần tự
								}
								//cập nhật status và thông báo nếu xảy ra lỗi
								task.ContinueWith(t => UpdateSMSStatus(task.Result, sms, countSMSSuccess));
								//}
							}
						}
						else
						{
							this.BeginInvoke(mWriteRichTextBox, new object[] { "Sending total 0 SMS\n", rtbSend });
						}
						///
					}
				}
				catch (Exception ex)
				{
					SMSProcessFactory.InsertLog(ex);
					//ngắt kết nối khi xảy ra lỗi đề phòng soft kết nối lại không dc
					SMSProcessFactory.smsSender.Reset();
					string errors = SMSProcessFactory.ErrorSendMT();
					if (errors != "") this.BeginInvoke(mWriteRichTextBox, new object[] { errors + "\n", rtbSend });
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Quá trình gửi tin xảy ra lỗi. Thời gian " + DateTime.Now.ToString() + "\n", rtbSend });
					logger.Debug(ex.Message + string.Format("Function SendMT_DoWork - Value of Parameters: Time: {0}", DateTime.Now), ex);
				}
				finally
				{
					mCurSendTime++;
					UpdateContinuteGetSMSSynchronous(true);

				}
			}
		}
		/// <summary>
		/// Khởi động lại COM
		/// </summary>
		private void RestartCOM()
		{
			this.BeginInvoke(mWriteRichTextBox, new object[] { "Restart COM \n", rtbSend });
			if (SMSProcessFactory.smsSender.Reset() && Convert.ToBoolean(ConfigurationManager.AppSettings["AllowRestartCOMWhenPrepareFail"]))
			{
				if (SMSProcessFactory.smsSender.RestartCOM("AT+CFUN=0", "OK\r\n"))
				{
					Thread.Sleep(3000);
					if (SMSProcessFactory.smsSender.RestartCOM("AT+CFUN=1", "OK\r\n"))
					{
						this.BeginInvoke(mWriteRichTextBox, new object[] { "Restart COM success, please wait for 20 seconds\n", rtbSend });
						Thread.Sleep(20000);
					}
				}

			}
		}
		/// <summary>
		/// lấy tin lên cho dịch vụ đầu tiên
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void GetMO_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "Bắt đầu lấy MO", rtbSend });
				this.BeginInvoke(mWriteRichTextBox, new object[] { "Bắt đầu lấy MO", rtbUpdate });
				//chuẩn bị thiết bị để gửi tin
				string getsmsresult = SMSProcessFactory.PrepareModemAdapter();
				if (getsmsresult != "")
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Khởi tạo thiết bị để gửi tin thất bại. " + getsmsresult + "\n", rtbUpdate });
					//Nếu chấp nhận lấy tin kể cả lúc modem bị hỏng
					//Cho phép gửi để update SendFail cho tin nhắn thì cứ gọi lấy tin
					if (mGetSMSWithModemError)
						SMSProcessFactory.GetMO(SMSProcessFactory.listServiceTypeID[0]);
				}
				else
				{   //lấy tin khi modem hoạt động tốt
					SMSProcessFactory.GetMO(SMSProcessFactory.listServiceTypeID[0]);
				}

				if (SMSProcessFactory.SMSListMO != null && SMSProcessFactory.SMSListMO.Count != 0)
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Getting total " + SMSProcessFactory.SMSListMO.Count + " SMS\n", rtbUpdate });
					//duyệt và gửi tuần tự
					for (int i = 0; i < SMSProcessFactory.SMSListMO.Count; i++)
					{
						this.Invoke(mChangeNumberOfThread, 1);
						ClientCommingSMSHostingData sms = SMSProcessFactory.SMSListMO[i];
						var task = Task.Factory.StartNew(() => UpdateMO(sms));
						if (mIsMultiThread)//nếu lựa chọn gửi tuần tự
						{
							Task.WaitAll(task);//đặt vào task để gửi tin tuần tự
						}
					}

				}

			}
			catch (Exception ex)
			{
				SMSProcessFactory.InsertLog(ex);
				SMSProcessFactory.ErrorGetMO();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "Quá trình nhận MO xảy ra lỗi. Thời gian " + DateTime.Now.ToString() + "\n", rtbSend });
				logger.Debug(ex.Message + string.Format("Function GetMO_DoWork - Value of Parameters: Time: {0}", DateTime.Now), ex);
			}
			finally
			{
				mCurGetTime += 1;
			}
		}

		private void V2_GetMO_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "Bắt đầu lấy MO servicetype: " + SMSProcessFactory.listServiceTypeID[0], rtbSend });
				//this.BeginInvoke(mWriteRichTextBox, new object[] { "Bắt đầu lấy MO", rtbUpdate });
				//chuẩn bị thiết bị để gửi tin
				string getsmsresult = SMSProcessFactory.PrepareModemAdapter();
				string status = "";
				if (getsmsresult != "")
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Khởi tạo thiết bị để gửi tin thất bại. " + getsmsresult + "\n", rtbUpdate });
					//Nếu chấp nhận lấy tin kể cả lúc modem bị hỏng
					//Cho phép gửi để update SendFail cho tin nhắn thì cứ gọi lấy tin
					if (mGetSMSWithModemError)
					{
						SMSProcessFactory.GetMO_V2(SMSProcessFactory.listServiceTypeID[0], ref status);
					}
				}
				else
				{   //lấy tin khi modem hoạt động tốt
					SMSProcessFactory.GetMO_V2(SMSProcessFactory.listServiceTypeID[0], ref status);
				}

				if (status == "Message Not Null")
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Có tin dài nhưng không nhận đủ", rtbUpdate });
				}

				if (SMSProcessFactory.dataIncomming != null && SMSProcessFactory.dataIncomming.Count != 0)
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Getting total " + SMSProcessFactory.dataIncomming.Count + " SMS\n", rtbUpdate });
					//duyệt và gửi tuần tự
					for (int i = 0; i < SMSProcessFactory.dataIncomming.Count; i++)
					{
						this.Invoke(mChangeNumberOfThread, 1);
						IncommingSMS sms = SMSProcessFactory.dataIncomming[i];
						var task = Task.Factory.StartNew(() => UpdateMO_V2(sms));
						if (mIsMultiThread)//nếu lựa chọn gửi tuần tự
						{
							Task.WaitAll(task);//đặt vào task để gửi tin tuần tự
						}
					}

				}
				else
				{
					//Nếu 1 list có 40 tin nếu lỗi thì báo 40 lần => not good
					//Chi cần báo 1 lần để check.
					//Nếu 1 list tất cả các tin đều thành công thì mới cho sẵn sàng để nt alert
					SMSProcessFactory.SendSMSAlert = true;
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Getting total " + 0 + " SMS\n", rtbUpdate });
				}

			}
			catch (Exception ex)
			{
				SMSProcessFactory.InsertLog(ex);
				SMSProcessFactory.ErrorGetMO();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "Quá trình nhận MO xảy ra lỗi. Thời gian " + DateTime.Now.ToString() + "\n", rtbSend });
				logger.Debug(ex.Message + string.Format("Function GetMO_DoWork - Value of Parameters: Time: {0}", DateTime.Now), ex);
			}
			finally
			{
				mCurGetTime += 1;
			}
		}

		private string GetStringFromDestinationWeb(string url)
		{
			// Open a connection
			HttpWebRequest WebRequestObject = (HttpWebRequest)HttpWebRequest.Create(url);
			WebRequestObject.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)";
			WebRequestObject.Timeout = 60000;

			// Request response:
			using (WebResponse Response = WebRequestObject.GetResponse())
			{

				// Open data stream:
				Stream WebStream = Response.GetResponseStream();

				// Create reader object:
				StreamReader Reader = new StreamReader(WebStream);

				// Read the entire stream content:
				string PageContent = Reader.ReadToEnd();

				// Cleanup
				Reader.Close();
				WebStream.Close();
				Response.Close();

				return PageContent;
			}
		}

		/// <summary>
		/// Kiểm tra có bgw nào đang chạy không
		/// 16/05/2015
		/// NMH
		/// </summary>
		/// <returns></returns>
		private bool AllBackgroundWorkerNotBusy()
		{
			bool isNotBusy = true;
			Parallel.For(0, _bgws.Count, i =>
			{
				BackgroundWorker bgw = _bgws[i];
				if (bgw.IsBusy)
				{
					isNotBusy = false;
				}
			});
			return isNotBusy;

		}

		private void SendTimer_Tick(object sender, EventArgs e)
		{
			if(!bgwSendSMSMoney.IsBusy)
			{
				bgwSendSMSMoney.RunWorkerAsync();
			}
			//else
			//{
			//	this.BeginInvoke(mWriteRichTextBox, new object[] { "Bắn tiền lần trước chưa xong...", rtbSend });
			//}

			
		}

		private void bgwSendSMSMoney_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				//if(_CountSendMoneyInDay>=_MaxSendMoneyInDay)
				//{
				//	this.BeginInvoke(mWriteRichTextBox, new object[] { "Sim đã đạt giới hạn bắn tiền trong ngày. Chờ ngày hôm sau", rtbSend });
				//	return;
				//}
				SMSSendMoney sms = GetToSendMoney;
				if (sms != null)
				{
					if (!SMSProcessFactory.DeleteMessage("1,4"))
					{
						sms.SMSSendMoneyStatus = SMSSendMoneyStatus.New;
						UpdateSendStatus = sms;
						this.BeginInvoke(mWriteRichTextBox, new object[] { "Khong xoa duoc tin nhan", rtbSend });
						return;
					}

					string message = string.Format("Phone: {0}, Money: {1}, status {2}, pushstatus: {3}", sms.PhoneNumber, sms.Money, sms.SMSSendMoneyStatus, sms.SMSSendMoneyPushStatus);
					this.BeginInvoke(mWriteRichTextBox, new object[] { message, rtbSend });

					string content = "";

					string syntaxSendMoney = ConfigurationManager.AppSettings["SyntaxSendMoney"].ToString();
					syntaxSendMoney = string.Format(syntaxSendMoney, sms.PhoneNumber, sms.Money, ConfigurationManager.AppSettings["PasswordSendMoney"].ToString());
					//this.BeginInvoke(mWriteRichTextBox, new object[] { syntaxSendMoney, rtbSend });

					if (ConfigurationManager.AppSettings["SendMoneyByUSD"].ToString() == "1")
					{
						if (SMSProcessFactory.SendMoney(syntaxSendMoney, ref content, ConfigurationManager.AppSettings["TypeEndSendMoneyByUSD"].ToString()))
						{
							if (content.Contains(ConfigurationManager.AppSettings["MessageWrongTypePhone"].ToString()))
							{
								sms.SMSSendMoneyStatus = SMSSendMoneyStatus.WrongTypePhone;
							}
							else
							{
								sms.SMSSendMoneyStatus = SMSSendMoneyStatus.Success;
								//NextSendMoney.AddMinutes(int.Parse(ConfigurationManager.AppSettings["SyntaxSendMoney"].ToString()));
							}
							

						}
						else
						{
							sms.SMSSendMoneyStatus = SMSSendMoneyStatus.Fail;
						}
					}
					else if (ConfigurationManager.AppSettings["SendMoneyByUSD"].ToString() == "3")
					{
						//kiếu viettel tra sau, toàn bộ bằng usd
						if (SMSProcessFactory.SendMoneyWithViettelPayLater(sms.PhoneNumber, sms.Money, ref content))
						{
							if (content.Contains(ConfigurationManager.AppSettings["MessageWrongTypePhone"].ToString()))
							{
								sms.SMSSendMoneyStatus = SMSSendMoneyStatus.WrongTypePhone;
							}
							else
							{
								sms.SMSSendMoneyStatus = SMSSendMoneyStatus.Success;
							}
							
						}
						else
						{
							sms.SMSSendMoneyStatus = SMSSendMoneyStatus.Fail;
						}
					}
					else
					{
						syntaxSendMoney = ConfigurationManager.AppSettings["Syntax2SendMoney"].ToString();
						syntaxSendMoney = string.Format(syntaxSendMoney, sms.PhoneNumber, sms.Money, ConfigurationManager.AppSettings["PasswordSendMoney"].ToString());

						if (SMSProcessFactory.SendMoneyWithoutUSD(syntaxSendMoney, ConfigurationManager.AppSettings["PhoneSyntax2SendMoney"].ToString(), ref content))
						{
							if (content.Contains(ConfigurationManager.AppSettings["MessageWrongTypePhone"].ToString()))
							{
								sms.SMSSendMoneyStatus = SMSSendMoneyStatus.WrongTypePhone;
							}
							else
							{
								sms.SMSSendMoneyStatus = SMSSendMoneyStatus.Success;
							}

						}
						else
						{
							sms.SMSSendMoneyStatus = SMSSendMoneyStatus.Fail;
						}
					}

					sms.Remark = content;

					this.BeginInvoke(mWriteLabel, new object[] { string.Format("{0} / {1}", GetTotalSendSuccess, _ListSendMoney.Count()), lblProgressSendMoney });

					UpdateSendStatus = sms;
					message = string.Format("Phone: {0}, Money: {1}, status {2}, pushstatus: {3}, remark: {4}", sms.PhoneNumber, sms.Money, sms.SMSSendMoneyStatus, sms.SMSSendMoneyPushStatus, sms.Remark);

					if (sms.SMSSendMoneyStatus == SMSSendMoneyStatus.Success || sms.SMSSendMoneyStatus == SMSSendMoneyStatus.WrongTypePhone)
					{

						string filename = string.Format("{1}\\{0}.txt", DateTime.Now.ToString("yyyyMMdd"), _PathLog);
						using (StreamWriter write = new StreamWriter(filename, true))
						{
							write.WriteLine(string.Format("\n{0},{1},{2},{3},{4}", ConfigurationManager.AppSettings["ModemAdaptorName"].ToString(), DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), sms.PhoneNumber, sms.Money, sms.SMSSendMoneyStatus.ToString()));
						}
					}

					this.BeginInvoke(mWriteRichTextBox, new object[] { message, rtbSend });

					Task.Factory.StartNew(() =>
					{
						UpdateStatusSMSSendMoney(sms);
					});
				}
				else
				{
					//if (GetTotalSendFail > 0)
					//{
					//	//Reset lại status để gửi lại
					//	this.BeginInvoke(mWriteRichTextBox, new object[] { "Có tin fail nên sẽ gửi lại", rtbSend });
					//	(from s in _ListSendMoney where s.SMSSendMoneyStatus == SMSSendMoneyStatus.Fail select s).ToList().ForEach(s => s.SMSSendMoneyStatus = SMSSendMoneyStatus.New);
					//}
					//else
					//{
						//this.BeginInvoke(mWriteRichTextBox, new object[] { "Tất cả đã gửi success", rtbSend });
					//}
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Chưa có danh sách số điện thoại", rtbSend });
				}
			}
			catch(Exception ex)
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "Error: "+ex.Message, rtbSend });
			}
		}

		private void UpdateStatusSMSSendMoney(SMSSendMoney sms)
		{
			try
			{
				if (sms.SMSSendMoneyStatus == SMSSendMoneyStatus.Success || sms.SMSSendMoneyStatus == SMSSendMoneyStatus.WrongTypePhone || sms.SMSSendMoneyStatus == SMSSendMoneyStatus.Fail)
				{
					Thread.Sleep(10);
					string guid = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
					sms.ModemSent = ConfigurationManager.AppSettings["ModemAdaptorName"].ToString();
					sms.GUID = guid;
					if (_smsSendMoneyRespository.InsertWithModem(sms))
					{
						sms.SMSSendMoneyPushStatus = SMSSendMoneyPushStatus.Success;

						if (sms.SMSSendMoneyStatus == SMSSendMoneyStatus.Success)
						{
							if (ConfigurationManager.AppSettings["AllowSendWarning"].ToString() == "1")
							{
								string messageWarning = string.Format(ConfigurationManager.AppSettings["MessageWarning"].ToString(), sms.PhoneNumber, sms.Money);

								//Bắn tiền thành công thì gửi tin nhắn = 2ways
								ServiceReference1.ServiceSoapClient service = new ServiceReference1.ServiceSoapClient();
								var resultsendsmswarning = service.SendSMS(ConfigurationManager.AppSettings["ClientNoWarning"].ToString(), ConfigurationManager.AppSettings["PasswordWarning"].ToString(),
									sms.PhoneNumber
									//"0904426381"
									, messageWarning,
									sms.GUID, 
									int.Parse(ConfigurationManager.AppSettings["ServiceTypeIdWarning"].ToString()));

								this.BeginInvoke(mWriteRichTextBox, new object[] { resultsendsmswarning, rtbSend });
							}
							else
							{
								this.BeginInvoke(mWriteRichTextBox, new object[] { "Không có chức năng gửi sms warning cho số nhận tiền", rtbSend });
							}
						}
					}
					else
					{
						sms.SMSSendMoneyPushStatus = SMSSendMoneyPushStatus.Fail;
					}
					UpdatePushStatus = sms;
					string message = string.Format("Phone: {0}, Money: {1}, status {2}, pushstatus: {3}, remark: {4}", sms.PhoneNumber, sms.Money, sms.SMSSendMoneyStatus, sms.SMSSendMoneyPushStatus, sms.Remark);
					this.BeginInvoke(mWriteRichTextBox, new object[] { message, rtbSend });
				}
				else
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Tin faile khong can update", rtbSend });
				}
			}
			catch(Exception ex)
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { ex.ToString(), rtbSend });
			}
		}


		private void SendTimer_Tick_Old(object sender, EventArgs e)
		{
			#region First process
			if (_ThisOnlyGetMO)
			{
				if (!bgwGetMO.IsBusy)
				{
					bgwGetMO.RunWorkerAsync();
				}
			}
			else
			{
				if (_countCunrrentSMSSentFail > _limitFail)
				{
					_RequiredRestartCOM = true;
				}

				if (_RequiredRestartCOM)
				{
					if (AllBackgroundWorkerNotBusy())
					{
						_bgwRestartCOM.RunWorkerAsync();
					}
					return;
				}
				///trả sau
				if (ConfigurationManager.AppSettings["TypeOfService"].ToString() == "5")
				{
					_smsCanSent = 1111;
				}
				///trả trước
				else if (ConfigurationManager.AppSettings["TypeOfService"].ToString() == "1")
				{

					//string currentStr = Invoke(mReadMyLabel, new object[] { lblAmountSMSKM }).ToString();
					//_smsCanSent = int.Parse(currentStr);


					//var task = Task.Factory.StartNew(() => { UpdateInfoSendSMS(SMSStatus.SentSuccess, 1); });

					///16/05/2015
					///NMH
					///Nếu có yêu cầu lấy số tin còn có thể gửi
					///
					if (_requestGetSMSCanSent && DateTime.Now >= _nextTimeGetSMSCanSent)
					{
						if (AllBackgroundWorkerNotBusy())
						{
							_bgwGetSMSCanSent.RunWorkerAsync();
						}
						return;
					}
				}

			#endregion End First process

				#region Chặn gửi tin
				///Xử lý chặn gửi tin nhắn
				///18/05/2015
				///NMH

				if (_smsCanSent <= Int32.Parse(ConfigurationManager.AppSettings["MinOfPromotionSMS"]))
				{
					_requestGetSMSCanSent = true;

					//return;
				}
				else
				{
					///Còn tiền
					_sentAlertMail = true;
					//_requestGetSMSCanSent = true;
					//return;
				}


				#endregion

				lock (locktime)
				{
					if (mCurSendTime < mSendTime)
					{
						// Không xử lý get tin khi số luồng hiện tại lớn hơn số luồng cho phép hoặc
						// Ứng dụng đang xử lý get tin (mContinuteGetSMS = false) và số luồng đang xử lý hiện tại > 0
						if ((mCurrentThread + mSMSPackage) > mMaxThread || (!mContinuteGetSMS && mCurrentThread > 0))
							return;
						UpdateContinuteGetSMSSynchronous(false);

						if (!bgwSendMT.IsBusy)
							bgwSendMT.RunWorkerAsync();
					}
					else if (mCurGetTime < mGetTime)
					{
						if (!bgwGetMO.IsBusy)
						{
							bgwGetMO.RunWorkerAsync();
						}
					}
					else
					{
						mCurSendTime = 0;
						mCurGetTime = 0;
					}
				}
			}
		}

		public void UpdateCurrentThreadSynchronous(int value)
		{
			lock (lockSendObject)
			{
				mCurrentThread += value;
			}
		}

		public void UpdateContinuteGetSMSSynchronous(bool value)
		{
			lock (lockGetObject)
			{
				mContinuteGetSMS = value;
			}
		}

		#endregion

		#region Event Form

		private void tsSend_Click(object sender, EventArgs e)
		{
			SendTimer_Tick(sender, e);
		}

		private void btnStartSend_Click(object sender, EventArgs e)
		{
			trSend.Enabled = !trSend.Enabled;
			btnStartSend.Text = trSend.Enabled ? "Stop" : "Start";
			UpdateSendTimer();
		}

		private void btnClearSend_Click(object sender, EventArgs e)
		{
			ClearLog(rtbSend);
			checkFirstTime = true;
		}

		public void ClearLog(RichTextBox rtb)
		{
			rtb.Clear();
			tbPass.Text = "";
		}

		private void btnSaveSend_Click(object sender, EventArgs e)
		{
			WriteLog(rtbSend, rtbSend.Name);
		}

		#endregion

		#region SendSMS UpdateStatus


		public void UpdateInfoSendSMS(SMSStatus status, int countSMSSuccess)
		{
			if (status == SMSStatus.SentSuccess)
			{
				this.BeginInvoke(mWriteLabel, new object[] { (int.Parse(lbCountNumberSuccess.Text) + 1) + "", lbCountNumberSuccess });
				this.BeginInvoke(mWriteLabel, new object[] { DateTime.Now.ToString(), lbLastSuccess });

				//Invoke(mWriteMyLabel, new object[] { countSMSSuccess, lblAmountSMSKM });
				//ghi ra file so luong tin km còn lại
				//string currentStr = Invoke(mReadMyLabel, new object[] { lblAmountSMSKM }).ToString();
				//Invoke(mWriteAmountPromotionSMS, new object[] { int.Parse(a), ConfigurationManager.AppSettings["amountPromotion"] });
				//luu lai gia tri de kiem tra
				//amountSMSPromotion = int.Parse(a);
				//int current = int.Parse(currentStr) - countSMSSuccess;
				_smsCanSent = _smsCanSent - countSMSSuccess;
				this.Invoke(mWriteRichTextBox, new object[] { "Gửi thành công " + countSMSSuccess + ", Số tin nhắn còn lại " + _smsCanSent, rtbSend });
				this.BeginInvoke(mWriteLabel, new object[] { _smsCanSent.ToString(), lblAmountSMSKM });
				_countCunrrentSMSSentFail = 0;

			}
			else
			{
				Invoke(mWriteLabel, new object[] { (int.Parse(lbCountSMSFail.Text) + 1) + "", lbCountSMSFail });
				Invoke(mWriteLabel, new object[] { DateTime.Now.ToString(), lbLastFail });
				_countCunrrentSMSSentFail += 1;
			}
		}


		public void UpdateSMSStatus(ResultSendSMS result, SMS sms, int countSMSSuccess)
		{//---> chưa xử lý hết trạng thái
			lock (lockupdateMT)
			{
				if (result.SMSStatus == SMSStatus.SentSuccess)
				{
					Invoke(mWriteLabelMoney, new object[] { sms.TelCoID == 2 ? 99 * sms.TotalSMS : 250 * sms.TotalSMS, lblCaculatorMoney });

				}
				UpdateInfoSendSMS(result.SMSStatus, countSMSSuccess);
				try
				{
					if (sms != null)
					{
						if (ViewCommand)
						{
							string mes = sms.ExtenstionProperties["CLIENTNAME"] + "-->" + sms.ExtenstionProperties["CLIENTNO"]
							+ " Phone: " + sms.PhoneNumber + " Message: " + sms.Message;
							BeginInvoke(mWriteRichTextBox, new object[] { mes, rtbSend });
						}
					}
					sms.SMSStatus = result.SMSStatus;
					if (!string.IsNullOrEmpty(result.ErrorsSMS) && result.ErrorsSMS != ModemDeviceStatus.EnableNetWork.ToString())
						BeginInvoke(mWriteRichTextBox, new object[] { "Lỗi SMS:" + result.ErrorsSMS + "\r\n", rtbSend });

					if (!string.IsNullOrEmpty(result.ErrorsModem) && result.ErrorsModem != ModemAdaptorStatus.Active.ToString())
						BeginInvoke(mWriteRichTextBox, new object[] { "Lỗi Modem Adapter:" + result.ErrorsModem, rtbSend });
					bool update = SMSProcessFactory.UpdateSMS(sms);
					BeginInvoke(mWriteRichTextBox,
								update
									? new object[]
                                          {
                                              "Cập nhật trạng thái tin nhắn thành công:" + sms.SMSStatus.ToString() +
                                              "\r\n", rtbSend
                                          }
									: new object[]
                                          {
                                              "Cập nhật trạng thái tin nhắn thất bại:" + sms.SMSStatus.ToString() + "\r\n",
                                              rtbSend
                                          });

				}
				catch (Exception ex)
				{
					logger.Error(ex.Message);
				}
				finally
				{
					Invoke(mChangeNumberOfThread, -1);
				}
			}
		}

		private void UpdateSMSStatusAgain(SMS sms)
		{
			try
			{   //đã try catch update trạng thái tin và đưa vào hàng đợi nếu thất bại
				bool update = SMSProcessFactory.UpdateSMS(sms);
				if (update)
				{
					BeginInvoke(mWriteRichTextBox, new object[] { "Cập nhật lại trạng thái tin nhắn thành công:" + sms.ID + " " + sms.SMSStatus.ToString(), rtbSend });
				}
				else
				{
					BeginInvoke(mWriteRichTextBox, new object[] { "Cập nhật lại trạng thái tin nhắn thất bại:" + sms.ID + " " + sms.SMSStatus.ToString(), rtbSend });
				}
			}
			catch (Exception)
			{
				BeginInvoke(mWriteRichTextBox, new object[] { "Cập nhật lại trạng thái tin nhắn thất bại:" + sms.ID + " " + sms.SMSStatus.ToString(), rtbSend });
			}
		}

		private void UpdateMO(ClientCommingSMSHostingData sms)
		{
			try
			{
				string result = SMSProcessFactory.UpdateMO(sms);
				if (result == "")
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Quá trình Update MO thành công.\n", rtbUpdate });
				}
				else if (result == "TurnOff")//nếu lỗi nghiêm trọng thì chặn phần lấy tin của thiết bị
				{
					mGetTime = 0;
				}
				else this.BeginInvoke(mWriteRichTextBox, new object[] { result + "\n", rtbUpdate });
			}
			catch (Exception ex)
			{   //nếu update thất bại cũng ko sao! lần sau sẽ gửi lại tiếp
				logger.Error(ex);
				logger.Debug(ex);
				this.BeginInvoke(mWriteRichTextBox, new object[] { string.Format("Quá trình Update MO xảy ra lỗi.\n{0}", ex.Message), rtbUpdate });
			}
			finally
			{
				Invoke(mChangeNumberOfThread, -1);
			}
		}

		private void UpdateMO_V2(IncommingSMS sms)
		{
			try
			{
				string result = SMSProcessFactory.UpdateMO_V2(sms, _usePushSMSToClient);
				string fmat = string.Format(
					"{0}-UpdateMO ({3}): {1}\n {2}",
					DateTime.Now.ToString(),
					result,
					sms.URLPushSMSToClient,
					sms.AmountOfPushSMSToClient
					);
				this.BeginInvoke(mWriteRichTextBox, new object[] { fmat, rtbUpdate });
				this.BeginInvoke(mWriteRichTextBox, new object[] { fmat, rtbLinkProcess });
				//if (result == "")
				//{

				//    this.BeginInvoke(mWriteRichTextBox, new object[] { "Quá trình Update MO thành công.\n", rtbUpdate });
				//}
				//else if (result == "TurnOff")//nếu lỗi nghiêm trọng thì chặn phần lấy tin của thiết bị
				//{
				//    mGetTime = 0;
				//}
				//else this.BeginInvoke(mWriteRichTextBox, new object[] { result + "\n", rtbUpdate });
				if (result.Contains("UpdateMO_V2 Error") && sms.AmoutUpdateMO == _limitFail && SMSProcessFactory.SendSMSAlert)
				{
					SendSMSAlert(mModemAdaptorName + " UpdateMO bi loi.");

				}

				if (result.Contains("<SubmitClient Fail>") && sms.AmountOfPushSMSToClient == _limitFail && SMSProcessFactory.SendSMSAlert)
				{
					//thông báo submit client fail
					SendSMSAlert(mModemAdaptorName + " co MO khong the submit client");
					sms.IsSubmit = true;
				}

				if (result.Contains("<SubmitFibo Fail>") && sms.AmountSubmitFibo == _limitFail && SMSProcessFactory.SendSMSAlert)
				{
					//thông báo submit fibo fail
					SendSMSAlert(mModemAdaptorName + " co MO khong the submit Fibo");

				}

				if (result.Contains("<Delete MO Success>"))
				{
					SMSProcessFactory.dataIncomming.Remove(sms);
					//WriteToLog
					SMSProcessFactory.WriteLog(sms, "Log\\LogMO", result);
				}
				else
				{
					if (sms.AmountDeleteMO == _limitFail && SMSProcessFactory.SendSMSAlert)
					{
						//thông báo có tin xóa không được
						SendSMSAlert(mModemAdaptorName + " co MO khong the xoa");
					}
				}
			}
			catch (Exception ex)
			{   //nếu update thất bại cũng ko sao! lần sau sẽ gửi lại tiếp
				logger.Error(ex);
				logger.Debug(ex);
				this.BeginInvoke(mWriteRichTextBox, new object[] { string.Format("Quá trình Update MO xảy ra lỗi.\n{0}", ex.ToString()), rtbUpdate });
			}
			finally
			{
				Invoke(mChangeNumberOfThread, -1);
			}
		}

		public void WrongPhoneNumber(SMS sms)
		{   //phải tương tự hàm update
			try
			{
				sms.SMSStatus = SMSStatus.ToNumberNotCorrect;
				bool update = SMSProcessFactory.UpdateSMS(sms);
				if (update)
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Cập nhật trạng thái nhắn thành công:" + sms.SMSStatus.ToString(), rtbSend });
				}
				else
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Cập nhật trạng thái tin nhắn thất bại:" + sms.SMSStatus.ToString(), rtbSend });
				}
			}
			catch (Exception ex)
			{
			}
			finally
			{
				Invoke(mChangeNumberOfThread, -1);
			}

		}

		#endregion

		private void SendSMSAlert(string message)
		{
			try
			{
				if (_AllowSendSMSAlert)
				{
					ServiceReference1.ServiceSoapClient services = new ServiceReference1.ServiceSoapClient();
					services.SendSMS(
					   ConfigurationManager.AppSettings["ClientNOSendAlert"].ToString(),
						ConfigurationManager.AppSettings["PassSendAlert"].ToString().Trim(),
						ConfigurationManager.AppSettings["PhoneNumberReciveAlert"].ToString().Trim(),
						message,
						Guid.NewGuid().ToString(),
						Convert.ToInt32(ConfigurationManager.AppSettings["ServiceTypeIDSendAlert"].ToString()));
					SMSProcessFactory.SendSMSAlert = false;
				}
			}
			catch (Exception)
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "Không thể send sms alert", rtbSend });
			}
		}

		/// <summary>
		/// Chuyển object sang xml
		/// </summary>
		/// <param name="objectToConvert"></param>
		/// <returns></returns>
		public string ConvertObjectToXmlString(object objectToConvert)
		{
			try
			{
				var ms = new MemoryStream();
				var xs = new XmlSerializer(objectToConvert.GetType());
				var xtw = new XmlTextWriter(ms, Encoding.UTF8);

				xs.Serialize(xtw, objectToConvert);
				ms = (MemoryStream)xtw.BaseStream;
				string strXML = new UTF8Encoding().GetString(ms.ToArray());
				strXML = strXML.Substring(strXML.LastIndexOf("?>") + 2);

				return strXML;
			}
			catch
			{
				return "";
			}
		}

		/// <summary>
		/// Check số phone
		/// </summary>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		public static bool CheckPhoneNumber(string phoneNumber)
		{
			try
			{
				Convert.ToInt64(phoneNumber);
				return true;
			}
			catch
			{
				return false;
			}
		}

		private void btTestManager_Click(object sender, EventArgs e)
		{
			SMSProcessFactory.ViewLogEvent -= SMSProcessFactory_ViewLogEvent;
			trSend.Enabled = false;
			btnStartSend.Text = "Start";
			var testfrom = new TestManagerFrom(bgwSendMT, bgwGetMO, SMSProcessFactory, ViewCommand);
			testfrom.ShowDialog(this);
			testfrom.Dispose();
			testfrom = null;
			DialogResult result = MessageBox.Show("Bạn có muốn Start Chương trình", "", MessageBoxButtons.OKCancel);
			if (result == DialogResult.OK)
			{
				trSend.Start();
				btnStartSend.Text = "Stop";
				UpdateSendTimer();
			}
			SMSProcessFactory.ViewLogEvent += SMSProcessFactory_ViewLogEvent;
		}

		private void tbPass_TextChanged(object sender, EventArgs e)
		{
			if (tbPass.Text == Crypto.ActionDecrypt(ConfigurationManager.AppSettings["PassAdmin"]))
			{
				ViewCommand = true;
			}
			else
			{
				ViewCommand = false;
			}
		}

		private void cbAccept_CheckedChanged(object sender, EventArgs e)
		{
			if (SMSProcessFactory != null)
			{
				SMSProcessFactory.UseTimeConfig = cbAccept.Checked;
				SMSProcessFactory.SendMailAlert = !cbAccept.Checked;
				if (tbDelay.Text != "")
					SMSProcessFactory.TimeDelay = int.Parse(tbDelay.Text);
				if (tbTotal.Text != "")
					SMSProcessFactory.TotalSMSAccept = int.Parse(tbTotal.Text);
				SMSProcessFactory.AcceptSendSMS = true;
			}
			InfoTimeConfig();
		}

		private void tbDelay_TextChanged(object sender, EventArgs e)
		{
			if (tbDelay.Text != "")
				SMSProcessFactory.TimeDelay = int.Parse(tbDelay.Text);
			InfoTimeConfig();
		}

		private void tbDelay_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
			{
				e.Handled = true;
			}
		}

		private void tbTotal_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
			{
				e.Handled = true;
			}
		}

		private void tbTotal_TextChanged(object sender, EventArgs e)
		{
			if (tbTotal.Text != "")
				SMSProcessFactory.TotalSMSAccept = int.Parse(tbTotal.Text);
			InfoTimeConfig();
		}
		public void InfoTimeConfig()
		{
			string info = "TimeConfig: TimeDelay" + SMSProcessFactory.TimeDelay + " TotalSMSAccept: " + SMSProcessFactory.TotalSMSAccept + " CurrentSMS: " + SMSProcessFactory.CurrentSMS + " AcceptSendSMS: " + SMSProcessFactory.AcceptSendSMS;
			if (this.InvokeRequired)
				this.BeginInvoke(mWriteRichTextBox, new object[] { info, rtbSend });
			else
			{
				rtbSend.Text += info + "\n";
			}
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			SendSMSAlert(mModemAdaptorName + " da bi tat.");
			SMSProcessFactory.EndConnectModemAdapter();
		}

		private void bttestGetMO_Click(object sender, EventArgs e)
		{
			//SMSProcessFactory.Login();
			//SMSProcessFactory.PrepareModemAdapter();
			//SMSProcessFactory.GetMO(1);
		}

		private void cbCheckMoney_CheckedChanged(object sender, EventArgs e)
		{
			if (SMSProcessFactory != null)
			{
				SMSProcessFactory.EnableCheckMoney = cbCheckMoney.Checked;
			}
			string info = "Enable Auto Stop Send SMS: " + cbCheckMoney.Checked.ToString();
			this.BeginInvoke(mWriteRichTextBox, new object[] { info, rtbSend });
		}

		#region NMHIEU
		///// <summary>
		///// kiểm tra thời gian, qua 1 ngày thì đang ký khuyến mãi
		///// </summary>
		///// <returns></returns>
		//private bool RequestRegisterPromotion(string fileName)
		//{
		//    bool result = false;
		//    try
		//    {
		//        string strLastTime = "";
		//        using (StreamReader r = new StreamReader(@"Template\"+fileName+".txt"))
		//        {
		//            strLastTime = r.ReadToEnd();
		//        }
		//        DateTime now = DateTime.Now;

		//        DateTime last = DateTime.ParseExact(strLastTime, @"dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

		//        //DateTime delta = last.AddDays(1);
		//        if (now>last)
		//        {
		//            result = true;
		//        }
		//    }
		//    catch { }

		//    return result;
		//}
		///// <summary>
		///// Luu ngay dang ky khuyến mãi vào file
		///// </summary>
		///// <param name="dateRegPromotion"></param>
		//private void WriteDateRegPromotion(string fileName,string dateRegPromotion)
		//{
		//    try
		//    {
		//        using (StreamWriter w = new StreamWriter(@"Template\" + fileName + ".txt"))
		//        {
		//            w.Write(dateRegPromotion);
		//        }
		//    }
		//    catch { }
		//}

		//private string ReadInbox()
		//{
		//    string result = string.Empty;
		//    result = SMSProcessFactory.sendCMGL("ALL", 5000);
		//    return result;
		//}

		//private string GetDateFromSMS(string sms, string start, string end)
		//{
		//    int index1 = sms.IndexOf(start);
		//    int indexEnd = sms.IndexOf(end, index1);
		//    int lenght = indexEnd - (index1 + start.Length);
		//    string subString = sms.Substring(index1 + start.Length, lenght);
		//    return subString;
		//}

		//private int ReadAmountPromotionSMSFromFile(string filename)
		//{
		//    int amount = 0;

		//    try
		//    {

		//        string str = "";
		//        using (StreamReader r = new StreamReader(@"Template\" + filename + ".txt"))
		//        {
		//            str = r.ReadToEnd();
		//        }
		//        int.TryParse(str, out amount);
		//    }
		//    catch { }

		//    return amount;
		//}

		//private void WriteAmountPromotionSMSFromFile(int amount, string fileName)
		//{
		//    try
		//    {
		//        using (StreamWriter w = new StreamWriter(@"Template\" + fileName + ".txt"))
		//        {
		//            w.Write(amount.ToString());
		//        }
		//    }
		//    catch { }
		//}

		//private void RegisterPromotion_DoWork(object sender, DoWorkEventArgs e)
		//{
		//    this.BeginInvoke(mWriteRichTextBox, new object[] { "\nThực hiện đăng ký khuyến mãi", rtbSend });

		//    string getsmsresult = SMSProcessFactory.PrepareModemAdapter();
		//    SMSStatus status;
		//    //đếm số lần thực hiện xóa tin nhắn , bị lỗi
		//    int countDeleteMessageError = 0;
		//    bool isStop = false;
		//    if (SMSProcessFactory.AcceptSendSMS)
		//    {
		//        if (getsmsresult == "")
		//        {
		//            this.BeginInvoke(mWriteRichTextBox, new object[] { "\nDelete all message inbox", rtbSend });
		//            do
		//            {
		//                //xóa danh sách tin nhắn
		//                if (!SMSProcessFactory.smsSender.DeleteMessage("1,4"))
		//                {
		//                    countDeleteMessageError++;
		//                    this.BeginInvoke(mWriteRichTextBox, new object[] { "Delete message fail. " + countDeleteMessageError, rtbSend });
		//                }
		//                else
		//                {
		//                    //xóa thành công thì thoát ra 
		//                    break;
		//                }
		//            }
		//            while (countDeleteMessageError <= 5);

		//            if(countDeleteMessageError > 5)
		//            {
		//                BaseSupportSMS.sendMail("reportagentjob@gmail.com", "sql@admin", "hieu.nm@fibo.vn", "Modem: " + ConfigurationManager.AppSettings["ModemAdaptorName"].ToString() + "Xóa tin nhắn lỗi 5 lần", "Modem: " + ConfigurationManager.AppSettings["ModemAdaptorName"].ToString() + "Xóa tin nhắn lỗi 5 lần");
		//                isStop = true;
		//            }

		//            if (!isStop)
		//            {
		//                //Gửi tin nhắn đăng ký
		//                this.BeginInvoke(mWriteRichTextBox, new object[] { "\nSent SMS Register Promotion", rtbSend });
		//                status = SMSProcessFactory.smsSender.SendSMS("999", "dk qs", ContentType.Text);

		//                //status = SMSStatus.SentSuccess;

		//                string message = string.Empty;

		//                if (status == SMSStatus.SentSuccess)
		//                {
		//                    Stopwatch stop = new Stopwatch();
		//                    DataTable tableSMS = new DataTable();
		//                    string smsSuccessRegMobi = ConfigurationManager.AppSettings["SMSSuccessRegMobi"];
		//                    string smsErrorRegMobi = ConfigurationManager.AppSettings["SMSErrorRegMobi"];
		//                    bool registerSuccess = false;
		//                    bool isRegistered = false;
		//                    stop.Start();
		//                    int index = 0;
		//                    do
		//                    {
		//                        this.BeginInvoke(mWriteRichTextBox, new object[] { "\nGet SMS Promotion", rtbSend });
		//                        tableSMS = SMSProcessFactory.smsSender.GetSMS();
		//                        if (tableSMS != null && tableSMS.Rows.Count > 0)
		//                        {
		//                            for (int i = 0; i < tableSMS.Rows.Count; i++)
		//                            {
		//                                this.BeginInvoke(mWriteRichTextBox, new object[] { "\n"+tableSMS.Rows[i]["Message"].ToString(), rtbSend });
		//                                if (tableSMS.Rows[i]["Message"].ToString().Contains(smsSuccessRegMobi))
		//                                {
		//                                    this.BeginInvoke(mWriteRichTextBox, new object[] { "\nFound SMS Promotion", rtbSend });
		//                                    index = i;
		//                                    registerSuccess = true;
		//                                    break;
		//                                }
		//                                else if (tableSMS.Rows[i]["Message"].ToString().Contains(smsErrorRegMobi))
		//                                {
		//                                    this.BeginInvoke(mWriteRichTextBox, new object[] { "\nFound Get SMS registered Promotion", rtbSend });
		//                                    index = i;
		//                                    isRegistered = true;
		//                                    break;
		//                                }
		//                            }
		//                            if (registerSuccess)
		//                            {
		//                                break;
		//                            }
		//                            else if (isRegistered)
		//                            {
		//                                break;
		//                            }
		//                            else
		//                            {
		//                                this.BeginInvoke(mWriteRichTextBox, new object[] { "\nNot Found SMS Promotion", rtbSend });
		//                                //nếu không tim thấy tin
		//                                //tableSMS = 40 tin thì xóa list
		//                                if (tableSMS.Rows.Count == 40)
		//                                {
		//                                    countDeleteMessageError = 0;
		//                                    do
		//                                    {
		//                                        //xóa danh sách tin nhắn
		//                                        if (!SMSProcessFactory.smsSender.DeleteMessage("1,4"))
		//                                        {
		//                                            countDeleteMessageError++;
		//                                            this.BeginInvoke(mWriteRichTextBox, new object[] { "\nDelete message fail. " + countDeleteMessageError, rtbSend });
		//                                        }
		//                                        else
		//                                        {
		//                                            //xóa thành công thì thoát ra 
		//                                            break;
		//                                        }
		//                                    }
		//                                    while (countDeleteMessageError <= 5);
		//                                    if (countDeleteMessageError > 5)
		//                                    {
		//                                        BaseSupportSMS.sendMail("reportagentjob@gmail.com", "sql@admin", "hieu.nm@fibo.vn", "Modem: " + ConfigurationManager.AppSettings["ModemAdaptorName"].ToString() + "Xóa tin nhắn lỗi 5 lần", "Modem: " + ConfigurationManager.AppSettings["ModemAdaptorName"].ToString() + "Xóa tin nhắn lỗi 5 lần");
		//                                    }
		//                                }
		//                            }
		//                        }
		//                        else
		//                        {
		//                            this.BeginInvoke(mWriteRichTextBox, new object[] { "Inbox empty", rtbSend });
		//                        }


		//                    }
		//                    while (stop.ElapsedMilliseconds <= 300000); //300000     60000
		//                    stop.Stop();

		//                    string strDate = "";

		//                    if (registerSuccess)
		//                    {
		//                        this.BeginInvoke(mWriteRichTextBox, new object[] { "\nĐang ký khuyến mãi thành công", rtbSend });
		//                        this.BeginInvoke(mWriteRichTextBox, new object[] { tableSMS.Rows[index]["Message"].ToString(), rtbSend });
		//                        string startDateMobi = ConfigurationManager.AppSettings["StartDateMobi"];
		//                        string endDateMobi = ConfigurationManager.AppSettings["EndDateMobi"];
		//                        //đang ky thanh công
		//                        //lấy thời gian sử dụng
		//                        this.BeginInvoke(mWriteRichTextBox, new object[] { "\nGet date register SMS Promotion", rtbSend });
		//                        strDate = GetDateFromSMS(tableSMS.Rows[index]["Message"].ToString(), startDateMobi, endDateMobi);
		//                        this.BeginInvoke(mWriteRichTextBox, new object[] { strDate, rtbSend });
		//                        //dang ky thanh cong thi ghi lại thời gian dang ky khuyến mãi
		//                        this.BeginInvoke(mWriteRichTextBox, new object[] { "\nSave date register SMS Promotion", rtbSend });
		//                        WriteDateRegPromotion(ConfigurationManager.AppSettings["LastTimePromotion"], strDate);
		//                        WriteAmountPromotionSMSFromFile(100, ConfigurationManager.AppSettings["amountPromotion"]);
		//                        this.Invoke(mWriteLabel, new object[] { "100", lblAmountSMSKM });
		//                        int.TryParse(Invoke(mReadMyLabel, new object[] { lblAmountSMSKM }).ToString(), out amountSMSPromotion);
		//                    }
		//                    else if (isRegistered)
		//                    {
		//                        this.BeginInvoke(mWriteRichTextBox, new object[] { tableSMS.Rows[index]["Message"].ToString(), rtbSend });
		//                    }
		//                    else
		//                    {
		//                        this.BeginInvoke(mWriteRichTextBox, new object[] { "\nĐăng ký thất bại", rtbSend });
		//                        WriteAmountPromotionSMSFromFile(0, ConfigurationManager.AppSettings["amountPromotion"]);
		//                        this.Invoke(mWriteLabel, new object[] { "0", lblAmountSMSKM });
		//                        int.TryParse(Invoke(mReadMyLabel, new object[] { lblAmountSMSKM }).ToString(), out amountSMSPromotion);
		//                        BaseSupportSMS.sendMail("reportagentjob@gmail.com", "sql@admin", "hieu.nm@fibo.vn", "Modem: " + ConfigurationManager.AppSettings["ModemAdaptorName"].ToString() + "đăng ký khuyễn mãi thất bại", "Modem: " + ConfigurationManager.AppSettings["ModemAdaptorName"].ToString() + "đăng ký khuyễn mãi thất bại");

		//                    }

		//                }
		//                else
		//                {
		//                    this.BeginInvoke(mWriteRichTextBox, new object[] { "\nSend SMS register promotion fail.", rtbSend });
		//                }
		//            }
		//            else
		//            {
		//                this.BeginInvoke(mWriteRichTextBox, new object[] { "\nDừng đăng ký khuyến mãi", rtbSend });
		//            }
		//        }
		//        else
		//        {
		//            this.BeginInvoke(mWriteRichTextBox, new object[] { "\nKhởi tạo thiết bị thất bại", rtbSend });
		//        }
		//    }
		//    else
		//    {
		//        this.BeginInvoke(mWriteRichTextBox, new object[] { "\nKhông cho phép gửi tin", rtbSend });
		//    }
		//    SMSProcessFactory.EndConnectModemAdapter();
		//}
		#endregion

		private void MainForm_Load(object sender, EventArgs e)
		{
			if (_ThisOnlyGetMO)
			{
				trSend.Interval = 1000;
				txtInterval.Text = "180000";
			}
			//string url = "";
			//        string pattern = @"^survey(\d\s\w\W)*";
			//        Regex myRegex = new Regex(pattern);

			//        if (myRegex.IsMatch("survey khaosatxyz*"))
			//        {

			//        }
			//        else
			//        {

			//        }

			//string content="";
			//string syntaxSendMoney = ConfigurationManager.AppSettings["SyntaxSendMoney"].ToString();
			//syntaxSendMoney = string.Format(syntaxSendMoney, ConfigurationManager.AppSettings["PasswordSendMoney"].ToString(), ConfigurationManager.AppSettings["PasswordSendMoney"].ToString(), "01656244909", "10000");

			//this.BeginInvoke(mWriteRichTextBox, new object[] { syntaxSendMoney, rtbSend });

			//if(SMSProcessFactory.SendMoney("01656244909","1000",ref content))
			//{
			//	this.BeginInvoke(mWriteRichTextBox, new object[] { "Ok", rtbSend });
			//}
			//else
			//{
			//	this.BeginInvoke(mWriteRichTextBox, new object[] { "No", rtbSend });
			//}

			//this.BeginInvoke(mWriteRichTextBox, new object[] { content, rtbSend });

			_ListSendMoney = new List<SMSSendMoney>();
			NextSendMoney = DateTime.Now;
			SMSProcessFactory.PrepareModemAdapter();

			_PathLog = Environment.CurrentDirectory + "\\LogBanTien";
			_PathExport = Environment.CurrentDirectory + "\\Export";

			if (!Directory.Exists(_PathLog))
			{
				Directory.CreateDirectory(_PathLog);

			}

			if (!Directory.Exists(_PathExport))
			{
				Directory.CreateDirectory(_PathExport);

			}

			//string filename = string.Format("{1}\\{0}.txt", DateTime.Now.ToString("yyyyMMdd"), _PathLog);
			//using (StreamWriter write = new StreamWriter(filename, true))
			//{
			//	write.WriteLine(string.Format("\n{0},{1},{2},{3},{4}", ConfigurationManager.AppSettings["ModemAdaptorName"].ToString(), DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), "sms.PhoneNumber", "sms.Money", "sms.SMSSendMoneyStatus.ToString()"));
			//}

			_MaxSendMoneyInDay = int.Parse(ConfigurationManager.AppSettings["Price"].ToString());
			_CountSendMoneyInDay = 0;
		}

		private void chkOnLast_CheckedChanged(object sender, EventArgs e)
		{

		}

		private void rtbSend_TextChanged(object sender, EventArgs e)
		{
			RichTextBox rtb = sender as RichTextBox;
			if (chkOnLast.Checked)
			{
				rtb.SelectionStart = rtb.Text.Length;
				rtb.ScrollToCaret();
			}
		}

		private void rtbUpdate_TextChanged(object sender, EventArgs e)
		{
			rtbSend_TextChanged(sender, e);
		}

		private void rtbLinkProcess_TextChanged(object sender, EventArgs e)
		{
			rtbSend_TextChanged(sender, e);
		}

		private void test()
		{
			List<string> patterns = new List<string>();
			List<string> match = new List<string>();
			patterns.Add("TKC:{0}d");

			match = SubStringByPattern(
				"AT+CUSD=1,\"*101#\" OK+CUSD: 2,\"MobiQ TKC:50005d,20/06/2015 KM1T:7498d,05/06/2015\",15",
				patterns
				);
		}

		private List<string> SubStringByPattern(string source, List<string> patterns)
		{
			List<string> str = new List<string>();

			Parallel.For(0, patterns.Count, i =>
			{
				//for (int i = 0; i < patterns.Count; i++)
				//{
				MatchCollection mc;
				string pattern = patterns[i].Replace("{0}", @"\d+");
				Regex regex = new Regex(pattern);
				mc = regex.Matches(source);
				if (mc.Count > 0)
				{
					foreach (Match item in mc)
					{
						str.Add(item.Value);
					}
				}
				//}
			});

			List<int> moneys = GetMoneyByPattern(str);
			return str;
		}

		private List<int> GetMoneyByPattern(List<string> list)
		{
			List<int> moneys = new List<int>();
			Parallel.For(0, list.Count, i =>
			{
				try
				{
					string pattern = @"\D";
					Regex regex = new Regex(pattern);
					string money = regex.Replace(list[i], "");
					moneys.Add(Convert.ToInt32(money));
				}
				catch
				{

				}
			});
			return moneys;
		}

		private void toolStripButtonGetAmoutSMS_Click(object sender, EventArgs e)
		{
			_requestGetSMSCanSent = true;
			_nextTimeGetSMSCanSent = DateTime.Now;
		}

		private void toolStripButtonRestartCOM_Click(object sender, EventArgs e)
		{
			_RequiredRestartCOM = true;
		}

		private void RegisterPromotion_DoWork(object sender, DoWorkEventArgs e)
		{

		}

		private void btnImportSendMoney_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			List<SMSSendMoney> list = new List<SMSSendMoney>();
			// Set filter options and filter index.
			dialog.Filter = "xlsx Files (.txt)|*.txt";
			dialog.FilterIndex = 1;

			dialog.Multiselect = false;

			// Call the ShowDialog method to show the dialog box.
			DialogResult result = dialog.ShowDialog();

			string filepath = "";
			// Process input if the user clicked OK.
			if (result == DialogResult.OK)
			{
				filepath = dialog.FileName;
			}

			string[] values=File.ReadAllLines(filepath);
			if(values!=null)
			{
				foreach (string item in values)
				{
					string[] row = item.Split('\t');
					int moneyInt=0;
					if (!string.IsNullOrEmpty(row[0]) && !string.IsNullOrEmpty(row[1]) && Int32.TryParse(row[1], out moneyInt))
					{

						SMSSendMoney sms = new SMSSendMoney() { PhoneNumber = row[0], Money = row[1] };
						list.Add(sms);
					}
					
				}
			}

			dgvSendMoney.DataSource = null;
			dgvSendMoney.DataSource = list;

			lblProgressSendMoney.Text = string.Format("{0} / {1}", 0, list.Count);
			_ListSendMoney = list;
		}

		private void btnImportSendMoney_Click2(object sender, EventArgs e)
		{
			
			OpenFileDialog dialog = new OpenFileDialog();

			// Set filter options and filter index.
			dialog.Filter = "xlsx Files (.xlsx)|*.xlsx";
			dialog.FilterIndex = 1;

			dialog.Multiselect = false;

			// Call the ShowDialog method to show the dialog box.
			DialogResult result = dialog.ShowDialog();

			string filepath = "";
			// Process input if the user clicked OK.
			if (result == DialogResult.OK)
			{
				filepath = dialog.FileName;

			}

			try
			{
				List<SMSSendMoney> list = new List<SMSSendMoney>();
				XSSFWorkbook xssfWorkbook;
				using (FileStream file = new FileStream(filepath, FileMode.Open, FileAccess.Read))
				{
					//xssfWorkbook = new XSSFWorkbook(file);
					//var docs= new POIFSFileSystem(file);
					xssfWorkbook = new XSSFWorkbook(file);
				}

				ISheet sheet = xssfWorkbook.GetSheetAt(0);
				System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
				IRow row = null;
				ICell cellPhone;
				ICell cellTelCo;
				ICell cellMoney;
				string phone;
				string telco;
				string money;
				while (rows.MoveNext())
				{
					row = (XSSFRow)rows.Current;
					if (row != null)
					{
						cellTelCo = row.GetCell(0);
						cellPhone = row.GetCell(1);
						cellMoney = row.GetCell(2);
						Int32 moneyInt;
						telco = cellTelCo != null ? cellTelCo.StringCellValue.TrimEnd().TrimStart() : string.Empty;
						phone = cellPhone != null ? cellPhone.StringCellValue.TrimEnd().TrimStart() : string.Empty;
						if (cellMoney.CellType == CellType.String)
						{
							money = cellMoney != null ? cellMoney.StringCellValue.TrimEnd().TrimStart() : string.Empty;
						}
						else
						{
							money = cellMoney != null ? cellMoney.NumericCellValue.ToString() : string.Empty;
						}

						if (!string.IsNullOrEmpty(phone) && !string.IsNullOrEmpty(money) && Int32.TryParse(money, out moneyInt))
						{
							list.Add(new SMSSendMoney() { TelCo = telco,PhoneNumber=phone,Money=money });
						}
					}
				}

				dgvSendMoney.DataSource = null;
				dgvSendMoney.DataSource = list;

				lblProgressSendMoney.Text = string.Format("{0} / {1}", 0, list.Count);
				_ListSendMoney = list;
			}
			catch(Exception ex)
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { ex.ToString(), rtbSend });
			}
	
		}

		private void btnSendMoneyExport_Click(object sender, EventArgs e)
		{
			try
			{
				if (_ListSendMoney != null)
				{
					string filename = string.Format("{1}\\{0}.txt", DateTime.Now.ToString("yyyyMMdd_HHmmss"), _PathExport);

					using (StreamWriter write = new StreamWriter(filename))
					{
						foreach (var sms in _ListSendMoney)
						{
							write.WriteLine(string.Format("\n{0},{1},{2},{3},{4},{5}", ConfigurationManager.AppSettings["ModemAdaptorName"].ToString(), DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), sms.PhoneNumber, sms.Money, sms.SMSSendMoneyStatus.ToString(), sms.Remark));
						}
					}

					OpenFileInExplorer(filename);
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Export loi: " + ex.Message);
			}
		}

		public void OpenFileInExplorer(string fullpath)
		{
			string windir = Environment.GetEnvironmentVariable("windir");
			if (string.IsNullOrEmpty(windir.Trim()))
			{
				windir = "C:\\Windows\\";
			}
			if (!windir.EndsWith("\\"))
			{
				windir += "\\";
			}

			FileInfo fileToLocate = null;
			fileToLocate = new FileInfo(fullpath);

			ProcessStartInfo pi = new ProcessStartInfo(windir + "explorer.exe");
			pi.Arguments = "/select, \"" + fileToLocate.FullName + "\"";
			pi.WindowStyle = ProcessWindowStyle.Normal;
			pi.WorkingDirectory = windir;

			//Start Process
			Process.Start(pi);
		}



		public string _PathLog { get; set; }

		public string _PathExport { get; set; }

		public int _MaxSendMoneyInDay { get; set; }

		public int _CountSendMoneyInDay { get; set; }

		private void btnSendAgainSMSFail_Click(object sender, EventArgs e)
		{
			if (GetTotalSendFail > 0)
			{
				//Reset lại status để gửi lại
				this.BeginInvoke(mWriteRichTextBox, new object[] { "Có tin fail nên sẽ gửi lại", rtbSend });
				(from s in _ListSendMoney where s.SMSSendMoneyStatus == SMSSendMoneyStatus.Fail select s).ToList().ForEach(s => s.SMSSendMoneyStatus = SMSSendMoneyStatus.New);
			}
			else
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "Tất cả đã gửi success", rtbSend });
			}
		}
	}
}
