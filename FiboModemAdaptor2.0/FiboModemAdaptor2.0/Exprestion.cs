﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModemAdaptorMutilThreadV2
{
	public static class Extension
	{
		public static void Update<T>(this T item, Action<T> updateAction)
		{
			updateAction(item);
		}
	}
}
