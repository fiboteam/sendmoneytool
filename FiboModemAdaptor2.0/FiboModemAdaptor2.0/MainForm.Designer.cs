﻿namespace ModemAdaptorMutilThreadV2
{
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.tsSend = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.bttestGetMO = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.btTestManager = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButtonGetAmoutSMS = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButtonRestartCOM = new System.Windows.Forms.ToolStripButton();
			this.rtbUpdate = new System.Windows.Forms.RichTextBox();
			this.gbConfig = new System.Windows.Forms.GroupBox();
			this.tbTimeOut = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.tbPass = new System.Windows.Forms.TextBox();
			this.cbMultiThread = new System.Windows.Forms.CheckBox();
			this.txtLengOfFile = new System.Windows.Forms.TextBox();
			this.txtLogFilePath = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtInterval = new System.Windows.Forms.TextBox();
			this.txtNumberOfThread = new System.Windows.Forms.TextBox();
			this.btnSaveConfig = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.gbInformationSystem = new System.Windows.Forms.GroupBox();
			this.lblAmountSMSKM = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.lblCaculatorMoney = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.lblCurrentMoney = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cbCheckMoney = new System.Windows.Forms.CheckBox();
			this.cbAccept = new System.Windows.Forms.CheckBox();
			this.lbtin = new System.Windows.Forms.Label();
			this.lbsecond = new System.Windows.Forms.Label();
			this.tbTotal = new System.Windows.Forms.TextBox();
			this.lbTotal = new System.Windows.Forms.Label();
			this.tbDelay = new System.Windows.Forms.TextBox();
			this.lbDelay = new System.Windows.Forms.Label();
			this.lbLastFail = new System.Windows.Forms.Label();
			this.lbCountSMSFail = new System.Windows.Forms.Label();
			this.lbNumberFail = new System.Windows.Forms.Label();
			this.lbLastSuccess = new System.Windows.Forms.Label();
			this.lbCountNumberSuccess = new System.Windows.Forms.Label();
			this.lbNumberSMS = new System.Windows.Forms.Label();
			this.lblListServiceType = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.lblTypeOfModem = new System.Windows.Forms.Label();
			this.lblTypeOfModemTitle = new System.Windows.Forms.Label();
			this.lblCurrentServiceType = new System.Windows.Forms.Label();
			this.lblServiceTypeIDTitle = new System.Windows.Forms.Label();
			this.lblModemName = new System.Windows.Forms.Label();
			this.lblModemNameTitle = new System.Windows.Forms.Label();
			this.lblModemPartner = new System.Windows.Forms.Label();
			this.lblModemPartnerTitle = new System.Windows.Forms.Label();
			this.lbTotalThread = new System.Windows.Forms.Label();
			this.lblTotalThreadTitle = new System.Windows.Forms.Label();
			this.lblUpdateTimerStatus = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.lblSendTimerStatus = new System.Windows.Forms.Label();
			this.lblSendTimerTitle = new System.Windows.Forms.Label();
			this.trSend = new System.Windows.Forms.Timer(this.components);
			this.tabControl = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.rtbSend = new System.Windows.Forms.RichTextBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.pageLinkProcess = new System.Windows.Forms.TabPage();
			this.rtbLinkProcess = new System.Windows.Forms.RichTextBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.btnSendAgainSMSFail = new System.Windows.Forms.Button();
			this.btnSendMoneyExport = new System.Windows.Forms.Button();
			this.btnImportSendMoney = new System.Windows.Forms.Button();
			this.lblProgressSendMoney = new System.Windows.Forms.Label();
			this.dgvSendMoney = new System.Windows.Forms.DataGridView();
			this.btnSaveSend = new System.Windows.Forms.Button();
			this.btnClearSend = new System.Windows.Forms.Button();
			this.btnStartSend = new System.Windows.Forms.Button();
			this.bgwSendMT = new System.ComponentModel.BackgroundWorker();
			this.bgwGetMO = new System.ComponentModel.BackgroundWorker();
			this.RegisterPromotion = new System.ComponentModel.BackgroundWorker();
			this.chkOnLast = new System.Windows.Forms.CheckBox();
			this.bgwSendSMSMoney = new System.ComponentModel.BackgroundWorker();
			this.statusStrip1.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.gbConfig.SuspendLayout();
			this.gbInformationSystem.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tabControl.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.pageLinkProcess.SuspendLayout();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvSendMoney)).BeginInit();
			this.SuspendLayout();
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
			this.statusStrip1.Location = new System.Drawing.Point(0, 636);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(758, 22);
			this.statusStrip1.TabIndex = 1;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(50, 17);
			this.toolStripStatusLabel1.Text = "Bắn tiền";
			// 
			// toolStripStatusLabel2
			// 
			this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
			this.toolStripStatusLabel2.Size = new System.Drawing.Size(40, 17);
			this.toolStripStatusLabel2.Text = "v.2.5.1";
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSend,
            this.toolStripSeparator1,
            this.bttestGetMO,
            this.toolStripSeparator2,
            this.btTestManager,
            this.toolStripSeparator3,
            this.toolStripButtonGetAmoutSMS,
            this.toolStripSeparator4,
            this.toolStripButtonRestartCOM});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(758, 25);
			this.toolStrip1.TabIndex = 2;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// tsSend
			// 
			this.tsSend.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.tsSend.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsSend.Name = "tsSend";
			this.tsSend.Size = new System.Drawing.Size(37, 22);
			this.tsSend.Text = "Send";
			this.tsSend.Click += new System.EventHandler(this.tsSend_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// bttestGetMO
			// 
			this.bttestGetMO.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.bttestGetMO.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.bttestGetMO.Name = "bttestGetMO";
			this.bttestGetMO.Size = new System.Drawing.Size(49, 22);
			this.bttestGetMO.Text = "GetMO";
			this.bttestGetMO.Visible = false;
			this.bttestGetMO.Click += new System.EventHandler(this.bttestGetMO_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// btTestManager
			// 
			this.btTestManager.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btTestManager.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btTestManager.Name = "btTestManager";
			this.btTestManager.Size = new System.Drawing.Size(79, 22);
			this.btTestManager.Text = "TestManager";
			this.btTestManager.Click += new System.EventHandler(this.btTestManager_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripButtonGetAmoutSMS
			// 
			this.toolStripButtonGetAmoutSMS.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripButtonGetAmoutSMS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonGetAmoutSMS.Image")));
			this.toolStripButtonGetAmoutSMS.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonGetAmoutSMS.Name = "toolStripButtonGetAmoutSMS";
			this.toolStripButtonGetAmoutSMS.Size = new System.Drawing.Size(96, 22);
			this.toolStripButtonGetAmoutSMS.Text = "GetAmountSMS";
			this.toolStripButtonGetAmoutSMS.Visible = false;
			this.toolStripButtonGetAmoutSMS.Click += new System.EventHandler(this.toolStripButtonGetAmoutSMS_Click);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripButtonRestartCOM
			// 
			this.toolStripButtonRestartCOM.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripButtonRestartCOM.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRestartCOM.Image")));
			this.toolStripButtonRestartCOM.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonRestartCOM.Name = "toolStripButtonRestartCOM";
			this.toolStripButtonRestartCOM.Size = new System.Drawing.Size(75, 22);
			this.toolStripButtonRestartCOM.Text = "RestartCOM";
			this.toolStripButtonRestartCOM.Visible = false;
			this.toolStripButtonRestartCOM.Click += new System.EventHandler(this.toolStripButtonRestartCOM_Click);
			// 
			// rtbUpdate
			// 
			this.rtbUpdate.Location = new System.Drawing.Point(6, 6);
			this.rtbUpdate.Name = "rtbUpdate";
			this.rtbUpdate.Size = new System.Drawing.Size(466, 365);
			this.rtbUpdate.TabIndex = 3;
			this.rtbUpdate.Text = "";
			this.rtbUpdate.TextChanged += new System.EventHandler(this.rtbUpdate_TextChanged);
			// 
			// gbConfig
			// 
			this.gbConfig.Controls.Add(this.tbTimeOut);
			this.gbConfig.Controls.Add(this.label9);
			this.gbConfig.Controls.Add(this.tbPass);
			this.gbConfig.Controls.Add(this.cbMultiThread);
			this.gbConfig.Controls.Add(this.txtLengOfFile);
			this.gbConfig.Controls.Add(this.txtLogFilePath);
			this.gbConfig.Controls.Add(this.label5);
			this.gbConfig.Controls.Add(this.txtInterval);
			this.gbConfig.Controls.Add(this.txtNumberOfThread);
			this.gbConfig.Controls.Add(this.btnSaveConfig);
			this.gbConfig.Controls.Add(this.label4);
			this.gbConfig.Controls.Add(this.label2);
			this.gbConfig.Controls.Add(this.label3);
			this.gbConfig.Location = new System.Drawing.Point(12, 478);
			this.gbConfig.Name = "gbConfig";
			this.gbConfig.Size = new System.Drawing.Size(486, 100);
			this.gbConfig.TabIndex = 4;
			this.gbConfig.TabStop = false;
			this.gbConfig.Text = "Configuration";
			// 
			// tbTimeOut
			// 
			this.tbTimeOut.Location = new System.Drawing.Point(248, 70);
			this.tbTimeOut.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tbTimeOut.Name = "tbTimeOut";
			this.tbTimeOut.Size = new System.Drawing.Size(80, 20);
			this.tbTimeOut.TabIndex = 34;
			this.tbTimeOut.Text = "30000";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(150, 73);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(102, 13);
			this.label9.TabIndex = 33;
			this.label9.Text = "Timeout Read inbox";
			// 
			// tbPass
			// 
			this.tbPass.Location = new System.Drawing.Point(361, 74);
			this.tbPass.Name = "tbPass";
			this.tbPass.Size = new System.Drawing.Size(120, 20);
			this.tbPass.TabIndex = 32;
			this.tbPass.UseSystemPasswordChar = true;
			this.tbPass.TextChanged += new System.EventHandler(this.tbPass_TextChanged);
			// 
			// cbMultiThread
			// 
			this.cbMultiThread.AutoSize = true;
			this.cbMultiThread.Location = new System.Drawing.Point(11, 70);
			this.cbMultiThread.Name = "cbMultiThread";
			this.cbMultiThread.Size = new System.Drawing.Size(82, 17);
			this.cbMultiThread.TabIndex = 31;
			this.cbMultiThread.Text = "MultiThread";
			this.cbMultiThread.UseVisualStyleBackColor = true;
			// 
			// txtLengOfFile
			// 
			this.txtLengOfFile.Location = new System.Drawing.Point(248, 42);
			this.txtLengOfFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtLengOfFile.Name = "txtLengOfFile";
			this.txtLengOfFile.Size = new System.Drawing.Size(80, 20);
			this.txtLengOfFile.TabIndex = 27;
			this.txtLengOfFile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeyPress_Number);
			// 
			// txtLogFilePath
			// 
			this.txtLogFilePath.Location = new System.Drawing.Point(64, 42);
			this.txtLogFilePath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtLogFilePath.Name = "txtLogFilePath";
			this.txtLogFilePath.Size = new System.Drawing.Size(80, 20);
			this.txtLogFilePath.TabIndex = 29;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 45);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(50, 13);
			this.label5.TabIndex = 28;
			this.label5.Text = "LogPath:";
			// 
			// txtInterval
			// 
			this.txtInterval.Location = new System.Drawing.Point(64, 14);
			this.txtInterval.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtInterval.Name = "txtInterval";
			this.txtInterval.Size = new System.Drawing.Size(80, 20);
			this.txtInterval.TabIndex = 23;
			this.txtInterval.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeyPress_Number);
			// 
			// txtNumberOfThread
			// 
			this.txtNumberOfThread.Location = new System.Drawing.Point(248, 14);
			this.txtNumberOfThread.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtNumberOfThread.Name = "txtNumberOfThread";
			this.txtNumberOfThread.Size = new System.Drawing.Size(80, 20);
			this.txtNumberOfThread.TabIndex = 24;
			this.txtNumberOfThread.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeyPress_Number);
			// 
			// btnSaveConfig
			// 
			this.btnSaveConfig.Location = new System.Drawing.Point(398, 12);
			this.btnSaveConfig.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btnSaveConfig.Name = "btnSaveConfig";
			this.btnSaveConfig.Size = new System.Drawing.Size(82, 22);
			this.btnSaveConfig.TabIndex = 25;
			this.btnSaveConfig.Text = "Save Config";
			this.btnSaveConfig.UseVisualStyleBackColor = true;
			this.btnSaveConfig.Click += new System.EventHandler(this.btnSaveConfig_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(150, 45);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(70, 13);
			this.label4.TabIndex = 26;
			this.label4.Text = "LengthOfFile:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(48, 13);
			this.label2.TabIndex = 21;
			this.label2.Text = "Interval :";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(150, 17);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(92, 13);
			this.label3.TabIndex = 22;
			this.label3.Text = "NumberOfThread:";
			// 
			// gbInformationSystem
			// 
			this.gbInformationSystem.Controls.Add(this.lblAmountSMSKM);
			this.gbInformationSystem.Controls.Add(this.label7);
			this.gbInformationSystem.Controls.Add(this.lblCaculatorMoney);
			this.gbInformationSystem.Controls.Add(this.label12);
			this.gbInformationSystem.Controls.Add(this.lblCurrentMoney);
			this.gbInformationSystem.Controls.Add(this.label8);
			this.gbInformationSystem.Controls.Add(this.groupBox1);
			this.gbInformationSystem.Controls.Add(this.lbLastFail);
			this.gbInformationSystem.Controls.Add(this.lbCountSMSFail);
			this.gbInformationSystem.Controls.Add(this.lbNumberFail);
			this.gbInformationSystem.Controls.Add(this.lbLastSuccess);
			this.gbInformationSystem.Controls.Add(this.lbCountNumberSuccess);
			this.gbInformationSystem.Controls.Add(this.lbNumberSMS);
			this.gbInformationSystem.Controls.Add(this.lblListServiceType);
			this.gbInformationSystem.Controls.Add(this.label6);
			this.gbInformationSystem.Controls.Add(this.lblTypeOfModem);
			this.gbInformationSystem.Controls.Add(this.lblTypeOfModemTitle);
			this.gbInformationSystem.Controls.Add(this.lblCurrentServiceType);
			this.gbInformationSystem.Controls.Add(this.lblServiceTypeIDTitle);
			this.gbInformationSystem.Controls.Add(this.lblModemName);
			this.gbInformationSystem.Controls.Add(this.lblModemNameTitle);
			this.gbInformationSystem.Controls.Add(this.lblModemPartner);
			this.gbInformationSystem.Controls.Add(this.lblModemPartnerTitle);
			this.gbInformationSystem.Controls.Add(this.lbTotalThread);
			this.gbInformationSystem.Controls.Add(this.lblTotalThreadTitle);
			this.gbInformationSystem.Controls.Add(this.lblUpdateTimerStatus);
			this.gbInformationSystem.Controls.Add(this.label1);
			this.gbInformationSystem.Controls.Add(this.lblSendTimerStatus);
			this.gbInformationSystem.Controls.Add(this.lblSendTimerTitle);
			this.gbInformationSystem.Location = new System.Drawing.Point(504, 29);
			this.gbInformationSystem.Name = "gbInformationSystem";
			this.gbInformationSystem.Size = new System.Drawing.Size(254, 604);
			this.gbInformationSystem.TabIndex = 6;
			this.gbInformationSystem.TabStop = false;
			this.gbInformationSystem.Text = "Information";
			// 
			// lblAmountSMSKM
			// 
			this.lblAmountSMSKM.AutoSize = true;
			this.lblAmountSMSKM.Location = new System.Drawing.Point(131, 237);
			this.lblAmountSMSKM.Name = "lblAmountSMSKM";
			this.lblAmountSMSKM.Size = new System.Drawing.Size(13, 13);
			this.lblAmountSMSKM.TabIndex = 30;
			this.lblAmountSMSKM.Text = "0";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 237);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(118, 13);
			this.label7.TabIndex = 29;
			this.label7.Text = "Số tin nhắn khuyến mãi";
			// 
			// lblCaculatorMoney
			// 
			this.lblCaculatorMoney.AutoSize = true;
			this.lblCaculatorMoney.Location = new System.Drawing.Point(97, 216);
			this.lblCaculatorMoney.Name = "lblCaculatorMoney";
			this.lblCaculatorMoney.Size = new System.Drawing.Size(13, 13);
			this.lblCaculatorMoney.TabIndex = 28;
			this.lblCaculatorMoney.Text = "0";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(6, 216);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(83, 13);
			this.label12.TabIndex = 27;
			this.label12.Text = "Số tiền ước tính";
			// 
			// lblCurrentMoney
			// 
			this.lblCurrentMoney.AutoSize = true;
			this.lblCurrentMoney.Location = new System.Drawing.Point(97, 194);
			this.lblCurrentMoney.Name = "lblCurrentMoney";
			this.lblCurrentMoney.Size = new System.Drawing.Size(13, 13);
			this.lblCurrentMoney.TabIndex = 24;
			this.lblCurrentMoney.Text = "0";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(6, 194);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(77, 13);
			this.label8.TabIndex = 23;
			this.label8.Text = "Số tiền hiện tại";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cbCheckMoney);
			this.groupBox1.Controls.Add(this.cbAccept);
			this.groupBox1.Controls.Add(this.lbtin);
			this.groupBox1.Controls.Add(this.lbsecond);
			this.groupBox1.Controls.Add(this.tbTotal);
			this.groupBox1.Controls.Add(this.lbTotal);
			this.groupBox1.Controls.Add(this.tbDelay);
			this.groupBox1.Controls.Add(this.lbDelay);
			this.groupBox1.Location = new System.Drawing.Point(6, 317);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(242, 152);
			this.groupBox1.TabIndex = 22;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Time Config";
			// 
			// cbCheckMoney
			// 
			this.cbCheckMoney.AutoSize = true;
			this.cbCheckMoney.Location = new System.Drawing.Point(9, 52);
			this.cbCheckMoney.Name = "cbCheckMoney";
			this.cbCheckMoney.Size = new System.Drawing.Size(143, 17);
			this.cbCheckMoney.TabIndex = 7;
			this.cbCheckMoney.Text = "Không gửi tin khi hết tiền";
			this.cbCheckMoney.UseVisualStyleBackColor = true;
			this.cbCheckMoney.CheckedChanged += new System.EventHandler(this.cbCheckMoney_CheckedChanged);
			// 
			// cbAccept
			// 
			this.cbAccept.AutoSize = true;
			this.cbAccept.Location = new System.Drawing.Point(9, 75);
			this.cbAccept.Name = "cbAccept";
			this.cbAccept.Size = new System.Drawing.Size(153, 17);
			this.cbAccept.TabIndex = 6;
			this.cbAccept.Text = "Sử dụng cấu hình thời gian";
			this.cbAccept.UseVisualStyleBackColor = true;
			this.cbAccept.CheckedChanged += new System.EventHandler(this.cbAccept_CheckedChanged);
			// 
			// lbtin
			// 
			this.lbtin.AutoSize = true;
			this.lbtin.Location = new System.Drawing.Point(182, 126);
			this.lbtin.Name = "lbtin";
			this.lbtin.Size = new System.Drawing.Size(22, 13);
			this.lbtin.TabIndex = 5;
			this.lbtin.Text = "Tin";
			// 
			// lbsecond
			// 
			this.lbsecond.AutoSize = true;
			this.lbsecond.Location = new System.Drawing.Point(182, 99);
			this.lbsecond.Name = "lbsecond";
			this.lbsecond.Size = new System.Drawing.Size(28, 13);
			this.lbsecond.TabIndex = 4;
			this.lbsecond.Text = "Giây";
			// 
			// tbTotal
			// 
			this.tbTotal.Location = new System.Drawing.Point(71, 123);
			this.tbTotal.Name = "tbTotal";
			this.tbTotal.Size = new System.Drawing.Size(105, 20);
			this.tbTotal.TabIndex = 3;
			this.tbTotal.Text = "4000";
			this.tbTotal.TextChanged += new System.EventHandler(this.tbTotal_TextChanged);
			this.tbTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTotal_KeyPress);
			// 
			// lbTotal
			// 
			this.lbTotal.AutoSize = true;
			this.lbTotal.Location = new System.Drawing.Point(6, 126);
			this.lbTotal.Name = "lbTotal";
			this.lbTotal.Size = new System.Drawing.Size(60, 13);
			this.lbTotal.TabIndex = 2;
			this.lbTotal.Text = "Total SMS:";
			// 
			// tbDelay
			// 
			this.tbDelay.Location = new System.Drawing.Point(71, 96);
			this.tbDelay.Name = "tbDelay";
			this.tbDelay.Size = new System.Drawing.Size(105, 20);
			this.tbDelay.TabIndex = 1;
			this.tbDelay.Text = "15";
			this.tbDelay.TextChanged += new System.EventHandler(this.tbDelay_TextChanged);
			this.tbDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDelay_KeyPress);
			// 
			// lbDelay
			// 
			this.lbDelay.AutoSize = true;
			this.lbDelay.Location = new System.Drawing.Point(6, 99);
			this.lbDelay.Name = "lbDelay";
			this.lbDelay.Size = new System.Drawing.Size(65, 13);
			this.lbDelay.TabIndex = 0;
			this.lbDelay.Text = "Delay Send:";
			// 
			// lbLastFail
			// 
			this.lbLastFail.AutoSize = true;
			this.lbLastFail.Location = new System.Drawing.Point(6, 169);
			this.lbLastFail.Name = "lbLastFail";
			this.lbLastFail.Size = new System.Drawing.Size(49, 13);
			this.lbLastFail.TabIndex = 21;
			this.lbLastFail.Text = "Last Fail:";
			// 
			// lbCountSMSFail
			// 
			this.lbCountSMSFail.AutoSize = true;
			this.lbCountSMSFail.Location = new System.Drawing.Point(129, 145);
			this.lbCountSMSFail.Name = "lbCountSMSFail";
			this.lbCountSMSFail.Size = new System.Drawing.Size(13, 13);
			this.lbCountSMSFail.TabIndex = 20;
			this.lbCountSMSFail.Text = "0";
			// 
			// lbNumberFail
			// 
			this.lbNumberFail.AutoSize = true;
			this.lbNumberFail.Location = new System.Drawing.Point(6, 145);
			this.lbNumberFail.Name = "lbNumberFail";
			this.lbNumberFail.Size = new System.Drawing.Size(86, 13);
			this.lbNumberFail.TabIndex = 19;
			this.lbNumberFail.Text = "NumberSMSFail:";
			// 
			// lbLastSuccess
			// 
			this.lbLastSuccess.AutoSize = true;
			this.lbLastSuccess.Location = new System.Drawing.Point(6, 121);
			this.lbLastSuccess.Name = "lbLastSuccess";
			this.lbLastSuccess.Size = new System.Drawing.Size(74, 13);
			this.lbLastSuccess.TabIndex = 18;
			this.lbLastSuccess.Text = "Last Success:";
			// 
			// lbCountNumberSuccess
			// 
			this.lbCountNumberSuccess.AutoSize = true;
			this.lbCountNumberSuccess.Location = new System.Drawing.Point(129, 97);
			this.lbCountNumberSuccess.Name = "lbCountNumberSuccess";
			this.lbCountNumberSuccess.Size = new System.Drawing.Size(13, 13);
			this.lbCountNumberSuccess.TabIndex = 17;
			this.lbCountNumberSuccess.Text = "0";
			// 
			// lbNumberSMS
			// 
			this.lbNumberSMS.AutoSize = true;
			this.lbNumberSMS.Location = new System.Drawing.Point(6, 97);
			this.lbNumberSMS.Name = "lbNumberSMS";
			this.lbNumberSMS.Size = new System.Drawing.Size(111, 13);
			this.lbNumberSMS.TabIndex = 16;
			this.lbNumberSMS.Text = "NumberSMSSuccess:";
			// 
			// lblListServiceType
			// 
			this.lblListServiceType.AutoSize = true;
			this.lblListServiceType.Location = new System.Drawing.Point(110, 509);
			this.lblListServiceType.Name = "lblListServiceType";
			this.lblListServiceType.Size = new System.Drawing.Size(13, 13);
			this.lblListServiceType.TabIndex = 15;
			this.lblListServiceType.Text = "0";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 509);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(83, 13);
			this.label6.TabIndex = 14;
			this.label6.Text = "ListServiceType";
			// 
			// lblTypeOfModem
			// 
			this.lblTypeOfModem.AutoSize = true;
			this.lblTypeOfModem.Location = new System.Drawing.Point(110, 481);
			this.lblTypeOfModem.Name = "lblTypeOfModem";
			this.lblTypeOfModem.Size = new System.Drawing.Size(13, 13);
			this.lblTypeOfModem.TabIndex = 13;
			this.lblTypeOfModem.Text = "0";
			// 
			// lblTypeOfModemTitle
			// 
			this.lblTypeOfModemTitle.AutoSize = true;
			this.lblTypeOfModemTitle.Location = new System.Drawing.Point(6, 481);
			this.lblTypeOfModemTitle.Name = "lblTypeOfModemTitle";
			this.lblTypeOfModemTitle.Size = new System.Drawing.Size(80, 13);
			this.lblTypeOfModemTitle.TabIndex = 12;
			this.lblTypeOfModemTitle.Text = "TypeOfModem:";
			// 
			// lblCurrentServiceType
			// 
			this.lblCurrentServiceType.AutoSize = true;
			this.lblCurrentServiceType.Location = new System.Drawing.Point(110, 534);
			this.lblCurrentServiceType.Name = "lblCurrentServiceType";
			this.lblCurrentServiceType.Size = new System.Drawing.Size(13, 13);
			this.lblCurrentServiceType.TabIndex = 11;
			this.lblCurrentServiceType.Text = "0";
			// 
			// lblServiceTypeIDTitle
			// 
			this.lblServiceTypeIDTitle.AutoSize = true;
			this.lblServiceTypeIDTitle.Location = new System.Drawing.Point(6, 534);
			this.lblServiceTypeIDTitle.Name = "lblServiceTypeIDTitle";
			this.lblServiceTypeIDTitle.Size = new System.Drawing.Size(104, 13);
			this.lblServiceTypeIDTitle.TabIndex = 10;
			this.lblServiceTypeIDTitle.Text = "CurrentServiceType:";
			// 
			// lblModemName
			// 
			this.lblModemName.AutoSize = true;
			this.lblModemName.Location = new System.Drawing.Point(110, 558);
			this.lblModemName.Name = "lblModemName";
			this.lblModemName.Size = new System.Drawing.Size(13, 13);
			this.lblModemName.TabIndex = 9;
			this.lblModemName.Text = "0";
			// 
			// lblModemNameTitle
			// 
			this.lblModemNameTitle.AutoSize = true;
			this.lblModemNameTitle.Location = new System.Drawing.Point(6, 558);
			this.lblModemNameTitle.Name = "lblModemNameTitle";
			this.lblModemNameTitle.Size = new System.Drawing.Size(73, 13);
			this.lblModemNameTitle.TabIndex = 8;
			this.lblModemNameTitle.Text = "ModemName:";
			// 
			// lblModemPartner
			// 
			this.lblModemPartner.AutoSize = true;
			this.lblModemPartner.Location = new System.Drawing.Point(110, 583);
			this.lblModemPartner.Name = "lblModemPartner";
			this.lblModemPartner.Size = new System.Drawing.Size(13, 13);
			this.lblModemPartner.TabIndex = 7;
			this.lblModemPartner.Text = "0";
			// 
			// lblModemPartnerTitle
			// 
			this.lblModemPartnerTitle.AutoSize = true;
			this.lblModemPartnerTitle.Location = new System.Drawing.Point(6, 583);
			this.lblModemPartnerTitle.Name = "lblModemPartnerTitle";
			this.lblModemPartnerTitle.Size = new System.Drawing.Size(79, 13);
			this.lblModemPartnerTitle.TabIndex = 6;
			this.lblModemPartnerTitle.Text = "ModemPartner:";
			// 
			// lbTotalThread
			// 
			this.lbTotalThread.AutoSize = true;
			this.lbTotalThread.Location = new System.Drawing.Point(129, 73);
			this.lbTotalThread.Name = "lbTotalThread";
			this.lbTotalThread.Size = new System.Drawing.Size(13, 13);
			this.lbTotalThread.TabIndex = 5;
			this.lbTotalThread.Text = "0";
			// 
			// lblTotalThreadTitle
			// 
			this.lblTotalThreadTitle.AutoSize = true;
			this.lblTotalThreadTitle.Location = new System.Drawing.Point(6, 73);
			this.lblTotalThreadTitle.Name = "lblTotalThreadTitle";
			this.lblTotalThreadTitle.Size = new System.Drawing.Size(68, 13);
			this.lblTotalThreadTitle.TabIndex = 4;
			this.lblTotalThreadTitle.Text = "TotalThread:";
			// 
			// lblUpdateTimerStatus
			// 
			this.lblUpdateTimerStatus.AutoSize = true;
			this.lblUpdateTimerStatus.Location = new System.Drawing.Point(129, 49);
			this.lblUpdateTimerStatus.Name = "lblUpdateTimerStatus";
			this.lblUpdateTimerStatus.Size = new System.Drawing.Size(37, 13);
			this.lblUpdateTimerStatus.TabIndex = 3;
			this.lblUpdateTimerStatus.Text = "Status";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 49);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(101, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "UpdateTimerStatus:";
			// 
			// lblSendTimerStatus
			// 
			this.lblSendTimerStatus.AutoSize = true;
			this.lblSendTimerStatus.Location = new System.Drawing.Point(129, 25);
			this.lblSendTimerStatus.Name = "lblSendTimerStatus";
			this.lblSendTimerStatus.Size = new System.Drawing.Size(37, 13);
			this.lblSendTimerStatus.TabIndex = 1;
			this.lblSendTimerStatus.Text = "Status";
			// 
			// lblSendTimerTitle
			// 
			this.lblSendTimerTitle.AutoSize = true;
			this.lblSendTimerTitle.Location = new System.Drawing.Point(6, 25);
			this.lblSendTimerTitle.Name = "lblSendTimerTitle";
			this.lblSendTimerTitle.Size = new System.Drawing.Size(91, 13);
			this.lblSendTimerTitle.TabIndex = 0;
			this.lblSendTimerTitle.Text = "SendTimerStatus:";
			// 
			// trSend
			// 
			this.trSend.Interval = 1000;
			this.trSend.Tick += new System.EventHandler(this.SendTimer_Tick);
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.tabPage1);
			this.tabControl.Controls.Add(this.tabPage2);
			this.tabControl.Controls.Add(this.pageLinkProcess);
			this.tabControl.Controls.Add(this.tabPage3);
			this.tabControl.Location = new System.Drawing.Point(12, 30);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(486, 411);
			this.tabControl.TabIndex = 7;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.rtbSend);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(478, 385);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "SendMT";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// rtbSend
			// 
			this.rtbSend.Location = new System.Drawing.Point(6, 6);
			this.rtbSend.Name = "rtbSend";
			this.rtbSend.Size = new System.Drawing.Size(466, 375);
			this.rtbSend.TabIndex = 5;
			this.rtbSend.Text = "";
			this.rtbSend.TextChanged += new System.EventHandler(this.rtbSend_TextChanged);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.rtbUpdate);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(478, 385);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "GetMO";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// pageLinkProcess
			// 
			this.pageLinkProcess.Controls.Add(this.rtbLinkProcess);
			this.pageLinkProcess.Location = new System.Drawing.Point(4, 22);
			this.pageLinkProcess.Name = "pageLinkProcess";
			this.pageLinkProcess.Padding = new System.Windows.Forms.Padding(3);
			this.pageLinkProcess.Size = new System.Drawing.Size(478, 385);
			this.pageLinkProcess.TabIndex = 2;
			this.pageLinkProcess.Text = "Link Process";
			this.pageLinkProcess.UseVisualStyleBackColor = true;
			// 
			// rtbLinkProcess
			// 
			this.rtbLinkProcess.Dock = System.Windows.Forms.DockStyle.Fill;
			this.rtbLinkProcess.Location = new System.Drawing.Point(3, 3);
			this.rtbLinkProcess.Name = "rtbLinkProcess";
			this.rtbLinkProcess.Size = new System.Drawing.Size(472, 379);
			this.rtbLinkProcess.TabIndex = 0;
			this.rtbLinkProcess.Text = "";
			this.rtbLinkProcess.TextChanged += new System.EventHandler(this.rtbLinkProcess_TextChanged);
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.btnSendAgainSMSFail);
			this.tabPage3.Controls.Add(this.btnSendMoneyExport);
			this.tabPage3.Controls.Add(this.btnImportSendMoney);
			this.tabPage3.Controls.Add(this.lblProgressSendMoney);
			this.tabPage3.Controls.Add(this.dgvSendMoney);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(478, 385);
			this.tabPage3.TabIndex = 3;
			this.tabPage3.Text = "Config bắn tiền";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// btnSendAgainSMSFail
			// 
			this.btnSendAgainSMSFail.Location = new System.Drawing.Point(238, 6);
			this.btnSendAgainSMSFail.Name = "btnSendAgainSMSFail";
			this.btnSendAgainSMSFail.Size = new System.Drawing.Size(75, 23);
			this.btnSendAgainSMSFail.TabIndex = 34;
			this.btnSendAgainSMSFail.Text = "Gửi lại các Fail";
			this.btnSendAgainSMSFail.UseVisualStyleBackColor = true;
			this.btnSendAgainSMSFail.Click += new System.EventHandler(this.btnSendAgainSMSFail_Click);
			// 
			// btnSendMoneyExport
			// 
			this.btnSendMoneyExport.Location = new System.Drawing.Point(319, 6);
			this.btnSendMoneyExport.Name = "btnSendMoneyExport";
			this.btnSendMoneyExport.Size = new System.Drawing.Size(75, 23);
			this.btnSendMoneyExport.TabIndex = 33;
			this.btnSendMoneyExport.Text = "Export";
			this.btnSendMoneyExport.UseVisualStyleBackColor = true;
			this.btnSendMoneyExport.Click += new System.EventHandler(this.btnSendMoneyExport_Click);
			// 
			// btnImportSendMoney
			// 
			this.btnImportSendMoney.Location = new System.Drawing.Point(400, 6);
			this.btnImportSendMoney.Name = "btnImportSendMoney";
			this.btnImportSendMoney.Size = new System.Drawing.Size(75, 23);
			this.btnImportSendMoney.TabIndex = 9;
			this.btnImportSendMoney.Text = "Import";
			this.btnImportSendMoney.UseVisualStyleBackColor = true;
			this.btnImportSendMoney.Click += new System.EventHandler(this.btnImportSendMoney_Click);
			// 
			// lblProgressSendMoney
			// 
			this.lblProgressSendMoney.AutoSize = true;
			this.lblProgressSendMoney.Location = new System.Drawing.Point(6, 11);
			this.lblProgressSendMoney.Name = "lblProgressSendMoney";
			this.lblProgressSendMoney.Size = new System.Drawing.Size(30, 13);
			this.lblProgressSendMoney.TabIndex = 32;
			this.lblProgressSendMoney.Text = "0 / 0";
			// 
			// dgvSendMoney
			// 
			this.dgvSendMoney.AllowUserToAddRows = false;
			this.dgvSendMoney.AllowUserToDeleteRows = false;
			this.dgvSendMoney.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dgvSendMoney.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
			this.dgvSendMoney.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvSendMoney.Location = new System.Drawing.Point(3, 35);
			this.dgvSendMoney.Name = "dgvSendMoney";
			this.dgvSendMoney.Size = new System.Drawing.Size(472, 344);
			this.dgvSendMoney.TabIndex = 26;
			// 
			// btnSaveSend
			// 
			this.btnSaveSend.Location = new System.Drawing.Point(333, 449);
			this.btnSaveSend.Name = "btnSaveSend";
			this.btnSaveSend.Size = new System.Drawing.Size(75, 23);
			this.btnSaveSend.TabIndex = 4;
			this.btnSaveSend.Text = "Save";
			this.btnSaveSend.UseVisualStyleBackColor = true;
			this.btnSaveSend.Click += new System.EventHandler(this.btnSaveSend_Click);
			// 
			// btnClearSend
			// 
			this.btnClearSend.Location = new System.Drawing.Point(414, 449);
			this.btnClearSend.Name = "btnClearSend";
			this.btnClearSend.Size = new System.Drawing.Size(75, 23);
			this.btnClearSend.TabIndex = 0;
			this.btnClearSend.Text = "Clear";
			this.btnClearSend.UseVisualStyleBackColor = true;
			this.btnClearSend.Click += new System.EventHandler(this.btnClearSend_Click);
			// 
			// btnStartSend
			// 
			this.btnStartSend.Location = new System.Drawing.Point(252, 449);
			this.btnStartSend.Name = "btnStartSend";
			this.btnStartSend.Size = new System.Drawing.Size(75, 23);
			this.btnStartSend.TabIndex = 2;
			this.btnStartSend.Text = "Start";
			this.btnStartSend.UseVisualStyleBackColor = true;
			this.btnStartSend.Click += new System.EventHandler(this.btnStartSend_Click);
			// 
			// bgwSendMT
			// 
			this.bgwSendMT.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SendMT_DoWork);
			// 
			// bgwGetMO
			// 
			this.bgwGetMO.DoWork += new System.ComponentModel.DoWorkEventHandler(this.V2_GetMO_DoWork);
			// 
			// RegisterPromotion
			// 
			this.RegisterPromotion.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RegisterPromotion_DoWork);
			// 
			// chkOnLast
			// 
			this.chkOnLast.AutoSize = true;
			this.chkOnLast.Checked = true;
			this.chkOnLast.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkOnLast.Location = new System.Drawing.Point(12, 443);
			this.chkOnLast.Name = "chkOnLast";
			this.chkOnLast.Size = new System.Drawing.Size(68, 17);
			this.chkOnLast.TabIndex = 8;
			this.chkOnLast.Text = "Theo dõi";
			this.chkOnLast.UseVisualStyleBackColor = true;
			this.chkOnLast.CheckedChanged += new System.EventHandler(this.chkOnLast_CheckedChanged);
			// 
			// bgwSendSMSMoney
			// 
			this.bgwSendSMSMoney.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwSendSMSMoney_DoWork);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(758, 658);
			this.Controls.Add(this.chkOnLast);
			this.Controls.Add(this.tabControl);
			this.Controls.Add(this.btnSaveSend);
			this.Controls.Add(this.gbInformationSystem);
			this.Controls.Add(this.btnClearSend);
			this.Controls.Add(this.gbConfig);
			this.Controls.Add(this.btnStartSend);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.statusStrip1);
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Bắn tiền";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.gbConfig.ResumeLayout(false);
			this.gbConfig.PerformLayout();
			this.gbInformationSystem.ResumeLayout(false);
			this.gbInformationSystem.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.tabControl.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.pageLinkProcess.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvSendMoney)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsSend;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.RichTextBox rtbUpdate;
        private System.Windows.Forms.GroupBox gbConfig;
        private System.Windows.Forms.GroupBox gbInformationSystem;
        private System.Windows.Forms.Label lblSendTimerTitle;
        private System.Windows.Forms.Label lblSendTimerStatus;
        private System.Windows.Forms.Label lblUpdateTimerStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbTotalThread;
        private System.Windows.Forms.Label lblTotalThreadTitle;
        private System.Windows.Forms.TextBox txtLengOfFile;
        private System.Windows.Forms.TextBox txtLogFilePath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtInterval;
        private System.Windows.Forms.TextBox txtNumberOfThread;
        private System.Windows.Forms.Button btnSaveConfig;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer trSend;
        private System.Windows.Forms.Label lblModemPartner;
        private System.Windows.Forms.Label lblModemPartnerTitle;
        private System.Windows.Forms.Label lblModemName;
        private System.Windows.Forms.Label lblModemNameTitle;
        private System.Windows.Forms.CheckBox cbMultiThread;
        private System.Windows.Forms.Label lblCurrentServiceType;
        private System.Windows.Forms.Label lblServiceTypeIDTitle;
        private System.Windows.Forms.Label lblTypeOfModem;
        private System.Windows.Forms.Label lblTypeOfModemTitle;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnSaveSend;
        private System.Windows.Forms.Button btnStartSend;
        private System.Windows.Forms.Button btnClearSend;
        private System.ComponentModel.BackgroundWorker bgwSendMT;
        private System.ComponentModel.BackgroundWorker bgwGetMO;
        private System.Windows.Forms.RichTextBox rtbSend;
        private System.Windows.Forms.ToolStripButton bttestGetMO;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblListServiceType;
        private System.Windows.Forms.ToolStripButton btTestManager;
        private System.Windows.Forms.TextBox tbPass;
        private System.Windows.Forms.Label lbLastFail;
        private System.Windows.Forms.Label lbCountSMSFail;
        private System.Windows.Forms.Label lbNumberFail;
        private System.Windows.Forms.Label lbLastSuccess;
        private System.Windows.Forms.Label lbCountNumberSuccess;
        private System.Windows.Forms.Label lbNumberSMS;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbDelay;
        private System.Windows.Forms.Label lbDelay;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.Label lbTotal;
        private System.Windows.Forms.CheckBox cbAccept;
        private System.Windows.Forms.Label lbtin;
        private System.Windows.Forms.Label lbsecond;
        private System.Windows.Forms.CheckBox cbCheckMoney;
        private System.Windows.Forms.Label lblCaculatorMoney;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblCurrentMoney;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblAmountSMSKM;
        private System.ComponentModel.BackgroundWorker RegisterPromotion;
        private System.Windows.Forms.TextBox tbTimeOut;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkOnLast;
        private System.Windows.Forms.TabPage pageLinkProcess;
        private System.Windows.Forms.RichTextBox rtbLinkProcess;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButtonGetAmoutSMS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripButton toolStripButtonRestartCOM;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.DataGridView dgvSendMoney;
		private System.Windows.Forms.Label lblProgressSendMoney;
		private System.Windows.Forms.Button btnImportSendMoney;
		private System.ComponentModel.BackgroundWorker bgwSendSMSMoney;
		private System.Windows.Forms.Button btnSendMoneyExport;
		private System.Windows.Forms.Button btnSendAgainSMSFail;
    }
}

