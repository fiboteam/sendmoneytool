﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    public enum statCMGL : short
    {
        ReceivedUnreadMessage = 0,
        ReceivedReadMessage = 1,
        StoredUnsentMessage = 2,
        StoredSentMessage = 3,
        AllMessage = 4
    }
    public enum SMSStatus : short
    {
        Pending = 0,
        Sending = 1,
        SentSuccess = 2,
        SentFailed = 3,
        Deleted = 4,
        ToNumberNotCorrect = 5,
        CanNotSend = 6,
        OutOfDate = 7,
        SendMTFailed = 8,//smsgateway send MT failed
        Other = 9,
        SpamSMS = 10,
        RestoreToAccountClient = 11,
        Delay = 12,
        WrongSenderName = 13,
        WaitResult = 14
    }

    public enum SMSSentStatus : short
    {
        Pending = 0,
        Sending = 1,
        SentSuccess = 2,
        SentFailed = 3,
        Deleted = 4,
        ToNumberNotCorrect = 5,
        CanNotSend = 6,
        OutOfDate = 7,
        SendMTFailed = 8,//smsgateway send MT failed
        Other = 9,
        SpamSMS = 10,
        RestoreToAccountClient = 11,
        Delay = 12,
        WrongSenderName = 13,
        WaitResult = 14
    }

    public enum SMSHostingMessageType : short //Sử dụng chung cho các dịch vụ SMS Hosting
    {
        Draft = 0,
        NormalSMS = 1,
        ConcatenatedSMS = 2,
        Pending = 3,
        Delay = 9,
        CheckPhoneNumberStatus = 10,
		Flash,
    }

    public enum SMSHostingContentType : short //Sử dụng chung cho các dịch vụ SMS Hosting
    {
        Text = 0,
        Wappush = 8,
        Unicode = 11
    }

    public enum IsSubstract : short
    {
        NotSubstract = 0,
        IsSubstracted = 1
    }

    public enum PushSMSStatus : short
    {
        JustReceived = 1,
        Processing = 2,
        Success = 3,
        Fail = 4,
        NotQueryURL = 5
    }

    public enum PhoneNumberStatus : short //Dùng dịch vụ kiểm tra số điện thoại của Infobip
    {
        Success = 1,
        NotSuccess = 2,
        NotCheck = 3,
        NotResult = 4
    }

    public enum ModemAdaptorStatus : short
    {
        DeActive = 0,
        Active = 1,
        Blocked = 4,
        Asisstant = 5,
        SimOut = 6
    }

    public enum ModemType : short
    {
        PublicModem = 0,
        PrivateModem = 1
    }

    public enum UpdateModemAdaptor : short
    {
        TurnOn = 0,
        TurnOff = 1
    }

    public enum TurnOffModemAdaptor : short
    {
        TurnOn = 0,
        TurnOff = 1
    }

    public enum CurrentErrorType : short
    {
        Normal = 0,
        ConnectionError = 1,
        OpenPortError = 2,
        ConnectModemError = 3,
        SendSMSError = 4,
        EntityError = 5,
        WrongCom = 6
    }

    public enum ClientStatus : short
    {
        Registered = 0,
        Active = 1,
        Deactive = 2,
        Deleted = 3,
        Expired = 4
    }

    public enum ClientType : short
    {
        SMSGateway = 0,
        SMSHosting = 1,
        Full = 2
    }

    public enum IsAgency : short
    {
        Active = 0,
        DeActive = 1
    }

    public enum IsConcatenatedSMS : short
    {
        Active = 0,
        DeActive = 1
    }

    public enum IsNewService : short
    {
        Active = 1,
        DeActive = 2
    }

    public enum PayLater : short
    {
        Active = 1,
        DeActive = 2
    }

    public enum IsPersonal : short
    {
        Active = 0,
        DeActive = 1
    }

    public enum IsApprove : short
    {
        Active = 0,
        DeActive = 1
    }

    public enum IsSpam : short
    {
        Active = 0,
        DeActive = 1
    }

    public enum TelCoStatus : short
    {
        DeActive = 0,
        Active = 1
    }

    /// <summary>
    /// Service type la dich vu dau so vi du : 19001733
    /// </summary>
    public enum ServiceTypeStatus : short
    {
        Draft = 0,// Moi tao
        Active = 1,// Duoc kich hoat
        Deactive = 2,// khong duoc kich hoat
        Deleted = 3,// xoa
        Expired = 4// het han
    }

    public enum TypeOfService : short
    {
        DauSo = 0,
        SimCard = 1,//SendSMSNormalService = 1,
        SenderName = 2,
        SendSMSEmail = 3,
        SendSMSFree = 4,
        PrivateNotBalance = 5,
        PrivateAndBalance = 6,
        SentByGateway = 7,
        CheckPhoneNumber = 8,
        TopUp = 9
    }

    public enum ServiceTypePaymentStatus : short
    {
        NotPay = 0,
        Tranfer = 1,
        Paid = 2
    }

    public enum IsPublicKeyword : short
    {
        True = 1, //Keyword chung
        False = 0 //Keyword rieng
    }

    public enum PaymentStatus : short
    {
        NotPay = 0,
        Tranfer = 1,
        Paid = 2
    }

    public enum ServiceCompanyStatus : short
    {
        Active = 0,
        DeActive = 1
    }

    public enum IsSMSPro : short
    {
        Active = 0,
        DeActive = 1
    }

    public enum ClientCommingSMSHostingStatus : short
    {
        JustReceived = 0,// Moi nhan duoc tin SMS tu thiet bi va luu lai hang doi
        Processing = 1,// sau khi lay sms de xu ly thi doi status thanh Processing, 
        Confirmed = 2,//neu site khach hang confirm thi doi status thanh confirm
        WrongSyntax = 3,// kiem tra sms syntact voi client syntact, neu khong dung voi syntact nao cua clien thi sms co trang thai nay
        Expired = 4,// Neu ProcessingTime lon hon 20 thi doi trang thai cua sms ve Expired
        NoClientResponse = 5,// Neu ProcessingTime lon hon 20 thi doi trang thai cua sms ve Expired
        ProcessingError = 6,
        ClientResponseWrongFormat = 7
    }

    public enum IsSentMail : short
    {
        Active = 0,
        DeActive = 1
    }

    public enum IsPaid : short
    {
        Active = 0,
        DeActive = 1
    }

    public enum UserStatus : short
    {
        JustCreated = 1,
        Active = 2,
        Deactive = 3,
        Expired = 4,
        Delete = 5,
        Other = 6
    }

    public enum SyntaxStatus : short
    {
        Draft = 0,// moi tao
        Active = 1,// Duoc kich hoat, SMSAlert chi lay nhung syntaxt nao co status la Active
        DeActive = 2,// Tam thoi khong kich hoat
        Approve = 3,
        Deleted = 4
    }

    public enum IsFullCharged : short
    {
        Active = 1,
        DeActive = 2
    }

    public enum SyntaxType : short
    {
        SMSGateway = 1,
        PushSMSStatus = 2,
        PushInComingSMSHosting = 3
    }

    public enum ClientIPStatus : short
    {
        DeActive = 0,
        Active = 1
    }

    public enum ErrorCode : short
    {
        Other = 100,
        UserNotExist = 101,
        WrongPassword = 102,
        AccountIsDeactived = 103,
        CanNotAccess = 104,
        AccountIsZero = 105,
        Success = 106,
        WrongSign = 107,
        WrongBrandName = 108,
        ExceedSms = 109,
        SendSmsFail = 110,
        WrongServiceType = 111,
        ExpiredRequest = 112,
        NotHasAESKey = 113
    }

    public enum ClientServiceChargesStatus : short
    {
        Active = 1,
        DeActive = 2
    }

    public enum ClientSenderNameStatus : short
    {
        Draft = 0,
        Active = 1,
        Deactive = 2,
        Deleted = 3,
        Expired = 4
    }

    public enum OperatorStatus : short
    {
        NOT_DELIVERED = 0, //Nhà mạng không gửi đến đầu cuối
        DELIVERED = 1, //Nhà mạng đã gửi đến đầu cuối
        NOT_SENT = 2, //Gửi tin không đến nhà mạng được
        NOT_HAVE_REPORT = 3  //Không có report từ nhà mạng về tình trạng tin nhắn
    }
    /// <summary>
    /// Trạng thái hành động của modem
    /// 19/05/2015
    /// NMH
    /// </summary>
    public enum StatusModemAdaptorAction:short
    {
        JustCreated = 1,
        Processed = 2,
    }
    public enum TypeModemAdaptorAction : short
    {
        Not_Have_Balance
    }

	
}
