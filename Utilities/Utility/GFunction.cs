﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Web;
using System.Security;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Security.Cryptography;
using System.Configuration;

using Encryption;
using System.IO;

namespace Utility
{
    public static class GFunction
    {
        /// <summary>
        /// Set value in XML config file
        /// </summary>
        /// <param name="filePath">The path to Config File, default is SystemConfig.xml</param>
        /// <param name="parameterName"></param>
        /// <param name="parameterValue"></param>
        public static void SetValueForSystemConfigParameter(string filePath, string parameterName, string parameterValue)
        {
            try
            {
                if (filePath.Length <= 0)
                    filePath = @"SystemConfig.xml";

                XmlDocument Xmldoc = new XmlDocument();
                Xmldoc.Load(filePath);

                XmlNode root = Xmldoc.DocumentElement;
                XmlNodeList listchildnode = root.ChildNodes;

                foreach (XmlElement element in listchildnode)
                {
                    if (element.Name.Equals(parameterName))
                    {
                        element.InnerText = parameterValue;
                        break;
                    }
                }

                Xmldoc.Save(filePath);
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Get value of parameter from xml file config
        /// </summary>
        /// <param name="filePath">The path to Config File, default is SystemConfig.xml</param>
        /// <param name="parameterName"></param>
        /// <returns></returns>
        public static string GetSystemConfigParameter(string filePath, string parameterName)
        {
            try
            {
                if (filePath.Length <= 0)
                    filePath = @"SystemConfig.xml";

                XmlDocument Xmldoc = new XmlDocument();
                Xmldoc.Load(filePath);

                XmlNode root = Xmldoc.DocumentElement;
                XmlNodeList listchildnode = root.ChildNodes;

                foreach (XmlElement element in listchildnode)
                {
                    if (element.Name.Equals(parameterName))
                        return element.InnerText;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string Encrypt(string encryptString, string separator = "", string extension = "")
        {
            string s = encryptString + separator + extension;
            return Crypto.ActionEncrypt(s);
        }

        public static string[] Decrypt(string encryptedString, string separator = "")
        {
            string s = Crypto.ActionDecrypt(encryptedString);
            string[] separators = new string[] { separator };
            return s.Split(separators, StringSplitOptions.None);
        }

        public static string Decrypt(string encryptedString, string separator = "", int indexOfString = 0)
        {
            string s = Crypto.ActionDecrypt(encryptedString);
            if (separator != null && separator.Length > 0)
                s = s.Substring(indexOfString, s.IndexOf(separator));

            return s;
        }

        public static string ConvertSpecialXMLCharToOriginalChar(string strString)
        {
            string strRet = "";
            if (strString != null)
            {
                strRet = HttpUtility.HtmlDecode(strString);
            }
            return strRet;
        }

        public static string ReplaceSpecialCharsForXML(string strString)
        {
            string strRet = "";
            if (strString != null)
            {
                strRet = SecurityElement.Escape(strString);
            }
            return strRet;
        }

        public static string GetSystemConfigParameter(string parameterName)
        {
            try
            {
                XmlDocument Xmldoc = new XmlDocument();
                Xmldoc.Load(@"SystemConfig.xml");

                XmlNode root = Xmldoc.DocumentElement;
                XmlNodeList listchildnode = root.ChildNodes;
                foreach (XmlElement element in listchildnode)
                {
                    if (element.Name.Equals(parameterName))
                        return element.InnerText;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string GetAppConfigParameter(string parName)
        {
            try
            {
                return ConfigurationManager.AppSettings[parName];
            }
            catch
            {
            }
            return "";
        }

        public static string[] SplitByString(string testString, string split)
        {
            int offset = 0;
            int index = 0;
            int[] offsets = new int[testString.Length + 1];

            while (index < testString.Length)
            {
                int indexOf = testString.IndexOf(split, index);
                if (indexOf != -1)
                {
                    offsets[offset++] = indexOf;
                    index = (indexOf + split.Length);
                }
                else
                {
                    index = testString.Length;
                }
            }

            string[] final = new string[offset + 1];
            if (offset == 0)
            {
                final[0] = testString;
            }
            else
            {
                offset--;
                final[0] = testString.Substring(0, offsets[0]);
                for (int i = 0; i < offset; i++)
                {
                    final[i + 1] = testString.Substring(offsets[i] + split.Length, offsets[i + 1] - offsets[i] - split.Length);
                }
                final[offset + 1] = testString.Substring(offsets[offset] + split.Length);
            }
            return final;
        }

        public static void SetValueForSystemConfigParameter(string parameterName, string parameterValue)
        {
            try
            {
                XmlDocument Xmldoc = new XmlDocument();
                Xmldoc.Load(@"SystemConfig.xml");

                XmlNode root = Xmldoc.DocumentElement;
                XmlNodeList listchildnode = root.ChildNodes;
                foreach (XmlElement element in listchildnode)
                {
                    if (element.Name.Equals(parameterName))
                    {
                        element.InnerText = parameterValue;
                        break;
                    }
                }
                Xmldoc.Save(@"SystemConfig.xml");
            }
            catch (Exception ex)
            {
            }
        }

        public static string ReplaceSpecialChars(string strString)
        {
            if (strString == null)
                return "";
            else
                return strString.Replace("'", "''");
        }
        /// <summary>
        /// Check Client IP is exist in the configuration list in file config
        /// </summary>
        /// <param name="nameOfParameter">The name of parameter in file config that contains List Client IP</param>
        /// <returns></returns>
        public static bool CheckClientIP_WCF(string nameOfParameter = "ListClientIP")
        {
            try
            {
                if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AllowAllIP"].ToString()))
                    return true;
                OperationContext context = OperationContext.Current;
                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                string clientIP = endpointProperty.Address;

                if (ConfigurationManager.AppSettings[nameOfParameter].Contains(clientIP))
                    return true;

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static string GetClientIP_WCF()
        {
            try
            {
                OperationContext context = OperationContext.Current;
                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                return endpointProperty.Address;
            }
            catch
            {
                return "";
            }
        }

        public static string RandomString(int size)
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            StringBuilder builder = new StringBuilder();

            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public static string GetMD5(string pwd)
        {
            if (pwd != null)
            {
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] bHash = md5.ComputeHash(Encoding.UTF8.GetBytes(pwd));
                StringBuilder sbHash = new StringBuilder();

                foreach (byte b in bHash)
                {
                    sbHash.Append(String.Format("{0:x2}", b));
                }
                return sbHash.ToString();
            }
            return "";
        }

        public static String AES_Encrypt(String Input, string AES_Key, string AES_IV)
        {
            var aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 256;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Convert.FromBase64String(AES_Key);
            aes.IV = Convert.FromBase64String(AES_IV);

            var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] xBuff = null;
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
                {
                    byte[] xXml = Encoding.UTF8.GetBytes(Input);
                    cs.Write(xXml, 0, xXml.Length);
                }

                xBuff = ms.ToArray();
            }

            String Output = Convert.ToBase64String(xBuff);
            return Output;
        }

        public static String AES_Decrypt(String Input, string AES_Key, string AES_IV)
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 256;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            aes.Key = Convert.FromBase64String(AES_Key);
            aes.IV = Convert.FromBase64String(AES_IV);

            var decrypt = aes.CreateDecryptor();
            byte[] xBuff = null;
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
                {
                    byte[] xXml = Convert.FromBase64String(Input);
                    cs.Write(xXml, 0, xXml.Length);
                }

                xBuff = ms.ToArray();
            }

            String Output = Encoding.UTF8.GetString(xBuff);
            return Output;
        }
    }
}
