using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface IDaoFactory
    {

		ISMSSendMoney SMSSendMoneyDao { get; }

    }
}