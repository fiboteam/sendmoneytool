using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface IKeywordPlanner : IDao<KeywordPlanner>
    {
		IList<KeywordPlanner> GetList(long pageSize, long pageNum, string keywordName,int avgMonthlySearches,string competition,decimal suggestedBid,string formatCurrency,short keywordPlanerStatus,string note,string remark,int shortCreatedDate);

		bool InsertWithImport(KeywordPlanner ob, long blockId);
		IList<KeywordPlanner> GetAllWithFilter(KeywordPlanner ob);
		IList<KeywordPlanner> GetAllWithBlockId(long blockId,long userid);

		List<KeywordPlanner> GetAllKeywordPlannerNameToRefresh();
		bool UpdateFromRefresh(KeywordPlanner ob);
	}
}
