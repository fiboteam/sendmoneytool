using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
	public interface ISMSSendMoney : IDao<SMSSendMoney>
    {
		bool InsertWithModem(SMSSendMoney ob);
	}
}
