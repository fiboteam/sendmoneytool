﻿using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAO.Interface
{
    public interface IDao<T> where T : ObjectBase
    {
        /// <summary>
        /// Add entity to the repository
        /// </summary>
        /// <param name="entity">the entity to add</param>
		bool Insert(T businessObject);

        /// <summary>
        /// Update entity within the repository
        /// </summary>
        /// <param name="entity">the entity to update</param>
        bool Update(T businessObject);

        /// <summary> 
        /// Mark entity to be deleted within the repository 
        /// </summary> 
        /// <param name="entity">The entity to delete</param> 
        int Delete(long entityID);

        /// <summary>
        /// Get a selected extiry by the object primary key ID
        /// </summary>
        /// <param name="entityID">Primary key ID</param>
        T GetSingle(long entityID);

        /// <summary>
        /// Get all the element of this repository
        /// </summary>
        /// <returns></returns>
        IList<T> GetAll();

        /// <summary>
        /// All item count
        /// </summary>
        /// <returns></returns>
        int Count();
    }
}
