using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using DAO.Interface;


namespace DAO
{
    public static class DataAccess
    {
		//private static readonly string connectionStringName = ConfigurationManager.AppSettings.Get("ConnectionStringName");
		private static readonly string connectionStringName = Properties.Settings.Default.ConnectionStringName;

		/*
		 * Không cần ép kiểu nữa
		 * private static readonly IDaoFactory factory = (IDaoFactory)DaoFactories.GetFactory(connectionStringName);
		 */
		private static readonly IDaoFactory factory = DaoFactories.GetFactory(connectionStringName);


		public static ISMSSendMoney SMSSendMoneyDao { get { return factory.SMSSendMoneyDao; } }
    }
}