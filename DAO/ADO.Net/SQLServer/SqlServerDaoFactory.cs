using DAO.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.AdoNet.SqlServer
{
	public class SqlServerDaoFactory : IDaoFactory
	{
		public ISMSSendMoney SMSSendMoneyDao
		{
			get { return new SqlSMSSendMoneyDao(); }
		}
	}
}