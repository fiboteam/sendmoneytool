using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DAO.AdoNet.SqlServer
{
    public class SqlSMSSendMoneyDao : SqlDaoBase<SMSSendMoney>, ISMSSendMoney
    {
        public SqlSMSSendMoneyDao()
        {
			TableName = "tblSMSSendMoney";
			EntityIDName = "SMSSendMoneyId";
			StoreProcedurePrefix = "spSMSSendMoney_";
        }
		public SqlSMSSendMoneyDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		//public IList<KeywordPlanner> GetList(long pageSize, long pageNum, string keywordName,int avgMonthlySearches,string competition,decimal suggestedBid,string formatCurrency,short keywordPlanerStatus,string note,string remark,int shortCreatedDate)
		//{
		//	//try
		//	//{
		//	//	string sql = StoreProcedurePrefix + "GetList";
		//	//	object[] parms = { "@pagesize", pageSize, "@pagenum", pageNum, "@keywordname",keywordName,"@avgmonthlysearches",avgMonthlySearches,"@competition",competition,"@suggestedbid",suggestedBid,"@formatcurrency",formatCurrency,"@keywordplanerstatus",keywordPlanerStatus,"@note",note,"@remark",remark,"@shortcreateddate",shortCreatedDate};
		//	//	return DbAdapter1.ReadList(sql, Make, true, parms);
		//	//}
		//	//catch (Exception)
		//	//{
		//	return null;
		//	//}
		//}


		//public bool InsertWithImport(KeywordPlanner ob, long blockId)
		//{
		//	string sql = StoreProcedurePrefix + "AddWithImport";
		//	object[] parms = { "@BlockID", blockId, "@KeywordName", ob.KeywordName };
		//	var id = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

		//	if (id > 0)
		//		return true;
		//	else
		//		return false;
		//}


		//public IList<KeywordPlanner> GetAllWithFilter(KeywordPlanner ob)
		//{
		//	try
		//	{
		//		string sql = StoreProcedurePrefix + "GetAllWithFilterToView";
		//		object[] parms = { "@KeywordName", ob.KeywordName, "@AvgMonthlySearches", ob.AvgMonthlySearches, "@Competition", ob.Competition, "@SuggestedBid", ob.SuggestedBid, "@KeywordPlanerStatus", (short)ob.KeywordPlanerStatus, "@userid", ob.UserId };
		//		return DbAdapter1.ReadList(sql, Make, true, parms);
		//	}
		//	catch (Exception)
		//	{
		//	return null;
		//	}
		//}


		//public IList<KeywordPlanner> GetAllWithBlockId(long blockId,long userid)
		//{
		//	try
		//	{
		//		string sql = StoreProcedurePrefix + "GetAllWithBlockIdToView";
		//		object[] parms = { "@BlockID", blockId, "@userid", userid };
		//		return DbAdapter1.ReadList(sql, Make, true, parms);
		//	}
		//	catch (Exception)
		//	{
		//		return null;
		//	}
		//}


		//public List<KeywordPlanner> GetAllKeywordPlannerNameToRefresh()
		//{
		//	try
		//	{
		//		string sql = StoreProcedurePrefix + "GetAllKeywordPlannerNameToRefresh";
		//		//object[] parms = { "@BlockID", blockId };
		//		return DbAdapter1.ReadList(sql, Make, true, null);
		//	}
		//	catch (Exception)
		//	{
		//		return null;
		//	}
		//}


		//public bool UpdateFromRefresh(KeywordPlanner ob)
		//{
		//	string sql = StoreProcedurePrefix + "UpdateFromRefresh";
		//	object[] parms = { "@keywordName", ob.KeywordName, "@AvgMonthlySearches", ob.AvgMonthlySearches, "@Competition", ob.Competition, "@SuggestedBid", ob.SuggestedBid, "@FormatCurrency", ob.FormatCurrency, "@CompetitionIndex", ob.CompetitionIndex, "@ClickRate", ob.ClickRate, "@TotalCostPerMonth",ob.TotalCostPerMonth,
		//					 "@SEOFibo",ob.SEOFibo,"@ChargesForMaintenance",ob.ChargesForMaintenance,"@Setup",ob.Setup,"@HardWorkIndex",ob.HardWorkIndex,"@CompetitionInt",ob.CompetitionInt,"@SetupCost",ob.SetupCost,"@KeywordPlanerStatus",(short)ob.KeywordPlanerStatus};
		//	ob.ID = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

		//	if (ob.ID > 0)
		//		return true;
		//	else
		//		return false;
		//}

		public bool InsertWithModem(SMSSendMoney ob)
		{
			string sql = StoreProcedurePrefix + "AddNew";
			object[] parms = { "@PhoneNumber", ob.PhoneNumber, "@Money", int.Parse(ob.Money), "@Status", (short)ob.SMSSendMoneyStatus, "@remark", ob.Remark, "@modemsent", ob.ModemSent ?? "", "@GUID", ob.GUID };
			var id = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

			if (id > 0)
				return true;
			else
				return false;
		}
	}
}
