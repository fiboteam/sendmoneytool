USE [fibosms]
GO
/****** Object:  StoredProcedure [dbo].[spSMSSendMoney_AddNew]    Script Date: 05/11/2016 14:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spSMSSendMoney_AddNew]
	@PhoneNumber varchar(50),
	@Money int,
	@Status tinyint,
	@remark nvarchar(max),
	@modemsent nvarchar(max),
	@GUID nvarchar(100)
as
begin
	declare @telcoid int = dbo.fnFindTelCo(@PhoneNumber)
	
	insert into tblsmssendmoney (phonenumber,[money],telcoid,SMSSendMoneyStatus,remark,ModemSent,GUID)
	values (@PhoneNumber,@Money,@telcoid,@Status,@remark,@modemsent,@GUID)
	
	select @@IDENTITY
end