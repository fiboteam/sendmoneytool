﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class SMSPro : BusinessObject
    {
        #region Private Members
        private string mMessage = "";

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string Message
        {
            get { return this.mMessage; }
            set { GFunction.ConvertSpecialXMLCharToOriginalChar(value); }
        }

        [DataMember]
        public string SMSGUID { get; set; }

        [DataMember]
        public SMSStatus SMSStatus { get; set; }

        [DataMember]
        public DateTime SentDate { get; set; }

        [DataMember]
        public long SendingTime { get; set; }

        [DataMember]
        public string InCommingSMSID { get; set; }

        [DataMember]
        public string SenderName { get; set; }

        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        public SMSHostingMessageType MessageType { get; set; }

        [DataMember]
        public SMSHostingContentType ContentType { get; set; }

        [DataMember]
        public IsSpam IsSpam { get; set; }

        [DataMember]
        public DateTime DelayDate { get; set; }

        [DataMember]
        public double Price { get; set; }

        [DataMember]
        public IsSubstract IsSubstract { get; set; }

        [DataMember]
        public IsNewService IsNewService { get; set; }

        [DataMember]
        public int TotalSMS { get; set; }

        [DataMember]
        public int PartnerCode { get; set; }

        #region ForeignKey

        [DataMember]
        public long ClientID { get; set; }

        [DataMember]
        public Client Client { get; set; }

        [DataMember]
        public long ServiceTypeID { get; set; }

        [DataMember]
        public ServiceType ServiceType { get; set; }

        [DataMember]
        public long TelcoID { get; set; }

        [DataMember]
        public Telco Telco { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public SMSPro()
        {
            ClientID = -1;
            PhoneNumber = "";
            Message = "";
            SMSGUID = "";
            SMSStatus = Utility.SMSStatus.Pending;
            CreatedDate = DateTime.Now;
            SentDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            ServiceTypeID = -1;
            SendingTime = 0;
            InCommingSMSID = "=1";
            SenderName = "n/a";
            CountryCode = "";
            TelcoID = 0;
            MessageType = SMSHostingMessageType.Draft;
            ContentType = SMSHostingContentType.Text;
            IsSpam = Utility.IsSpam.DeActive;
            DelayDate = DateTime.Now;
            Price = -1;
            IsSubstract = Utility.IsSubstract.NotSubstract;
            IsNewService = Utility.IsNewService.Active;
            TotalSMS = 0;
            PartnerCode = -1;

            Client = new Client();
            Telco = new Telco();
            ServiceType = new ServiceType();
        }

        #endregion Contructor
    }
}
