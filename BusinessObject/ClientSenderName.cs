﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class ClientSenderName : BusinessObject
    {
        #region Private Members

        [DataMember]
        public string SenderName { get; set; }

        [DataMember]
        public ClientSenderNameStatus ClientSenderNameStatus { get; set; }

        #region ForeignKey

        [DataMember]
        public long ClientId { get; set; }

        [DataMember]
        public Client Client { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public ClientSenderName()
        {
            SenderName = "";
            ClientSenderNameStatus = Utility.ClientSenderNameStatus.Active;
        }

        #endregion Constructor
    }
}
