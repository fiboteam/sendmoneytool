﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class TelcoModemAdaptor : BusinessObject
    {
        #region Private Members

        #region ForeignKey

        [DataMember]
        public int TelCoID { get; set; }

        [DataMember]
        public int ModemAdaptorID { get; set; }

        [DataMember]
        public int ServiceTypeID { get; set; }

        [DataMember]
        public Telco Telco { get; set; }

        [DataMember]
        public ServiceType ServiceType { get; set; }

        [DataMember]
        public ModemAdaptor ModemAdaptor { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public TelcoModemAdaptor()
        {
            TelCoID = -1;
            ModemAdaptorID = -1;
            ModemAdaptorID = -1;
            CreatedDate = DateTime.Now;
        }

        #endregion Constructor
    }
}
