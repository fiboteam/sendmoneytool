﻿create  proc [dbo].[v2_spTelCoModemAdaptor_AddUpdate]
@TelCoModemAdaptorID int,
@TelCoID int,
@ServiceTypeID int,
@ModemAdaptorID int,
@CreatedDate datetime
as
begin
	if(@TelCoModemAdaptorID < 0)
		begin
			insert into tblTelCoModemAdaptor(TelCoID, ServiceTypeID, ModemAdaptorID, CreatedDate)
									  values(@TelCoID, @ServiceTypeID, @ModemAdaptorID, getdate())
							  
			set @TelCoModemAdaptorID = @@identity
		end
	else
		begin
			update 	tblTelCoModemAdaptor
			set 	TelCoID = @TelCoID,
					ServiceTypeID = @ServiceTypeID,
					ModemAdaptorID = @ModemAdaptorID
			where 	TelCoModemAdaptorID = @TelCoModemAdaptorID
		end
		
	select @TelCoModemAdaptorID
end