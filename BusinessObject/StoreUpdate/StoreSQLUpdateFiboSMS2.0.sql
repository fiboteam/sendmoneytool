﻿use fibosms
go

ALTER TABLE tblTelcoModemAdaptor
ADD ServiceTypeID int

GO

GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[v2_spClientCommingSMSHosting_AddUpdate]
@ClientCommingSMSHostingID bigint,
@ClientID bigint,
@PhoneNumber nvarchar(20),
@Message nvarchar(270),
@ClientCommingSMSHostingStatus tinyint,
@CreatedDate datetime,
@SentDate datetime,
@UpdatedDate datetime,
@ServiceTypeID int,
@CheckDate datetime,
@InCommingSMSID bigint,
@TelcoID int,
@IsSentMail tinyint,
@ProcessingTime int,
@GetByModemAdaptor int= 0
as
begin
if(@ClientCommingSMSHostingID < 0)
	begin

		if @ClientID is null or @ClientID = -1
		begin
			set @ClientID = 1381
		end

		insert into tblClientCommingSMSHosting(ClientID, PhoneNumber, [Message], ClientCommingSMSHostingStatus,
			CreatedDate, SentDate, UpdatedDate, ServiceTypeID, CheckDate, InCommingSMSID, TelcoID, IsSentMail, ProcessingTime,GetByModemAdaptor)
			values(@ClientID, @PhoneNumber, @Message, @ClientCommingSMSHostingStatus, @CreatedDate, @SentDate, @UpdatedDate, @ServiceTypeID, @CheckDate, @InCommingSMSID, @TelcoID, @IsSentMail, @IsSentMail,@GetByModemAdaptor)

		set @ClientCommingSMSHostingID=@@identity
	end
else
	begin
		update 	tblClientCommingSMSHosting
		set		ClientID = @ClientID, 
				PhoneNumber = @PhoneNumber, 
				[Message] = @Message, 
				ClientCommingSMSHostingStatus = @ClientCommingSMSHostingStatus,
				CreatedDate = @CreatedDate, 
				SentDate = @SentDate, 
				UpdatedDate = @UpdatedDate, 
				ServiceTypeID = @ServiceTypeID, 
				CheckDate = @CheckDate, 
				InCommingSMSID = @InCommingSMSID, 
				TelcoID = @TelcoID, 
				IsSentMail = @IsSentMail, 
				ProcessingTime= @IsSentMail
		where	ClientCommingSMSHostingID = @ClientCommingSMSHostingID
	end

select @ClientCommingSMSHostingID as ClientCommingSMSHostingID
end


SET QUOTED_IDENTIFIER OFF 

/****** Object:  StoredProcedure [dbo].[v2_spSMS_AddUpdate]    Script Date: 12/11/2012 09:54:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[v2_spSMS_AddUpdate]
	@SMSID bigint, 
	@ClientID bigint, 
	@PhoneNumber varchar(50), 
	@Message nvarchar(600), 
	@SMSGUID varchar(50),
	@SMSStatus tinyint,
	@CreatedDate datetime,
	@SentDate datetime,
	@UpdatedDate datetime,
	@ServiceTypeID int,
	@SendingTime int=0,
	@InCommingSMSID int = 0,
	@SenderName nvarchar(20)='n/a',
	@CountryCode nvarchar(10)='',
	@TelCoID int = 0,
	@MessageType int=1,
	@ContentType int=0,
	@IsSpam int,
	@DelayDate datetime,
	@Price decimal =0,
	@IsSubstract tinyint,
	@IsNewService tinyint,
	@PushSMSStatus tinyint,
	@ProcessingTimePushStatus int,
	@TotalSMS int,
	@ShortCreatedDate int,
	@PhoneNumberStatus int =3,
	@SpecifiedModemID int,
	@Remark nvarchar(1024)
AS
begin
	BEGIN TRANSACTION v2_spSMS_AddUpdate
	--bỏ qua check ip vì dùng nội bộ trong công ty, kiểm tra tài khoản
	if(@ServiceTypeID = 3498)
		set @SMSID = -1
	else
		begin
		-- check sendername if it is in the cient sender list
		if(	@sendername is not null and @sendername<>'' and lower(@sendername)<>'n/a') and 
			(
				select	count(ClientSenderNameID)
				from	dbo.tblClientSenderName
				where	ClientSenderName=@senderName and
						clientid=@clientID
			)<1
		begin
		
			set @SMSID=-1
		end
		else
		begin
			if(@SMSID<0)
				begin
					declare @TotalSMSReal int
					declare @Len int 
					set @Len = len(@Message)
				
					if(@ServiceTypeID = 3498)
						set @Len = @Len + 38
					
					if (@Len > 160 and @ServiceTypeID = 3498)
						set @TotalSMSReal = Ceiling(CAST(@Len as float) / 160)
					else
						begin
								if(@Len > 160)
									set @TotalSMSReal = Ceiling(CAST(@Len as float) / 150)
								else
									set @TotalSMSReal = 1
						end

					if(@TelCoID <= 0 or @TelCoID = null) 
						set @TelCoID = dbo.fnFindTelCo(@PhoneNumber)
					
					
					insert into dbo.tblSMS(ClientID,PhoneNumber, Message, SMSGUID,SMSStatus,ServiceTypeID,SenderName,TelCoID,MessageType,ContentType,SendingTime,InCommingSMSID,DelayDate,IsNewService,IsSubstract,TotalSMS)
						values(@ClientID,@PhoneNumber, @Message, @SMSGUID,@SMSStatus,@ServiceTypeID,@SenderName,@TelCoID,@MessageType,@ContentType,@SendingTime,@InCommingSMSID,@DelayDate,@IsNewService,@IsSubstract,@TotalSMSReal)
						set @SMSID=@@identity
						
					if(@IsNewService = 2)
						begin
							update	dbo.tblClientService
							set 	balance = balance - @TotalSMSReal
							where 	ClientID = @ClientID and
									ServiceTypeID =  @ServiceTypeID
						end
				end
			else
				begin
					update	dbo.tblSMS
					set		ClientID = @ClientID, 
							PhoneNumber = @PhoneNumber,
							Message = @Message, 
							SMSGUID = @SMSGUID,
							SMSStatus = @SMSStatus,
							ServiceTypeID = @ServiceTypeID,
							updatedDate = getdate(),
							SenderName = @SenderName,
							TelCoID = @TelCoID,
							MessageType = @MessageType,
							ContentType = @ContentType,
							SendingTime = @SendingTime,
							InCommingSMSID = @InCommingSMSID,
							DelayDate = @DelayDate,
							IsSubstract = @IsSubstract,
							IsNewService = @IsNewService
					where	SMSID=@SMSID
				end
		end
	END
	select @SMSID as SMSID
	
	IF @@ERROR != 0  -- neu co loi xay ra, rollback 
		BEGIN
			ROLLBACK TRAN v2_spSMS_AddUpdate
		END

	COMMIT TRAN v2_spSMS_AddUpdate
end
/****** Object:  StoredProcedure [dbo].[v2_spSMS_UpdateSMSBaseOnSentStatus]    Script Date: 12/11/2012 09:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[v2_spSMS_UpdateSMSBaseOnSentStatus]
@SMSID bigint,
@SMSStatus tinyint,
@SendingTime int,
@OperatorStatus tinyint = 1
as
	BEGIN TRANSACTION v2_UpdateSMSBaseOnSentStatus
	
	if(@SMSStatus <> 2)--not SentSuccess
		begin
			if(@SendingTime > 5)
				begin
					--Neu SendingTime lon hon LIMIT(5)
					update	tblSMS
					set		SMSStatus = @SMSStatus,
							UpdatedDate = GetDate()
					where	SMSID = @SMSID

					--va copy mot bang len tblSMSSEnt voi @smsStatus
					insert into tblSMSSent(ClientID, PhoneNumber, [Message], SMSSentGUID, SMSSentStatus, CreatedDate, SentDate, UpdatedDate, ServiceTypeID, InCommingSMSID, SenderName, TelCoID, MessageType, ContentType, Price, IsNewService, TotalSMS, OperatorStatus)
						select	ClientID, PhoneNumber, [Message], SMSGUID, @SMSStatus, CreatedDate, SentDate,								UpdatedDate, ServiceTypeID, SMSID, SenderName, TelCoID, MessageType, ContentType, Price, IsNewService, TotalSMS, @OperatorStatus
						from	tblSMS
						where	SMSID = @SMSID
				end
			else
				begin
					-- Neu chua toi gioi han thi doi trang thai
					-- SMS ve ban dau (Pending) va ang processtime de xu ly lan sau
					update  tblSMS
					set		SMSStatus = 0,
							UpdatedDate = GetDate()
					where  	SMSID = @SMSID
				end
		end
	else
		begin
			update	tblSMS
			set		SMSStatus = @SMSStatus,
					UpdatedDate = GetDate()
			where	SMSID = @SMSID

			--va copy mot bang len tblSMSSEnt voi status =@smsStatus,
			--va copy mot bang len tblSMSSEnt voi @smsStatus
			insert into tblSMSSent(ClientID, PhoneNumber, [Message], SMSSentGUID, SMSSentStatus, CreatedDate, SentDate, UpdatedDate, ServiceTypeID, InCommingSMSID, SenderName, TelCoID, MessageType, ContentType, Price, IsNewService, TotalSMS, OperatorStatus)
				select	ClientID, PhoneNumber, [Message], SMSGUID, @SMSStatus, CreatedDate, SentDate,								UpdatedDate, ServiceTypeID, SMSID, SenderName, TelCoID, MessageType, ContentType, Price, IsNewService, TotalSMS, @OperatorStatus
				from	tblSMS
				where	SMSID = @SMSID
		end

	IF @@ERROR != 0  -- neu co loi xay ra, rollback 
		BEGIN
			ROLLBACK TRAN v2_UpdateSMSBaseOnSentStatus
		END

	COMMIT TRAN v2_UpdateSMSBaseOnSentStatus

SET ANSI_NULLS ON

USE [fibosms]
GO
/****** Object:  StoredProcedure [dbo].[v2_spSMS_GetSMSToSend]    Script Date: 12/12/2012 08:09:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--v2_spSMS_GetSMSToSend 'MB4', 27, '8,7,6,5,4,3,2,1', 1, -1
/*
declare @ModemAdaptorName varchar(50)
declare @ModemAdaptorID int
declare @ListTelcoID varchar(50)
declare @ServiceTypeID int
declare @ClientID bigint

set @ModemAdaptorName = 'MB4'
set @ModemAdaptorID = 27
set @ListTelcoID = '8,7,6,5,4,3,2,1'
set @ServiceTypeID = 1
set @ClientID = -1

SELECT TOP 10 SMSID
FROM	dbo.tblSMS
WHERE	SMSStatus = 0 and --Pending
		(ServicetypeID = @serviceTypeID) and
		MessageType in (1, 2, 9, 10) and
		IsSubstract = 1 and
		(@ListTelcoID = '' or TelCoID in (select * from dbo.fnSplitIDListToTable(@ListTelcoID, ','))) and
		(@ClientID = -1 or ClientID = @ClientID)
ORDER BY IsSpam asc, SendingTime asc, SMSID asc

*/
CREATE proc [dbo].[v2_spSMS_GetSMSToSend]
@ModemAdaptorName varchar(50),
@ModemAdaptorID int,
@ListTelcoID varchar(50),
@ServiceTypeID int,
@ClientID bigint,
@TypeOfService tinyint
as
begin

	/*
		Draft = 0,
        NormalSMS = 1,
        ConcatenatedSMS = 2,
        Pending = 3,
        Delay = 9,
        CheckPhoneNumberStatus = 10
	*/
	--WAITFOR DELAY '000:00:03'
	
	DECLARE @SMSToSend TABLE (SMSID bigint)
	
	INSERT INTO @SMSToSend(SMSID)
		SELECT TOP 10 SMSID
		FROM	dbo.tblSMS
		WHERE	SMSStatus = 0 and --Pending
				(ServicetypeID = @serviceTypeID) and
				MessageType in (1, 2, 9, 10) and
				IsSubstract = 1 and
				(@ListTelcoID = '' or TelCoID in (select * from dbo.fnSplitIDListToTable(@ListTelcoID, ','))) and
				(@ClientID = -1 or ClientID = @ClientID)
		ORDER BY IsSpam asc, SendingTime asc, SMSID asc
	
	IF(@TypeOfService <> 2)
		BEGIN
				UPDATE 	dbo.tblSMS
				SET		SMSStatus = 1, --Sending
						SenderName = @ModemAdaptorName,
						SentDate = getdate(),
						SendingTime = SendingTime + 1
				WHERE	SMSID in (Select * from @SMSToSend)
		END
	ELSE
		BEGIN
				UPDATE 	dbo.tblSMS
				SET		SMSStatus = 1, --Sending
						SentDate = getdate(),
						SendingTime = SendingTime + 1
				WHERE	SMSID in (Select * from @SMSToSend)
		END

	SELECT	tblSMS.*, Clientname, ClientNo
	FROM	dbo.tblSMS  inner join dbo.tblClient on dbo.tblSMS.ClientID = dbo.tblClient.ClientID
	WHERE	SMSID in (select SMSID from @SMSToSend)

end