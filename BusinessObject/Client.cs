﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class Client : BusinessObject
    {
        #region Private Members
        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public string LoginName { get; set; }

        [DataMember]
        public string ClientPassword { get; set; }
        [DataMember]
        public ClientStatus ClientStatus { get; set; }

        [DataMember]
        public string Email1 { get; set; }

        [DataMember]
        public string Email2 { get; set; }
        [DataMember]
        public string Phone1 { get; set; }

        [DataMember]
        public string Phone2 { get; set; }
        [DataMember]
        public string Address1 { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string ClientNo { get; set; }

        [DataMember]
        public string ClientWebsite { get; set; }

        [DataMember]
        public string ClientSMSPage { get; set; }

        [DataMember]
        public ClientType ClientType { get; set; }

        [DataMember]
        public string IdentityNo { get; set; }

        [DataMember]
        public string BankInfor { get; set; }

        [DataMember]
        public string TaxCode { get; set; }

        [DataMember]
        public string Remark { get; set; }

        [DataMember]
        public IsSpam IsSpam { get; set; }

        [DataMember]
        public IsConcatenatedSMS IsConcatenatedSMS { get; set; }

        [DataMember]
        public string PasswordAdmin { get; set; }

        [DataMember]
        public double AgencyRate { get; set; }

        [DataMember]
        public IsAgency IsAgency { get; set; }

        [DataMember]
        public DateTime BirthDay { get; set; }

        [DataMember]
        public DateTime IdentityDate { get; set; }

        [DataMember]
        public string IdentityPlace { get; set; }

        [DataMember]
        public string ContractNumber { get; set; }

        [DataMember]
        public DateTime ContractDate { get; set; }

        [DataMember]
        public IsPersonal IsPersonal { get; set; }

        [DataMember]
        public IsApprove IsApprove { get; set; }

        [DataMember]
        public IsNewService IsNewService { get; set; }

        [DataMember]
        public PayLater PayLater { get; set; }

        [DataMember]
        public double MaxTopupDay { get; set; }

        [DataMember]
        public string MaxTopupPhoneNumber { get; set; }

        [DataMember]
        public string InitVector { get; set; }

        [DataMember]
        public string SecretKey { get; set; }

        #region ForeignKey

        [DataMember]
        public long? ParentClientID { get; set; }

        [DataMember]
        public Client ParentClient { get; set; }

        #endregion ForeignKey

        private string ResetPassword { get; set; }

        private short IsAutoReply { get; set; }

        #endregion Private Members

        #region Constructor

        public Client()
        {
            ClientName = "";
            LoginName = "";
            ClientPassword = "";
            ClientStatus = Utility.ClientStatus.Registered;
            Email1 = "";
            Email2 = "";
            Phone1 = "";
            Phone2 = "";
            Address1 = "";
            Address2 = "";
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            ClientNo = "";
            ClientWebsite = "";
            ClientSMSPage = "";
            ClientType = Utility.ClientType.SMSGateway;
            IdentityNo = "";
            BankInfor = "";
            TaxCode = "";
            Remark = "";
            IsAutoReply = 0;
            IsSpam = Utility.IsSpam.Active;
            IsConcatenatedSMS = Utility.IsConcatenatedSMS.DeActive;
            PasswordAdmin = "";
            AgencyRate = 0;
            IsAgency = Utility.IsAgency.DeActive;
            ParentClientID = -1;
            BirthDay = DateTime.Now;
            IdentityDate = DateTime.Now;
            IdentityPlace = "";
            ContractNumber = "";
            ContractDate = DateTime.Now;
            IsPersonal = Utility.IsPersonal.Active;
            IsApprove = Utility.IsApprove.DeActive;
            IsNewService = Utility.IsNewService.DeActive;
            PayLater = Utility.PayLater.DeActive;
            ResetPassword = "";
            MaxTopupDay = 0;
            MaxTopupPhoneNumber = "";
        }

        #endregion Constructor
    }
}
