﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class ClientIp : BusinessObject
    {
        #region Private Members

        [DataMember]
        public string ClientIP { get; set; }

        [DataMember]
        public ClientIPStatus ClientIPStatus { get; set; }

        #region ForeignKey

        [DataMember]
        public long ClientID { get; set; }

        [DataMember]
        public Client Client { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public ClientIp()
        {
            this.ClientIP = "";
            this.ClientIPStatus = Utility.ClientIPStatus.Active;
        }

        #endregion Constructor
    }
}
