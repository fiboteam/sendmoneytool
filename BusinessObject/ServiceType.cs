﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class ServiceType : BusinessObject
    {
        #region Private Members

        [DataMember]
        public string ServiceTypeName { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public ServiceTypeStatus ServiceTypeStatus { get; set; }

        [DataMember]
        public string Prefix { get; set; }

        [DataMember]
        public TypeOfService TypeOfService { get; set; }

        [DataMember]
        public IsPublicKeyword IsPublicKeyword { get; set; }

        [DataMember]
        public PaymentStatus PaymentStatus { get; set; }

        #region ForeignKey

        [DataMember]
        public int ServiceCompanyID { get; set; }

        [DataMember]
        public ServiceCompany ServiceCompany { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public ServiceType()
        {
            ServiceTypeName = "";
            PhoneNumber = "";
            ServiceTypeStatus = Utility.ServiceTypeStatus.Deactive;
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            Prefix = "";
            TypeOfService = Utility.TypeOfService.DauSo;
            ServiceCompanyID = -1;
            IsPublicKeyword = Utility.IsPublicKeyword.True;
            PaymentStatus = Utility.PaymentStatus.NotPay;
        }

        #endregion Constructor
    }
}
