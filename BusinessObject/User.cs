﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class User : BusinessObject
    {
        #region Private Members
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public UserStatus UserStatus { get; set; }

        #endregion Private Members

        #region Constructor

        public User()
        {
            UserName = "";
            Password = "";
            PhoneNumber = "";
            FullName = "";
            Email = "";
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            UserStatus = Utility.UserStatus.JustCreated;
        }

        #endregion Constructor
    }
}
