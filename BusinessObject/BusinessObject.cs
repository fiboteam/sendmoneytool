﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

using Utility;
using Resources.BusinessObjects;

namespace BusinessObjects
{
    [DataContract]
    public abstract class BusinessObject
    {

        [DataMember]
        private Dictionary<string, object> dynamicProperties = new Dictionary<string, object>();

        [DataMember]
        public Dictionary<string, object> ExtenstionProperties
        {
            get
            {
                return dynamicProperties;
            }
            set
            {
                dynamicProperties = value;
            }
        }

        private long id = -1;

        [ScaffoldColumn(false)]
        [DataMember]
        [Required]
        [Display(ResourceType = typeof(Global), Name = "ID")]
        public virtual long ID
        {
            get { return id; }
            set { id = value; }
        }

        public string EcryptedID
        {
            get { return GFunction.Encrypt(ID.ToString()); }
        }

        private DateTime createdDate = DateTime.Now;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return this.createdDate; }
            set { this.createdDate = value; }
        }

        private DateTime updatedDate = DateTime.Now;
        [DataMember]
        public DateTime UpdatedDate
        {
            get { return this.updatedDate; }
            set { this.updatedDate = value; }
        }
    }
}
