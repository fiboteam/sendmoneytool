﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class Syntax : BusinessObject
    {
        #region Private Members

        [DataMember]
        public string Prefix { get; set; }

        [DataMember]
        public SyntaxStatus SyntaxStatus { get; set; }

        [DataMember]
        public string QueryUrl { get; set; }

        [DataMember]
        public string TemplateSyntax { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public SyntaxType Type { get; set; }

        [DataMember]
        public IsFullCharged IsFullCharged { get; set; }

        #region ForeignKey

        [DataMember]
        public long? ClientId { get; set; }

        [DataMember]
        public Client Client { get; set; }

        [DataMember]
        public long? ServiceTypeId { get; set; }

        [DataMember]
        public ServiceType ServiceType { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public Syntax()
        {
            ClientId = null;
            ServiceTypeId = null;
            Prefix = "";
            SyntaxStatus = Utility.SyntaxStatus.Active;
            QueryUrl = "";
            TemplateSyntax = "";
            Content = "";
            Type = SyntaxType.SMSGateway;
            IsFullCharged = Utility.IsFullCharged.DeActive;
        }

        #endregion Contructor

    }
}
