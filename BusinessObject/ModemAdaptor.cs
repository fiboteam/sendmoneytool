﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class ModemAdaptor : BusinessObject
    {
        #region Private Members

        [DataMember]
        public string ModemAdaptorName { get; set; }

        [DataMember]
        public ModemAdaptorStatus ModemAdaptorStatus { get; set; }

        [DataMember]
        public string Remark { get; set; }

        [DataMember]
        public int Timeout { get; set; }

        [DataMember]
        public ModemType ModemType { get; set; }

        [DataMember]
        public string ComPort { get; set; }

        [DataMember]
        public int BaudRate { get; set; }

        [DataMember]
        public int SendTime { get; set; }

        [DataMember]
        public int GetTime { get; set; }

        [DataMember]
        public int TimerInterval { get; set; }

        [DataMember]
        public UpdateModemAdaptor UpdateModemAdaptor { get; set; }

        [DataMember]
        public int NumberOfError { get; set; }

        [DataMember]
        public string StoreProcedure { get; set; }

        [DataMember]
        public TurnOffModemAdaptor TurnOffModemAdaptor { get; set; }

        [DataMember]
        public CurrentErrorType CurrentErrorType { get; set; }

        [DataMember]
        public IsSMSPro IsSMSPro { get; set; }

        #region ForeignKey

        [DataMember]
        public int TelcoID { get; set; }

        [DataMember]
        public Telco Telco { get; set; }

        [DataMember]
        public long ClientID { get; set; }

        [DataMember]
        public Client Client { get; set; }

        #endregion ForeignKey

        #region Reference

        [DataMember]
        public List<TelcoModemAdaptor> ListTelcoModemAdaptor { get; set; }

        #endregion Reference

        #region Extenstion

        #endregion Extension

        #endregion Private Members

        #region Constructor

        public ModemAdaptor()
        {
            ModemAdaptorName = "";
            ModemAdaptorStatus = Utility.ModemAdaptorStatus.DeActive;
            Remark = "";
            Timeout = 30;
            ModemType = Utility.ModemType.PublicModem;
            ComPort = "";
            BaudRate = 0;
            SendTime = 1;
            GetTime = 0;
            TimerInterval = 10000;
            UpdateModemAdaptor = Utility.UpdateModemAdaptor.TurnOff;
            NumberOfError = 3;
            ClientID = -1;
            StoreProcedure = "";
            TurnOffModemAdaptor = Utility.TurnOffModemAdaptor.TurnOff;
            CurrentErrorType = CurrentErrorType.Normal;
            CreatedDate = DateTime.Now;
            TelcoID = -1;
            IsSMSPro = Utility.IsSMSPro.DeActive;
            UpdatedDate = DateTime.Now;

            Telco = new Telco();
            Client = new Client();
        }

        #endregion Constructor

        public string GetListTelcoID(int serviceTypeID)
        {
            try
            {
                StringBuilder strBuilder = new StringBuilder();

                if (ListTelcoModemAdaptor != null)
                {
                    foreach (TelcoModemAdaptor telco in ListTelcoModemAdaptor.Where(telcoModemAdaptor => telcoModemAdaptor.ServiceTypeID == serviceTypeID))
                        strBuilder.Append(telco.TelCoID).Append(",");
                }

                return strBuilder.ToString();
            }
            catch
            {
                return "";
            }
        }

        public int[] GetListServiceType()
        {
            try
            {
                List<int> listServiceTypeID = new List<int>();

                if (ListTelcoModemAdaptor != null)
                {
                    foreach (int serviceTypeID in ListTelcoModemAdaptor.Select(telcoModemAdaptor => telcoModemAdaptor.ServiceTypeID).Distinct())
                        listServiceTypeID.Add(serviceTypeID);
                }

                return listServiceTypeID.ToArray();
            }
            catch
            {
                return null;
            }
        }
    }
}
