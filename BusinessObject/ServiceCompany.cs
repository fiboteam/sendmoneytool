﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class ServiceCompany : BusinessObject
    {
        #region Private Members

        [DataMember]
        public string ServiceCompanyName { get; set; }

        [DataMember]
        public ServiceCompanyStatus ServiceCompanyStatus { get; set; }

        [DataMember]
        public string Remark { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string ServiceDesc { get; set; }

        [DataMember]
        public string Contact { get; set; }

        [DataMember]
        public string Website { get; set; }

        [DataMember]
        public string ClassName { get; set; }

        #region Reference

        [DataMember]
        public List<ServiceType> ListServiceType { get; set; }

        #endregion Reference

        #endregion Private Members

        #region Constructor

        public ServiceCompany()
        {
            ServiceCompanyName = "";
            CreatedDate = DateTime.Now;
            ServiceCompanyStatus = Utility.ServiceCompanyStatus.DeActive;
            Remark = "";
            Phone = "";
            Address = "";
            ServiceDesc = "";
            Contact = "";
            Website = "";
            ClassName = "";
        }

        #endregion Constructor
    }
}
