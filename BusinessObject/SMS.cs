﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;
using Resources;

namespace BusinessObjects
{
    [DataContract]
    public class SMS : BusinessObject
    {
        #region Private Members
        private string mMessage = "";

        [DataMember]
        [Required]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string Message
        {
            get { return this.mMessage; }
            set { this.mMessage = GFunction.ConvertSpecialXMLCharToOriginalChar(value); }
        }

        [DataMember]
        public string SMSGUID { get; set; }

        [DataMember]
        [Required]
        public SMSStatus SMSStatus { get; set; }

        [DataMember]
        public DateTime SentDate { get; set; }

        [DataMember]
        public int SendingTime { get; set; }

        [DataMember]
        public string InCommingSMSID { get; set; }

        [DataMember]
        public string SenderName { get; set; }

        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        [Required]
        public SMSHostingMessageType MessageType { get; set; }

        [DataMember]
        [Required]
        public SMSHostingContentType ContentType { get; set; }

        [DataMember]
        public IsSpam IsSpam { get; set; }

        [DataMember]
        public DateTime DelayDate { get; set; }

        [DataMember]
        public double Price { get; set; }

        [DataMember]
        [Required]
        public IsSubstract IsSubstract { get; set; }

        [DataMember]
        [Required]
        public IsNewService IsNewService { get; set; }

        [DataMember]
        public PushSMSStatus PushSMSStatus { get; set; }

        [DataMember]
        public int ProcessingTimePushStatus { get; set; }

        [DataMember]
        public int TotalSMS { get; set; }

        [DataMember]
        public int ShortCreatedDate { get; set; }

        [DataMember]
        public PhoneNumberStatus PhoneNumberStatus { get; set; }

        [DataMember]
        public string Remark { get; set; }

        #region ForeignKey

        [DataMember]
        [Required]
        public long ClientID { get; set; }

        [DataMember]
        public Client Client { get; set; }

        [DataMember]
        public int TelCoID { get; set; }

        [DataMember]
        public Telco Telco { get; set; }

        [DataMember]
        public int SpecifiedModem { get; set; }

        [DataMember]
        public ModemAdaptor SpecifiedModemObject { get; set; }

        [DataMember]
        [Required]
        public int ServiceTypeID { get; set; }

        [DataMember]
        public ServiceType ServiceType { get; set; }

        [DataMember]
        public string PartnerPhoneNumberStatus { get; set; }

        [DataMember]
        public int? SentByModemAdaptor { get; set; }

        [DataMember]
        public ModemAdaptor SentByModemAdaptorModel { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public SMS()
        {
            ClientID = -1;
            PhoneNumber = "";
            Message = "";
            SMSGUID = "";
            SMSStatus = Utility.SMSStatus.Pending;
            CreatedDate = DateTime.Now;
            SentDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            ServiceTypeID = -1;
            SendingTime = 0;
            InCommingSMSID = "-1";
            SenderName = "n/a";
            CountryCode = "";
            TelCoID = 0;
            MessageType = SMSHostingMessageType.NormalSMS;
            ContentType = SMSHostingContentType.Text;
            IsSpam = Utility.IsSpam.DeActive;
            DelayDate = DateTime.Now;
            Price = 0;
            IsSubstract = Utility.IsSubstract.NotSubstract;
            IsNewService = Utility.IsNewService.Active;
            PushSMSStatus = Utility.PushSMSStatus.JustReceived;
            ProcessingTimePushStatus = 0;
            TotalSMS = 1;
            ShortCreatedDate = DateTime.Now.ToString("yyyyMMdd").AsInt();
            PhoneNumberStatus = Utility.PhoneNumberStatus.NotCheck;
            SpecifiedModem = -1;
            Remark = "";

            Client = new Client();
            Telco = new Telco();
            ServiceType = new ServiceType();
        }

        #endregion Contructor
    }
}
