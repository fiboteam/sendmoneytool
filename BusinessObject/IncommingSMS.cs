﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class DeleteMessage
    {
        public int IndexMessage { get; set; }
        public bool IsDelete { get; set; }
    }
    public class IncommingSMS
    {
        public IncommingSMS(string _phone, string _message, int _index)
        {
            PhoneNumber = _phone;
            Content = _message;
            IndexMessageList = new List<DeleteMessage>() { new DeleteMessage() { IndexMessage = _index } };
            TotalPart = 1;
            GuidId = Guid.NewGuid().ToString();
            IsSubmit = false;
            IsSubmitFibo = false;
            NumberTryDelete = 0;
            AmountOfPushSMSToClient = 0;
            AmountSubmitFibo = 0;
            AmountDeleteMO = 0;
            AmoutUpdateMO = 0;
            URLPushSMSToClient = "";
        }
        public void AddIndex(int _index)
        {
            if (IndexMessageList != null && !IndexMessageList.Any(t => t.IndexMessage == _index))
                IndexMessageList.Add(new DeleteMessage() { IndexMessage = _index });
        }
        public void UpdateFullMessage(string _message)
        {
            Content += _message;
        }
        public long TotalPart { get; set; }
        public string PhoneNumber { get; set; }
        public string Content { get; set; }
        public string GuidId { get; set; }
        public List<DeleteMessage> IndexMessageList { get; set; }
        public bool IsSubmit { get; set; }
        public bool IsSubmitFibo { get; set; }
        /// <summary>
        /// Check tin này đã thử delete nhiều lần ko dc
        /// </summary>
        public int NumberTryDelete { get; set; }

        public long ServiceTypeID { get; set; }
        /// <summary>
        /// Url được dùng để push 
        /// </summary>
        public string URLPushSMSToClient { get; set; }
        /// <summary>
        /// Số lần push sms fail
        /// </summary>
        public int AmountOfPushSMSToClient { get; set; }
        /// <summary>
        /// số lần submit MO lên Fibo
        /// </summary>
        public int AmountSubmitFibo { get; set; }
        /// <summary>
        /// Số lần delete MO
        /// </summary>
        public int AmountDeleteMO { get; set; }
        public int AmoutUpdateMO { get; set; }
    }
}
