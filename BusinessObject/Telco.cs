﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class Telco : BusinessObject
    {
        #region Private Members

        [DataMember]
        public string TelcoName { get; set; }

        [DataMember]
        public string TelcoSyntax { get; set; }

        [DataMember]
        public TelCoStatus TelcoStatus { get; set; }

        #endregion Private Members

        #region Constructor

        public Telco()
        {
            TelcoName = "";
            TelcoSyntax = "";
            TelcoStatus = Utility.TelCoStatus.DeActive;
            CreatedDate = DateTime.Now;
        }

        #endregion Constructor
    }
}
