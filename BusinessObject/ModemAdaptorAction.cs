﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    /// <summary>
    /// 19/05/2015
    /// NMH
    /// </summary>
    public class ModemAdaptorAction : BusinessObject
    {
        public int ModemAdaptorID { get; set; }
        public int TypeModemAdaptorActionID { get; set; }
        public Int16 StatusModemAdaptorAction { get; set; }
        public long UpdateBy { get; set; }
        public string Remark { get; set; }

        public ModemAdaptorAction()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            Remark = "";
            StatusModemAdaptorAction = 1;
        }
    }

}
