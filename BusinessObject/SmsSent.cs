﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class SmsSent : BusinessObject
    {
        #region Private Members

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string SMSSentGUID { get; set; }

        [DataMember]
        public SMSSentStatus SMSSentStatus { get; set; }

        [DataMember]
        public DateTime? SentDate { get; set; }

        [DataMember]
        public long InCommingSMSID { get; set; }

        [DataMember]
        public string SenderName { get; set; }

        [DataMember]
        public SMSHostingMessageType MessageType { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public IsNewService IsNewService { get; set; }

        [DataMember]
        public int SentByTelcoID { get; set; }

        [DataMember]
        public decimal CostOfSell { get; set; }

        [DataMember]
        public int TotalSMS { get; set; }

        [DataMember]
        public int ShortCreatedDate { get; set; }

        [DataMember]
        public PhoneNumberStatus PhoneNumberStatus { get; set; }

        [DataMember]
        public string PartnerPhoneNumberStatus { get; set; }

        [DataMember]
        public string Remark { get; set; }

        [DataMember]
        public OperatorStatus OperatorStatus { get; set; }

        [DataMember]
        public SMSHostingContentType ContentType { get; set; }

        #region ForeignKey

        [DataMember]
        public long ClientID { get; set; }

        [DataMember]
        public Client Client { get; set; }

        [DataMember]
        public long ServiceTypeID { get; set; }

        [DataMember]
        public ServiceType ServiceType { get; set; }

        [DataMember]
        public int? SentByModemAdaptor { get; set; }

        [DataMember]
        public ModemAdaptor SentByModemAdaptorModel { get; set; }

        [DataMember]
        public int TelCoID { get; set; }

        [DataMember]
        public Telco Telco { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public SmsSent()
        {
            ClientID = -1;
            PhoneNumber = "";
            Message = "";
            SMSSentGUID = "";
            SMSSentStatus = Utility.SMSSentStatus.SentSuccess;
            CreatedDate = DateTime.Now;
            SentDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            ServiceTypeID = -1;
            InCommingSMSID = -1;
            SenderName = "n/a";
            TelCoID = 0;
            MessageType = SMSHostingMessageType.Draft;
            ContentType = SMSHostingContentType.Text;
            Price = -1;
            IsNewService = Utility.IsNewService.Active;            
            TotalSMS = 1;
            ShortCreatedDate = DateTime.Now.ToString("yyyyMMdd").AsInt();
            PhoneNumberStatus = Utility.PhoneNumberStatus.NotCheck;
            Remark = "";

            Client = new Client();
            Telco = new Telco();
            ServiceType = new ServiceType();
        }

        #endregion Contructor
    }
}
