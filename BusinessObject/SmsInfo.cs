﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class SmsInfo
    {
        public SmsInfo()
        {
            index = 0;
            length = 0;
            total = 1;
            part = 1;
        }
        public int index { get; set; }
        public int stat { get; set; }
        public string alpha { get; set; }
        public int length { get; set; }
        public string message { get; set; }
        public string phonenumber { get; set; }
        public DateTime receiveddate { get; set; }
        public long total { get; set; }
        public string userdataheader { get; set; }
        public int part { get; set; }
    }
}
