﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class ClientServiceCharge : BusinessObject
    {
        #region Private Members

        [DataMember]
        public double Money { get; set; }

        [DataMember]
        public double RefundMoney { get; set; }

        [DataMember]
        public DateTime AvailableDate { get; set; }

        [DataMember]
        public ClientServiceChargesStatus Status { get; set; }

        #region ForeignKey

        [DataMember]
        public long UpdatedByUser { get; set; }

        [DataMember]
        public long CreatedByUser { get; set; }

        [DataMember]
        public long ClientID { get; set; }

        [DataMember]
        public Client Client { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public ClientServiceCharge()
        {
        }

        #endregion Constructor
    }
}
