using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

using Utility;

namespace BusinessObjects
{
    [DataContract]
    public class ClientCommingSMSHosting : BusinessObject
    {
        #region Private Members

        [DataMember]
        [Required]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public ClientCommingSMSHostingStatus ClientCommingSMSHostingStatus { get; set; }

        [DataMember]
        public DateTime SentDate { get; set; }

        [DataMember]
        public DateTime CheckDate { get; set; }

        [DataMember]
        public long InCommingSMSID { get; set; }

        [DataMember]
        public IsSentMail IsSentMail { get; set; }

        [DataMember]
        public int ProcessingTime { get; set; }

        [DataMember]
        public int GetByModemAdaptor { get; set; }

        #region ForeignKey

        [DataMember]
        [Required]
        public long ClientID { get; set; }

        [DataMember]
        public Client Client { get; set; }

        [DataMember]
        [Required]
        public int ServiceTypeID { get; set; }

        [DataMember]
        public ServiceType ServiceType { get; set; }

        [DataMember]
        public int TelCoID { get; set; }

        [DataMember]
        public Telco Telco { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public ClientCommingSMSHosting()
        {
            ClientID = -1;
            PhoneNumber = "";
            Message = "";
            ClientCommingSMSHostingStatus = Utility.ClientCommingSMSHostingStatus.JustReceived;
            CreatedDate = DateTime.Now;
            SentDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
            ServiceTypeID = 0;
            CheckDate = DateTime.Now;
            InCommingSMSID = -1;
            TelCoID = 0;
            IsSentMail = IsSentMail.DeActive;
            ProcessingTime = 0;
        }

        #endregion Constructor

    }
}
