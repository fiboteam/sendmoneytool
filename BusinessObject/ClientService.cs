﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BusinessObjects
{
    [DataContract]
    public class ClientService : BusinessObject
    {
        #region Private Members

        [DataMember]
        public DateTime SendSMSExpiredDate { get; set; }

        #region ForeignKey

        [DataMember]
        public long ClientID { get; set; }

        [DataMember]
        public long ServiceTypeId { get; set; }

        [DataMember]
        public Client Client { get; set; }

        [DataMember]
        public ServiceType ServiceType { get; set; }

        #endregion ForeignKey

        #endregion Private Members

        #region Constructor

        public ClientService()
        {
        }

        #endregion Constructor
    }
}
